var appendix =
[
    [ "Theming SafeShare's web-based interfaces", "theming-the-applications-web-based-interfaces.html", [
      [ "Themable areas of SafeShare for Web", "theming-the-applications-web-based-interfaces.html#themable-areas-of-the-web-application", null ],
      [ "Themable web-based administration areas", "theming-the-applications-web-based-interfaces.html#themable-web-based-administration-areas", null ],
      [ "Other themable SafeShare areas", "theming-the-applications-web-based-interfaces.html#other-themable-application-areas", null ]
    ] ],
    [ "Managing SafeShare for iOS through MDM software", "managing-safe-share-for-ios-through-mdm-software.html", [
      [ "Step 1 - Install SafeShare for iOS via MDM software", "managing-safe-share-for-ios-through-mdm-software.html#step-1-install-safe-share-for-ios-via-mdm-software", null ],
      [ "Step 2 - Configure SafeShare for iOS via MDM software", "managing-safe-share-for-ios-through-mdm-software.html#step-2-configure-safe-share-for-ios-via-mdm-software", null ],
      [ "The 'mdm-configuration.plist' file", "managing-safe-share-for-ios-through-mdm-software.html#the-mdm-configuration-plist-file", null ]
    ] ]
];