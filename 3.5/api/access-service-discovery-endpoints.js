var access_service_discovery_endpoints =
[
    [ "<access-service>/api/v1", "access-service-api-v1-discovery.html", [
      [ "URL structure", "access-service-api-v1-discovery.html#access-service-api-v1-discovery-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-discovery.html#access-service-api-v1-discovery-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-discovery.html#access-service-api-v1-discovery-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-discovery.html#access-service-api-v1-discovery-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-discovery.html#access-service-api-v1-discovery-required-headers", null ],
      [ "Returns", "access-service-api-v1-discovery.html#access-service-api-v1-discovery-returns", null ]
    ] ]
];