var folder_content_handling_endpoints =
[
    [ "<content-service>/api/v1/collections/{collectionId}/contents", "content-service-api-v1-collections-collectionid-contents.html", [
      [ "URL structure", "content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-detailed-description", null ],
      [ "Supported roles and conditions", "content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-supported-roles-and-conditions", null ],
      [ "Required headers", "content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-required-headers", null ],
      [ "Returns", "content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-returns", null ]
    ] ]
];