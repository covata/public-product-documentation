var developers_guide =
[
    [ "What the Covata API can do", "developers-guide.html#what-the-covata-api-can-do", null ],
    [ "Tools for accessing the Covata API", "developers-guide.html#tools-for-accessing-the-product-api", null ],
    [ "Authentication and authorisation", "authentication-and-authorisation.html", [
      [ "Working with OAuth 2.0 on the Covata Platform", "authentication-and-authorisation.html#working-with-oauth-2-0-on-the-platform", null ],
      [ "Registering your application on the Covata Platform", "authentication-and-authorisation.html#registering-your-application-on-the-platform", null ],
      [ "Overview of the OAuth 2.0 process", "authentication-and-authorisation.html#overview-of-the-oauth-2-0-process", null ],
      [ "Initiating authentication and authorisation", "authentication-and-authorisation.html#initiating-authentication-and-authorisation", [
        [ "Using the authorisation code grant type", "authentication-and-authorisation.html#using-the-authorisation-code-grant-type", null ],
        [ "Using the implicit grant type", "authentication-and-authorisation.html#using-the-implicit-grant-type", null ],
        [ "Using the password grant type", "authentication-and-authorisation.html#using-the-password-grant-type", null ]
      ] ],
      [ "Receiving the access token", "authentication-and-authorisation.html#receiving-the-access-token", [
        [ "From authorisation code or password grant type requests", "authentication-and-authorisation.html#from-authorisation-code-or-password-grant-type-requests", null ],
        [ "From implicit grant type requests", "authentication-and-authorisation.html#from-implicit-grant-type-requests", null ]
      ] ]
    ] ],
    [ "Working with files and folders (fundamentals)", "working-with-files-and-folders-fundamentals.html", [
      [ "Definitions", "working-with-files-and-folders-fundamentals.html#definitions", [
        [ "Secure Object", "working-with-files-and-folders-fundamentals.html#secure-object", null ],
        [ "Collection", "working-with-files-and-folders-fundamentals.html#collection", null ],
        [ "Item", "working-with-files-and-folders-fundamentals.html#item", null ],
        [ "Organisation", "working-with-files-and-folders-fundamentals.html#organisation", null ]
      ] ],
      [ "Obtaining a cryptographic key", "working-with-files-and-folders-fundamentals.html#obtaining-a-cryptographic-key", null ],
      [ "Initializing a new 'Incomplete' file object", "working-with-files-and-folders-fundamentals.html#initialising-a-new-incomplete-file-object", null ],
      [ "Completing a file object (uploading its locally-encrypted data)", "working-with-files-and-folders-fundamentals.html#completing-a-file-object-uploading-its-locally-encrypted-data", null ],
      [ "Completing a file object (uploading its unencrypted data)", "working-with-files-and-folders-fundamentals.html#completing-a-file-object-uploading-its-unencrypted-data", null ],
      [ "Downloading a file object's data", "working-with-files-and-folders-fundamentals.html#downloading-a-file-objects-data", null ],
      [ "Creating a folder to manage items", "working-with-files-and-folders-fundamentals.html#creating-a-folder-to-manage-items", null ],
      [ "Moving items to other folders", "working-with-files-and-folders-fundamentals.html#moving-items-to-other-folders", null ]
    ] ],
    [ "Retrieving information about items", "retrieving-information-about-items.html", [
      [ "Retrieving info about all immediate items of a common parent", "retrieving-information-about-items.html#retrieving-info-about-all-immediate-items-of-a-common-parent-folder", null ],
      [ "Retrieving info about all items filtered by text search", "retrieving-information-about-items.html#retrieving-info-about-all-items-filtered-by-text-search", null ],
      [ "Retrieving info about items based on ownership or sharing status", "retrieving-information-about-items.html#retrieving-info-about-items-based-on-ownership-or-sharing-status", null ],
      [ "Retrieving info about all ancestral collections of an item", "retrieving-information-about-items.html#retrieving-info-about-all-ancestral-folders-of-an-item", null ],
      [ "Sorting, ordering and paginating the list of item info", "retrieving-information-about-items.html#sorting-ordering-and-paginating-the-list-of-item-info", null ]
    ] ],
    [ "Sharing items", "sharing-items.html", [
      [ "Sharing a file with collaborators", "sharing-items.html#sharing-a-file-with-collaborators", null ],
      [ "Sharing a folder with collaborators", "sharing-items.html#sharing-a-folder-with-collaborators", null ],
      [ "Determining existing collaborators on an item", "sharing-items.html#determining-existing-collaborators-on-an-item", null ],
      [ "Modifying existing collaborators on an item", "sharing-items.html#modifying-existing-collaborators-on-an-item", null ]
    ] ]
];