var NAVTREEINDEX3 =
{
"access-service-api-v1-users-me-expire.html#access-service-api-v1-users-me-expire-supported-roles":[1,6,1,3],
"access-service-api-v1-users-me-expire.html#access-service-api-v1-users-me-expire-url-structure":[1,6,1,0],
"access-service-api-v1-users-me-notifications-email.html":[1,6,6],
"access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-detailed-description":[1,6,6,2],
"access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-required-headers":[1,6,6,4],
"access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-required-put-request-parameters":[1,6,6,5],
"access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-returns":[1,6,6,6],
"access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-supported-methods-and-overview":[1,6,6,1],
"access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-supported-roles":[1,6,6,3],
"access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-url-structure":[1,6,6,0],
"access-service-api-v1-users-me.html":[1,6,0],
"access-service-api-v1-users-me.html#access-service-api-v1-users-me-detailed-description":[1,6,0,2],
"access-service-api-v1-users-me.html#access-service-api-v1-users-me-optional-parameters":[1,6,0,5],
"access-service-api-v1-users-me.html#access-service-api-v1-users-me-required-headers":[1,6,0,4],
"access-service-api-v1-users-me.html#access-service-api-v1-users-me-returns":[1,6,0,6],
"access-service-api-v1-users-me.html#access-service-api-v1-users-me-supported-methods-and-overview":[1,6,0,1],
"access-service-api-v1-users-me.html#access-service-api-v1-users-me-supported-roles":[1,6,0,3],
"access-service-api-v1-users-me.html#access-service-api-v1-users-me-url-structure":[1,6,0,0],
"access-service-api-v1-users-userid-history.html":[1,6,7],
"access-service-api-v1-users-userid-history.html#access-service-api-v1-users-userid-history-detailed-description":[1,6,7,2],
"access-service-api-v1-users-userid-history.html#access-service-api-v1-users-userid-history-optional-parameters":[1,6,7,5],
"access-service-api-v1-users-userid-history.html#access-service-api-v1-users-userid-history-required-headers":[1,6,7,4],
"access-service-api-v1-users-userid-history.html#access-service-api-v1-users-userid-history-returns":[1,6,7,6],
"access-service-api-v1-users-userid-history.html#access-service-api-v1-users-userid-history-supported-methods-and-overview":[1,6,7,1],
"access-service-api-v1-users-userid-history.html#access-service-api-v1-users-userid-history-supported-roles-and-conditions":[1,6,7,3],
"access-service-api-v1-users-userid-history.html#access-service-api-v1-users-userid-history-url-structure":[1,6,7,0],
"access-service-api-v1-users-userid-unlock.html":[1,6,14],
"access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-detailed-description":[1,6,14,2],
"access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-required-headers":[1,6,14,4],
"access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-returns":[1,6,14,5],
"access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-supported-methods-and-overview":[1,6,14,1],
"access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-supported-roles-and-conditions":[1,6,14,3],
"access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-url-structure":[1,6,14,0],
"access-service-api-v1-users-userid.html":[1,6,13],
"access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-detailed-description":[1,6,13,2],
"access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-required-headers":[1,6,13,4],
"access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-required-parameters":[1,6,13,5],
"access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-returns":[1,6,13,6],
"access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-supported-methods-and-overview":[1,6,13,1],
"access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-supported-roles-and-conditions":[1,6,13,3],
"access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-url-structure":[1,6,13,0],
"access-service-api-v1-users.html":[1,6,12],
"access-service-api-v1-users.html#access-service-api-v1-users-detailed-description":[1,6,12,2],
"access-service-api-v1-users.html#access-service-api-v1-users-optional-get-request-parameters":[1,6,12,6],
"access-service-api-v1-users.html#access-service-api-v1-users-required-headers":[1,6,12,4],
"access-service-api-v1-users.html#access-service-api-v1-users-required-put-request-parameters":[1,6,12,5],
"access-service-api-v1-users.html#access-service-api-v1-users-returns":[1,6,12,7],
"access-service-api-v1-users.html#access-service-api-v1-users-supported-methods-and-overview":[1,6,12,1],
"access-service-api-v1-users.html#access-service-api-v1-users-supported-roles-and-conditions":[1,6,12,3],
"access-service-api-v1-users.html#access-service-api-v1-users-url-structure":[1,6,12,0],
"access-service-api-v1-userwhitelist-bulk.html":[1,10,4],
"access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-detailed-description":[1,10,4,2],
"access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-required-headers":[1,10,4,4],
"access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-required-parameters":[1,10,4,5],
"access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-returns":[1,10,4,6],
"access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-supported-methods-and-overview":[1,10,4,1],
"access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-supported-roles":[1,10,4,3],
"access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-url-structure":[1,10,4,0],
"access-service-api-v1-userwhitelist-objectid.html":[1,10,3],
"access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-detailed-description":[1,10,3,2],
"access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-required-headers":[1,10,3,4],
"access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-returns":[1,10,3,5],
"access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-supported-methods-and-overview":[1,10,3,1],
"access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-supported-roles":[1,10,3,3],
"access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-url-structure":[1,10,3,0],
"access-service-api-v1-versions-versionid-key.html":[1,2,4],
"access-service-api-v1-versions-versionid-key.html#access-service-api-v1-objects-objectid-key-url-structure":[1,2,4,0],
"access-service-api-v1-versions-versionid-key.html#access-service-api-v1-versions-versionid-key-detailed-description":[1,2,4,2],
"access-service-api-v1-versions-versionid-key.html#access-service-api-v1-versions-versionid-key-required-headers":[1,2,4,4],
"access-service-api-v1-versions-versionid-key.html#access-service-api-v1-versions-versionid-key-returns":[1,2,4,5],
"access-service-api-v1-versions-versionid-key.html#access-service-api-v1-versions-versionid-key-supported-methods-and-overview":[1,2,4,1],
"access-service-api-v1-versions-versionid-key.html#access-service-api-v1-versions-versionid-keys-supported-roles-and-conditions":[1,2,4,3],
"access-service-api-v1-versions-versionid-viewkey.html":[1,2,5],
"access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-detailed-description":[1,2,5,2],
"access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-required-headers":[1,2,5,4],
"access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-returns":[1,2,5,5],
"access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-supported-methods-and-overview":[1,2,5,1],
"access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-supported-roles-and-conditions":[1,2,5,3],
"access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-url-structure":[1,2,5,0],
"access-service-discovery-endpoints.html":[1,1],
"access-service-theme.html":[1,18,18],
"access-service-theme.html#access-service-theme-detailed-description":[1,18,18,2],
"access-service-theme.html#access-service-theme-required-headers":[1,18,18,4],
"access-service-theme.html#access-service-theme-returns":[1,18,18,5],
"access-service-theme.html#access-service-theme-supported-methods-and-overview":[1,18,18,1],
"access-service-theme.html#access-service-theme-supported-roles-and-conditions":[1,18,18,3],
"access-service-theme.html#access-service-theme-url-structure":[1,18,18,0],
"api-reference-guide.html":[1],
"auditing-reporting-endpoints.html":[1,13],
"authentication-and-authorisation-endpoints.html":[1,0],
"authentication-and-authorisation.html":[0,2],
"authentication-and-authorisation.html#from-authorisation-code-or-password-grant-type-requests":[0,2,4,0],
"authentication-and-authorisation.html#from-implicit-grant-type-requests":[0,2,4,1],
"authentication-and-authorisation.html#initiating-authentication-and-authorisation":[0,2,3],
"authentication-and-authorisation.html#overview-of-the-oauth-2-0-process":[0,2,2],
"authentication-and-authorisation.html#receiving-the-access-token":[0,2,4],
"authentication-and-authorisation.html#registering-your-application-on-the-platform":[0,2,1],
"authentication-and-authorisation.html#using-the-authorisation-code-grant-type":[0,2,3,0],
"authentication-and-authorisation.html#using-the-implicit-grant-type":[0,2,3,1],
"authentication-and-authorisation.html#using-the-password-grant-type":[0,2,3,2],
"authentication-and-authorisation.html#working-with-oauth-2-0-on-the-platform":[0,2,0],
"classification-endpoints.html":[1,8],
"client-application-endpoints.html":[1,17],
"content-service-api-v1-collections-collectionid-contents.html":[1,22,0],
"content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-detailed-description":[1,22,0,2],
"content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-required-headers":[1,22,0,4],
"content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-returns":[1,22,0,5],
"content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-supported-methods-and-overview":[1,22,0,1],
"content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-supported-roles-and-conditions":[1,22,0,3],
"content-service-api-v1-collections-collectionid-contents.html#content-service-api-v1-collections-collectionid-contents-url-structure":[1,22,0,0],
"content-service-api-v1-discovery.html":[1,20,0],
"content-service-api-v1-discovery.html#content-service-api-v1-discovery-detailed-description":[1,20,0,2],
"content-service-api-v1-discovery.html#content-service-api-v1-discovery-required-headers":[1,20,0,4],
"content-service-api-v1-discovery.html#content-service-api-v1-discovery-returns":[1,20,0,5],
"content-service-api-v1-discovery.html#content-service-api-v1-discovery-supported-methods-and-overview":[1,20,0,1],
"content-service-api-v1-discovery.html#content-service-api-v1-discovery-supported-roles":[1,20,0,3],
"content-service-api-v1-discovery.html#content-service-api-v1-discovery-url-structure":[1,20,0,0],
"content-service-api-v1-objects-objectid-contents.html":[1,21,0],
"content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-detailed-description":[1,21,0,2],
"content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-optional-get-request-parameters":[1,21,0,8],
"content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-get-request-parameters":[1,21,0,7],
"content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-headers":[1,21,0,4],
"content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-parameters-for-multi-chunk-uploads":[1,21,0,6],
"content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-post-request-parameters":[1,21,0,5],
"content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-returns":[1,21,0,9],
"content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-supported-methods-and-overview":[1,21,0,1],
"content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-supported-roles-and-conditions":[1,21,0,3],
"content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-url-structure":[1,21,0,0],
"content-service-api-v1-objects-objectid-versions.html":[1,21,2],
"content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-detailed-description":[1,21,2,2],
"content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-required-headers":[1,21,2,4],
"content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-required-parameters":[1,21,2,5],
"content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-required-parameters-for-multi-chunk-uploads":[1,21,2,6],
"content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-returns":[1,21,2,7],
"content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-supported-methods-and-overview":[1,21,2,1],
"content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-supported-roles-and-conditions":[1,21,2,3],
"content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-url-structure":[1,21,2,0],
"content-service-api-v1-objects-objectid-view.html":[1,21,1],
"content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-detailed-description":[1,21,1,2],
"content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-optional-parameters-on-GET":[1,21,1,5],
"content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-required-headers":[1,21,1,4],
"content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-returns-from-a-GET-or-PUT-request":[1,21,1,6],
"content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-supported-methods-and-overview":[1,21,1,1],
"content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-supported-roles-and-conditions":[1,21,1,3],
"content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-url-structure":[1,21,1,0],
"content-service-api-v1-versions-versionid-contents.html":[1,21,3],
"content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-detailed-description":[1,21,3,2],
"content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-optional-parameters":[1,21,3,6],
"content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-required-headers":[1,21,3,4],
"content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-required-parameters":[1,21,3,5],
"content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-returns":[1,21,3,7],
"content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-supported-methods-and-overview":[1,21,3,1],
"content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-supported-roles-and-conditions":[1,21,3,3],
"content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-url-structure":[1,21,3,0],
"content-service-api-v1-versions-versionid-view.html":[1,21,4],
"content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-detailed-description":[1,21,4,2],
"content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-optional-parameters":[1,21,4,6],
"content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-required-headers":[1,21,4,4],
"content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-required-parameters":[1,21,4,5],
"content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-returns":[1,21,4,7],
"content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-supported-methods-and-overview":[1,21,4,1],
"content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-supported-roles-and-conditions":[1,21,4,3],
"content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-url-structure":[1,21,4,0],
"content-service-discovery-endpoints.html":[1,20],
"developers-guide.html":[0],
"developers-guide.html#tools-for-accessing-the-product-api":[0,1],
"developers-guide.html#what-the-covata-api-can-do":[0,0],
"file-content-handling-endpoints.html":[1,21],
"file-handling-endpoints.html":[1,3],
"folder-content-handling-endpoints.html":[1,22],
"folder-handling-endpoints.html":[1,4],
"getting-support.html":[2],
"i18n-endpoints.html":[1,19],
"important-notice.html":[3],
"index.html":[],
"item-handling-endpoints.html":[1,5],
"key-handling-endpoints.html":[1,2],
"ldap-connection-endpoints.html":[1,16],
"organisation-administering-endpoints.html":[1,15],
"organisation-administration-dashboard-endpoints.html":[1,9],
"organisation-configuration-and-information-retrieval-endpoints.html":[1,7],
"pages.html":[],
"plan-endpoints.html":[1,12],
"platform-administration-configuration-endpoints.html":[1,18],
"platform-administration-dashboard-endpoints.html":[1,14],
"retrieving-information-about-items.html":[0,4],
"retrieving-information-about-items.html#retrieving-info-about-all-ancestral-folders-of-an-item":[0,4,3],
"retrieving-information-about-items.html#retrieving-info-about-all-immediate-items-of-a-common-parent-folder":[0,4,0],
"retrieving-information-about-items.html#retrieving-info-about-all-items-filtered-by-text-search":[0,4,1],
"retrieving-information-about-items.html#retrieving-info-about-items-based-on-ownership-or-sharing-status":[0,4,2],
"retrieving-information-about-items.html#sorting-ordering-and-paginating-the-list-of-item-info":[0,4,4],
"sharing-items.html":[0,5],
"sharing-items.html#determining-existing-collaborators-on-an-item":[0,5,2],
"sharing-items.html#modifying-existing-collaborators-on-an-item":[0,5,3],
"sharing-items.html#sharing-a-file-with-collaborators":[0,5,0],
"sharing-items.html#sharing-a-folder-with-collaborators":[0,5,1],
"user-account-handling-endpoints.html":[1,6],
"user-group-endpoints.html":[1,11],
"user-whitelist-endpoints.html":[1,10],
"working-with-files-and-folders-fundamentals.html":[0,3],
"working-with-files-and-folders-fundamentals.html#collection":[0,3,0,1],
"working-with-files-and-folders-fundamentals.html#completing-a-file-object-uploading-its-locally-encrypted-data":[0,3,3],
"working-with-files-and-folders-fundamentals.html#completing-a-file-object-uploading-its-unencrypted-data":[0,3,4],
"working-with-files-and-folders-fundamentals.html#creating-a-folder-to-manage-items":[0,3,6],
"working-with-files-and-folders-fundamentals.html#definitions":[0,3,0],
"working-with-files-and-folders-fundamentals.html#downloading-a-file-objects-data":[0,3,5],
"working-with-files-and-folders-fundamentals.html#initialising-a-new-incomplete-file-object":[0,3,2],
"working-with-files-and-folders-fundamentals.html#item":[0,3,0,2],
"working-with-files-and-folders-fundamentals.html#moving-items-to-other-folders":[0,3,7],
"working-with-files-and-folders-fundamentals.html#obtaining-a-cryptographic-key":[0,3,1],
"working-with-files-and-folders-fundamentals.html#organisation":[0,3,0,3],
"working-with-files-and-folders-fundamentals.html#secure-object":[0,3,0,0]
};
