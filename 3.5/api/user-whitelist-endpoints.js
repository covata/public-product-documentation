var user_whitelist_endpoints =
[
    [ "<access-service>/api/v1/organisations/{orgId}/userwhitelist", "access-service-api-v1-organisations-orgid-userwhitelist.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-userwhitelist.html#access-service-api-v1-organisations-orgid-userwhitelist-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-userwhitelist.html#access-service-api-v1-organisations-orgid-userwhitelist-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-userwhitelist.html#access-service-api-v1-organisations-orgid-userwhitelist-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-userwhitelist.html#access-service-api-v1-organisations-orgid-userwhitelist-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-userwhitelist.html#access-service-api-v1-organisations-orgid-userwhitelist-required-headers", null ],
      [ "Required POST-request parameters", "access-service-api-v1-organisations-orgid-userwhitelist.html#access-service-api-v1-organisations-orgid-userwhitelist-required-post-request-parameters", null ],
      [ "Optional GET-request parameters", "access-service-api-v1-organisations-orgid-userwhitelist.html#access-service-api-v1-organisations-orgid-userwhitelist-optional-get-request-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-userwhitelist.html#access-service-api-v1-organisations-orgid-userwhitelist-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/userwhitelist/{whitelistId}", "access-service-api-v1-organisations-orgid-userwhitelist-whitelistid.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-userwhitelist-whitelistid.html#access-service-api-v1-organisations-orgid-userwhitelist-whitelistid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-userwhitelist-whitelistid.html#access-service-api-v1-organisations-orgid-userwhitelist-whitelistid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-userwhitelist-whitelistid.html#access-service-api-v1-organisations-orgid-userwhitelist-whitelistid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-userwhitelist-whitelistid.html#access-service-api-v1-organisations-orgid-userwhitelist-whitelistid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-userwhitelist-whitelistid.html#access-service-api-v1-organisations-orgid-userwhitelist-whitelistid-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-organisations-orgid-userwhitelist-whitelistid.html#access-service-api-v1-organisations-orgid-userwhitelist-whitelistid-required-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-userwhitelist-whitelistid.html#access-service-api-v1-organisations-orgid-userwhitelist-whitelistid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/userwhitelist/check", "access-service-api-v1-organisations-orgid-userwhitelist-check.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-userwhitelist-check.html#access-service-api-v1-organisations-orgid-userwhitelist-check-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-userwhitelist-check.html#access-service-api-v1-organisations-orgid-userwhitelist-check-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-userwhitelist-check.html#access-service-api-v1-organisations-orgid-userwhitelist-check-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-userwhitelist-check.html#access-service-api-v1-organisations-orgid-userwhitelist-check-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-userwhitelist-check.html#access-service-api-v1-organisations-orgid-userwhitelist-check-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-organisations-orgid-userwhitelist-check.html#access-service-api-v1-organisations-orgid-userwhitelist-check-required-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-userwhitelist-check.html#access-service-api-v1-organisations-orgid-userwhitelist-check-returns", null ]
    ] ],
    [ "<access-service>/api/v1/userwhitelist/{objectId}", "access-service-api-v1-userwhitelist-objectid.html", [
      [ "URL structure", "access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-required-headers", null ],
      [ "Returns", "access-service-api-v1-userwhitelist-objectid.html#access-service-api-v1-userwhitelist-objectid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/userwhitelist/bulk", "access-service-api-v1-userwhitelist-bulk.html", [
      [ "URL structure", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-required-parameters", null ],
      [ "Returns", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-returns", null ]
    ] ]
];