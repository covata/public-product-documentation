var authentication_and_authorisation_endpoints =
[
    [ "<access-service>/api/oauth/authorise", "access-service-api-oauth-authorise.html", [
      [ "URL structure", "access-service-api-oauth-authorise.html#access-service-api-oauth-authorise-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-oauth-authorise.html#access-service-api-oauth-authorise-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-oauth-authorise.html#access-service-api-oauth-authorise-detailed-description", null ],
      [ "Required parameters", "access-service-api-oauth-authorise.html#access-service-api-oauth-authorise-required-parameters", null ],
      [ "Returns", "access-service-api-oauth-authorise.html#access-service-api-authorise-token-returns", null ]
    ] ],
    [ "<access-service>/api/oauth/token", "access-service-api-oauth-token.html", [
      [ "URL structure", "access-service-api-oauth-token.html#access-service-api-oauth-token-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-oauth-token.html#access-service-api-oauth-token-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-oauth-token.html#access-service-api-oauth-token-detailed-description", null ],
      [ "Required headers", "access-service-api-oauth-token.html#access-service-api-oauth-token-required-headers", null ],
      [ "Required parameters", "access-service-api-oauth-token.html#access-service-api-oauth-token-required-parameters", null ],
      [ "Grant type-specific parameters", "access-service-api-oauth-token.html#access-service-api-oauth-token-grant-type-specific-parameters", null ],
      [ "Returns", "access-service-api-oauth-token.html#access-service-api-oauth-token-returns", null ]
    ] ]
];