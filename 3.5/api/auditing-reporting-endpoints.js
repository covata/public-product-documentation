var auditing_reporting_endpoints =
[
    [ "<access-service>/api/v1/organisations/{organisationId}/audit", "access-service-api-v1-organisations-organisationid-audit.html", [
      [ "URL structure", "access-service-api-v1-organisations-organisationid-audit.html#access-service-api-v1-organisations-organisationid-audit-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-organisationid-audit.html#access-service-api-v1-organisations-organisationid-audit-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-organisationid-audit.html#access-service-api-v1-organisations-organisationid-audit-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-organisationid-audit.html#access-service-api-v1-organisations-organisationid-audit-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-organisationid-audit.html#access-service-api-v1-organisations-organisationid-audit-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-organisations-organisationid-audit.html#access-service-api-v1-organisations-organisationid-audit-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-organisationid-audit.html#access-service-api-v1-organisations-organisationid-audit-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{organisationId}/audit/exportReport", "access-service-api-v1-organisations-organisationid-audit-exportreport.html", [
      [ "URL structure", "access-service-api-v1-organisations-organisationid-audit-exportreport.html#access-service-api-v1-organisations-organisationid-audit-exportreport-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-organisationid-audit-exportreport.html#access-service-api-v1-organisations-organisationid-audit-exportreport-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-organisationid-audit-exportreport.html#access-service-api-v1-organisations-organisationid-audit-exportreport-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-organisationid-audit-exportreport.html#access-service-api-v1-organisations-organisationid-audit-exportreport-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-organisationid-audit-exportreport.html#access-service-api-v1-organisations-organisationid-audit-exportreport-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-organisations-organisationid-audit-exportreport.html#access-service-api-v1-organisations-organisationid-audit-exportreport-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-organisationid-audit-exportreport.html#access-service-api-v1-organisations-organisationid-audit-exportreport-returns", null ]
    ] ]
];