var platform_administration_dashboard_endpoints =
[
    [ "<access-service>/api/v1/dashboard/quotas", "access-service-api-v1-dashboard-quotas.html", [
      [ "URL structure", "access-service-api-v1-dashboard-quotas.html#access-service-api-v1-dashboard-quotas-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-dashboard-quotas.html#access-service-api-v1-dashboard-quotas-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-dashboard-quotas.html#access-service-api-v1-dashboard-quotas-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-dashboard-quotas.html#access-service-api-v1-dashboard-quotas-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-dashboard-quotas.html#access-service-api-v1-dashboard-quotas-required-headers", null ],
      [ "Returns", "access-service-api-v1-dashboard-quotas.html#access-service-api-v1-dashboard-quotas-returns", null ]
    ] ],
    [ "<access-service>/api/v1/dashboard/clients", "access-service-api-v1-dashboard-clients.html", [
      [ "URL structure", "access-service-api-v1-dashboard-clients.html#access-service-api-v1-dashboard-clients-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-dashboard-clients.html#access-service-api-v1-dashboard-clients-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-dashboard-clients.html#access-service-api-v1-dashboard-clients-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-dashboard-clients.html#access-service-api-v1-dashboard-clients-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-dashboard-clients.html#access-service-api-v1-dashboard-clients-required-headers", null ],
      [ "Returns", "access-service-api-v1-dashboard-clients.html#access-service-api-v1-dashboard-clients-returns", null ]
    ] ],
    [ "<access-service>/api/v1/dashboard/ldap", "access-service-api-v1-dashboard-ldap.html", [
      [ "URL structure", "access-service-api-v1-dashboard-ldap.html#access-service-api-v1-dashboard-ldap-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-dashboard-ldap.html#access-service-api-v1-dashboard-ldap-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-dashboard-ldap.html#access-service-api-v1-dashboard-ldap-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-dashboard-ldap.html#access-service-api-v1-dashboard-ldap-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-dashboard-ldap.html#access-service-api-v1-dashboard-ldap-required-headers", null ],
      [ "Returns", "access-service-api-v1-dashboard-ldap.html#access-service-api-v1-dashboard-ldap-returns", null ]
    ] ],
    [ "<access-service>/api/v1/dashboard/users/activity", "access-service-api-v1-dashboard-users-activity.html", [
      [ "URL structure", "access-service-api-v1-dashboard-users-activity.html#access-service-api-v1-dashboard-users-activity-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-dashboard-users-activity.html#access-service-api-v1-dashboard-users-activity-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-dashboard-users-activity.html#access-service-api-v1-dashboard-users-activity-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-dashboard-users-activity.html#access-service-api-v1-dashboard-users-activity-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-dashboard-users-activity.html#access-service-api-v1-dashboard-users-activity-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-dashboard-users-activity.html#access-service-api-v1-dashboard-users-activity-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-dashboard-users-activity.html#access-service-api-v1-dashboard-users-activity-returns", null ]
    ] ],
    [ "<access-service>/api/v1/dashboard/organisations", "access-service-api-v1-dashboard-organisations.html", [
      [ "URL structure", "access-service-api-v1-dashboard-organisations.html#access-service-api-v1-dashboard-organisations-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-dashboard-organisations.html#access-service-api-v1-dashboard-organisations-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-dashboard-organisations.html#access-service-api-v1-dashboard-organisations-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-dashboard-organisations.html#access-service-api-v1-dashboard-organisations-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-dashboard-organisations.html#access-service-api-v1-dashboard-organisations-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-dashboard-organisations.html#access-service-api-v1-dashboard-organisations-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-dashboard-organisations.html#access-service-api-v1-dashboard-organisations-returns", null ]
    ] ]
];