var plan_endpoints =
[
    [ "<access-service>/api/v1/organisations/{orgGroupId}/plans", "access-service-api-v1-organisations-orggroupid-plans.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-plans.html#access-service-api-v1-organisations-orggroupid-plans-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-plans.html#access-service-api-v1-organisations-orggroupid-plans-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-plans.html#access-service-api-v1-organisations-orggroupid-plans-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orggroupid-plans.html#access-service-api-v1-organisations-orggroupid-plans-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-plans.html#access-service-api-v1-organisations-orggroupid-plans-required-headers", null ],
      [ "Required POST-request parameters", "access-service-api-v1-organisations-orggroupid-plans.html#access-service-api-v1-organisations-orggroupid-plans-required-post-request-parameters", null ],
      [ "Optional POST-request parameters", "access-service-api-v1-organisations-orggroupid-plans.html#access-service-api-v1-organisations-orggroupid-plans-optional-post-request-parameters", null ],
      [ "Optional GET-request parameters", "access-service-api-v1-organisations-orggroupid-plans.html#access-service-api-v1-organisations-orggroupid-plans-optional-get-request-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-plans.html#access-service-api-v1-organisations-orggroupid-plans-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/plans/{planId}", "access-service-api-v1-organisations-orggroupid-plans-planid.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-plans-planid.html#access-service-api-v1-organisations-orggroupid-plans-planid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-plans-planid.html#access-service-api-v1-organisations-orggroupid-plans-planid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-plans-planid.html#access-service-api-v1-organisations-orggroupid-plans-planid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orggroupid-plans-planid.html#access-service-api-v1-organisations-orggroupid-plans-planid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-plans-planid.html#access-service-api-v1-organisations-orggroupid-plans-planid-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-organisations-orggroupid-plans-planid.html#access-service-api-v1-organisations-orggroupid-plans-planid-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-plans-planid.html#access-service-api-v1-organisations-orggroupid-plans-planid-returns", null ]
    ] ]
];