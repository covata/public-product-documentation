var NAVTREEINDEX0 =
{
"accessing-organisation-administration.html":[8,0],
"accessing-organisation-administration.html#signing-in-to-organisation-administration":[8,0,0],
"accessing-organisation-administration.html#signing-out-of-organisation-administration":[8,0,3],
"accessing-organisation-administration.html#the-organisation-administration-dashboard":[8,0,2],
"accessing-organisation-administration.html#the-organisation-administration-interface":[8,0,1],
"accessing-platform-administration.html":[0],
"accessing-platform-administration.html#accessing-platform-administration-for-the-first-time":[0,4],
"accessing-platform-administration.html#signing-in-to-platform-administration":[0,0],
"accessing-platform-administration.html#signing-out-of-platform-administration":[0,3],
"accessing-platform-administration.html#the-platform-administration-dashboard":[0,2],
"accessing-platform-administration.html#the-platform-administration-interface":[0,1],
"administering-files.html":[8,7],
"administering-files.html#a-file-objects-fields":[8,7,1],
"administering-files.html#disabling-or-re-enabling-a-file":[8,7,3],
"administering-files.html#finding-files":[8,7,0],
"administering-files.html#viewing-activities-on-files":[8,7,2],
"administering-organisations.html":[2],
"administering-organisations.html#adding-a-new-organisation":[2,0],
"administering-organisations.html#editing-an-existing-organisation":[2,2],
"administering-organisations.html#finding-existing-organisations":[2,1],
"administering-organisations.html#removing-an-organisation":[2,3],
"administering-platform-administrator-users.html":[1],
"administering-platform-administrator-users.html#a-platform-administrator-users-fields":[1,0],
"administering-platform-administrator-users.html#adding-a-platform-administrator-account":[1,2],
"administering-platform-administrator-users.html#changing-your-platform-administrator-account-password":[1,5],
"administering-platform-administrator-users.html#editing-your-platform-administrator-account":[1,4],
"administering-platform-administrator-users.html#enabling-or-disabling-2fa-for-a-platform-administrator":[1,7],
"administering-platform-administrator-users.html#platform-administrators-and-user-roles":[1,1],
"administering-platform-administrator-users.html#re-configuring-2fa-for-a-platform-administrator":[1,8],
"administering-platform-administrator-users.html#removing-platform-administrators":[1,3],
"administering-platform-administrator-users.html#terminating-your-platform-administrator-accounts-sessions":[1,6],
"administering-users-within-an-organisation.html":[8,1],
"administering-users-within-an-organisation.html#adding-an-organisation-user-account":[8,1,3],
"administering-users-within-an-organisation.html#adding-organisation-user-accounts-in-bulk-from-csv":[8,1,4],
"administering-users-within-an-organisation.html#an-organisation-users-activities":[8,1,2],
"administering-users-within-an-organisation.html#an-organisation-users-fields":[8,1,0],
"administering-users-within-an-organisation.html#an-organisation-users-roles":[8,1,1],
"administering-users-within-an-organisation.html#changing-your-organisation-administrator-account-password":[8,1,9],
"administering-users-within-an-organisation.html#configuring-users-page-columns-for-organisation-users":[8,1,13],
"administering-users-within-an-organisation.html#editing-an-organisation-user-account":[8,1,6],
"administering-users-within-an-organisation.html#editing-your-organisation-administrator-account":[8,1,8],
"administering-users-within-an-organisation.html#enabling-or-disabling-2fa-on-organisation-user-accounts":[8,1,11],
"administering-users-within-an-organisation.html#finding-user-accounts-within-your-organisation":[8,1,5],
"administering-users-within-an-organisation.html#re-configuring-2fa-for-an-organisation-user-account":[8,1,12],
"administering-users-within-an-organisation.html#removing-a-user-account-from-your-organisation":[8,1,7],
"administering-users-within-an-organisation.html#terminating-your-organisation-administrator-accounts-sessions":[8,1,10],
"appendix.html":[9],
"configuring-client-applications.html":[5],
"configuring-client-applications.html#disabling-or-re-enabling-a-client-application":[5,2],
"configuring-client-applications.html#editing-a-registered-client-application":[5,1],
"configuring-client-applications.html#registering-a-new-client-application":[5,0],
"configuring-ldap.html":[3],
"configuring-ldap.html#adding-a-new-ldap-connection":[3,0],
"configuring-ldap.html#disabling-or-re-enabling-an-ldap-connection":[3,2],
"configuring-ldap.html#editing-an-existing-ldap-connection":[3,1],
"configuring-organisation-administration-properties.html":[8,9],
"configuring-organisation-administration-properties.html#modifying-an-organisation-administration-property-value":[8,9,0],
"configuring-organisation-administration-properties.html#organisation-administration-properties":[8,9,1],
"configuring-platform-administration-properties.html":[6],
"configuring-platform-administration-properties.html#analytics-properties":[6,1,1],
"configuring-platform-administration-properties.html#application-links-properties":[6,1,2],
"configuring-platform-administration-properties.html#file-limits-properties":[6,1,4],
"configuring-platform-administration-properties.html#modifying-a-platform-administration-property-value":[6,0],
"configuring-platform-administration-properties.html#notifications-properties":[6,1,5],
"configuring-platform-administration-properties.html#platform-administration-properties":[6,1],
"configuring-platform-administration-properties.html#system-details-properties":[6,1,6],
"configuring-platform-administration-properties.html#user-authentication-properties":[6,1,0],
"configuring-platform-administration-properties.html#user-limits-properties":[6,1,3],
"configuring-single-sign-on-using-an-idp.html":[4],
"configuring-single-sign-on-using-an-idp.html#adding-an-idp-service-configuration":[4,0],
"configuring-single-sign-on-using-an-idp.html#disabling-or-re-enabling-the-idp-service-configuration":[4,2],
"configuring-single-sign-on-using-an-idp.html#editing-the-idp-service-configuration":[4,1],
"generating-reports.html":[8,8],
"generating-reports.html#activity-fields":[8,8,0],
"generating-reports.html#generating-a-report":[8,8,1],
"getting-support.html":[10],
"important-notice.html":[11],
"index.html":[],
"managing-classifications.html":[8,4],
"managing-classifications.html#adding-a-new-classification":[8,4,0],
"managing-classifications.html#removing-an-existing-classification":[8,4,1],
"managing-clearances.html":[8,5],
"managing-clearances.html#adding-a-new-clearance":[8,5,0],
"managing-clearances.html#editing-an-existing-clearance":[8,5,3],
"managing-clearances.html#modifying-classifications-on-an-existing-clearance":[8,5,2],
"managing-clearances.html#modifying-users-in-an-existing-clearance":[8,5,1],
"managing-clearances.html#removing-an-existing-clearance":[8,5,4],
"managing-contact-groups.html":[8,2],
"managing-contact-groups.html#adding-a-new-contact-group":[8,2,0],
"managing-contact-groups.html#editing-an-existing-contact-group":[8,2,1],
"managing-contact-groups.html#removing-an-existing-contact-group":[8,2,2],
"managing-internationalisation.html":[7],
"managing-internationalisation.html#changing-the-system-default-language":[7,0],
"managing-internationalisation.html#deleting-or-resetting-a-language-bundle":[7,3],
"managing-internationalisation.html#exporting-a-language-bundle":[7,1],
"managing-internationalisation.html#uploading-a-customised-external-language-bundle":[7,2],
"managing-safe-share-for-ios-through-mdm-software.html":[9,1],
"managing-safe-share-for-ios-through-mdm-software.html#step-1-install-safe-share-for-ios-via-mdm-software":[9,1,0],
"managing-safe-share-for-ios-through-mdm-software.html#step-2-configure-safe-share-for-ios-via-mdm-software":[9,1,1],
"managing-safe-share-for-ios-through-mdm-software.html#the-mdm-configuration-plist-file":[9,1,2],
"managing-user-whitelists.html":[8,3],
"managing-user-whitelists.html#adding-new-user-whitelist-rules":[8,3,0],
"managing-user-whitelists.html#editing-an-existing-user-whitelist-rules-description":[8,3,1],
"managing-user-whitelists.html#removing-an-existing-user-whitelist-rule":[8,3,2],
"managing-users-storage-quotas-through-plans.html":[8,6],
"managing-users-storage-quotas-through-plans.html#adding-a-new-plan":[8,6,0],
"managing-users-storage-quotas-through-plans.html#editing-an-existing-plan":[8,6,1],
"managing-users-storage-quotas-through-plans.html#removing-an-existing-plan":[8,6,2],
"organisation-administration.html":[8],
"pages.html":[],
"theming-the-applications-web-based-interfaces.html":[9,0],
"theming-the-applications-web-based-interfaces.html#other-themable-application-areas":[9,0,2],
"theming-the-applications-web-based-interfaces.html#themable-areas-of-the-web-application":[9,0,0],
"theming-the-applications-web-based-interfaces.html#themable-web-based-administration-areas":[9,0,1]
};
