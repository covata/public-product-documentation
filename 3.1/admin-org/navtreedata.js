var NAVTREE =
[
  [ "Organization Administrator's Guide for Safe Share 3.1", "index.html", [
    [ "Accessing Safe Share Organization Administration", "accessing-organisation-administration.html", [
      [ "Signing in to Organization Administration", "accessing-organisation-administration.html#signing-in-to-organisation-administration", null ],
      [ "The Organization Administration interface", "accessing-organisation-administration.html#the-organisation-administration-interface", null ],
      [ "The Organization Administration dashboard", "accessing-organisation-administration.html#the-organisation-administration-dashboard", null ],
      [ "Signing out of Organization Administration", "accessing-organisation-administration.html#signing-out-of-organisation-administration", null ]
    ] ],
    [ "Administering users within an organization", "administering-users-within-an-organisation.html", [
      [ "An organization user's fields", "administering-users-within-an-organisation.html#an-organisation-users-fields", null ],
      [ "An organization user's roles", "administering-users-within-an-organisation.html#an-organisation-users-roles", null ],
      [ "Adding an organization user account", "administering-users-within-an-organisation.html#adding-an-organisation-user-account", null ],
      [ "Adding organization user accounts in bulk (from CSV)", "administering-users-within-an-organisation.html#adding-organisation-user-accounts-in-bulk-from-csv", null ],
      [ "Finding user accounts within your organization", "administering-users-within-an-organisation.html#finding-user-accounts-within-your-organisation", null ],
      [ "Editing an organization user account", "administering-users-within-an-organisation.html#editing-an-organisation-user-account", null ],
      [ "Removing a user account from your organization", "administering-users-within-an-organisation.html#removing-a-user-account-from-your-organisation", null ],
      [ "Editing your Organization administrator account", "administering-users-within-an-organisation.html#editing-your-organisation-administrator-account", null ],
      [ "Changing your Organization administrator account password", "administering-users-within-an-organisation.html#changing-your-organisation-administrator-account-password", null ],
      [ "Terminating your Organization administrator account's sessions", "administering-users-within-an-organisation.html#terminating-your-organisation-administrator-accounts-sessions", null ],
      [ "Enabling or disabling 2FA on Organization user accounts", "administering-users-within-an-organisation.html#enabling-or-disabling-2fa-on-organisation-user-accounts", null ],
      [ "Re-configuring 2FA for an organisation user account", "administering-users-within-an-organisation.html#re-configuring-2fa-for-an-organisation-user-account", null ],
      [ "Configuring 'Users' page columns for organization users", "administering-users-within-an-organisation.html#configuring-users-page-columns-for-organisation-users", null ]
    ] ],
    [ "Managing user whitelists", "managing-user-whitelists.html", [
      [ "Managing the user whitelist", "managing-user-whitelists.html#managing-the-user-whitelist", null ],
      [ "Adding new user whitelist rules", "managing-user-whitelists.html#adding-new-user-whitelist-rules", null ],
      [ "Editing an existing user whitelist rule's description", "managing-user-whitelists.html#editing-an-existing-user-whitelist-rules-description", null ],
      [ "Removing an existing user whitelist rule", "managing-user-whitelists.html#removing-an-existing-user-whitelist-rule", null ]
    ] ],
    [ "Managing classifications", "managing-classifications.html", [
      [ "Adding a new classification", "managing-classifications.html#adding-a-new-classification", null ],
      [ "Removing an existing classification", "managing-classifications.html#removing-an-existing-classification", null ]
    ] ],
    [ "Managing groups", "managing-groups.html", [
      [ "Adding a new group", "managing-groups.html#adding-a-new-group", null ],
      [ "Modifying users in an existing group", "managing-groups.html#modifying-users-in-an-existing-group", null ],
      [ "Modifying classifications on an existing group", "managing-groups.html#modifying-classifications-on-an-existing-group", null ],
      [ "Editing an existing group", "managing-groups.html#editing-an-existing-group", null ],
      [ "Removing an existing group", "managing-groups.html#removing-an-existing-group", null ]
    ] ],
    [ "Managing users' storage quotas through plans", "managing-users-storage-quotas-through-plans.html", [
      [ "Adding a new plan", "managing-users-storage-quotas-through-plans.html#adding-a-new-plan", null ],
      [ "Editing an existing plan", "managing-users-storage-quotas-through-plans.html#editing-an-existing-plan", null ],
      [ "Removing an existing plan", "managing-users-storage-quotas-through-plans.html#removing-an-existing-plan", null ]
    ] ],
    [ "Administering files", "administering-files.html", [
      [ "Finding Secure Objects", "administering-files.html#finding-secure-objects", null ],
      [ "A Secure Object's fields", "administering-files.html#a-secure-objects-fields", null ],
      [ "Viewing activities on Secure Objects", "administering-files.html#viewing-activities-on-secure-objects", null ],
      [ "Disabling or (re-)enabling a Secure Object", "administering-files.html#disabling-or-re-enabling-secure-objects", null ]
    ] ],
    [ "Generating reports", "generating-reports.html", [
      [ "Activity fields", "generating-reports.html#activity-fields", null ],
      [ "Generating a report", "generating-reports.html#generating-a-report", null ]
    ] ],
    [ "Configuring Safe Share Organization Administration properties", "configuring-organisation-administration-properties.html", [
      [ "Modifying a Safe Share Organization Administration property value", "configuring-organisation-administration-properties.html#modifying-an-organisation-administration-property-value", null ]
    ] ],
    [ "Appendix", "appendix.html", [
      [ "Managing Safe Share for iOS through MDM software", "appendix.html#managing-safe-share-for-ios-through-mdm-software", [
        [ "Step 1 - Install Safe Share for iOS via MDM software", "appendix.html#step-1-install-safe-share-for-ios-via-mdm-software", null ],
        [ "Step 2 - Configure Safe Share for iOS via MDM software", "appendix.html#step-2-configure-safe-share-for-ios-via-mdm-software", null ],
        [ "The 'mdm-configuration.plist' file", "appendix.html#the-mdm-configuration-plist-file", null ]
      ] ]
    ] ],
    [ "Getting Support", "getting-support.html", null ],
    [ "Important notice", "important-notice.html", null ],
    [ "Covata Documentation Home", "../../", null ]
  ] ]
];

var NAVTREEINDEX =
[
"../../"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';