var NAVTREE =
[
  [ "Safe Share for Windows 3.1 User's Guide", "index.html", [
    [ "Installing and upgrading Safe Share for Windows", "installing-and-upgrading-the-application.html", [
      [ "System requirements", "installing-and-upgrading-the-application.html#system-requirements", null ],
      [ "Installing", "installing-and-upgrading-the-application.html#installing", null ],
      [ "Configuring your Covata Platform instance", "installing-and-upgrading-the-application.html#configuring-your-server-url", null ],
      [ "Upgrading", "installing-and-upgrading-the-application.html#upgrading", null ],
      [ "Uninstalling", "installing-and-upgrading-the-application.html#uninstalling", null ],
      [ "Modifying the Covata Platform URL", "installing-and-upgrading-the-application.html#modifying-the-server-url", null ]
    ] ],
    [ "Starting Safe Share for Windows and signing in", "starting-the-application-and-signing-in.html", [
      [ "Starting Safe Share for Windows", "starting-the-application-and-signing-in.html#starting-the-application", null ],
      [ "Signing in manually", "starting-the-application-and-signing-in.html#signing-in-manually", null ],
      [ "Configuring a mobile device for two-factor authentication", "starting-the-application-and-signing-in.html#configuring-a-mobile-device-for-two-factor-authentication", null ],
      [ "Accessing the web application", "starting-the-application-and-signing-in.html#accessing-the-web-application", null ],
      [ "Signing out", "starting-the-application-and-signing-in.html#signing-out", null ],
      [ "Exiting Safe Share for Windows", "starting-the-application-and-signing-in.html#exiting-the-application", null ]
    ] ],
    [ "Uploading, storing and sharing content securely", "uploading-storing-and-sharing-content-securely.html", [
      [ "Accessing the 'Safe Share' folder", "uploading-storing-and-sharing-content-securely.html#accessing-the-applications-folder", null ],
      [ "Changing organizations", "uploading-storing-and-sharing-content-securely.html#changing-organisations", null ],
      [ "Uploading and storing personally owned content", "uploading-storing-and-sharing-content-securely.html#uploading-and-storing-personally-owned-content", null ],
      [ "Sharing secure content", "uploading-storing-and-sharing-content-securely.html#sharing-secure-content", null ],
      [ "Uploading, storing and sharing content 'in one go'", "uploading-storing-and-sharing-content-securely.html#uploading-storing-and-sharing-content-in-one-go", null ],
      [ "Uploading content to share through another user's folder", "uploading-storing-and-sharing-content-securely.html#uploading-content-to-share-through-another-users-folder", null ]
    ] ],
    [ "Accessing secure content", "accessing-secure-content.html", [
      [ "Viewing/printing/downloading your own content", "accessing-secure-content.html#viewing-printing-downloading-your-own-content", null ],
      [ "Viewing/printing/downloading content (shared with you)", "accessing-secure-content.html#viewing-printing-downloading-content-shared-with-you", null ],
      [ "Safe Share for Windows' read-only content viewer", "accessing-secure-content.html#read-only-content-viewer", null ]
    ] ],
    [ "Managing secure content", "managing-secure-content.html", [
      [ "Finding secure content", "managing-secure-content.html#finding-secure-content", null ],
      [ "Organizing content with folders", "managing-secure-content.html#organising-content-with-folders", null ],
      [ "Unsharing secure content", "managing-secure-content.html#unsharing-secure-content", null ],
      [ "Renaming an item", "managing-secure-content.html#renaming-an-item", null ],
      [ "Removing personally owned content", "managing-secure-content.html#removing-personally-owned-content", null ],
      [ "Removing content shared through another user's folder", "managing-secure-content.html#removing-content-shared-through-another-users-folder", null ],
      [ "Removing your access to content (shared with you)", "managing-secure-content.html#removing-your-access-to-content-shared-with-you", null ]
    ] ],
    [ "Managing cache policy settings", "managing-cache-policy-settings.html", [
      [ "Accessing cache policy settings", "managing-cache-policy-settings.html#accessing-cache-policy-settings", null ],
      [ "Modifying cache policy settings", "managing-cache-policy-settings.html#modifying-cache-policy-settings", null ]
    ] ],
    [ "Managing your account", "managing-your-account.html", [
      [ "Managing your contacts", "managing-your-account.html#managing-your-contacts", null ]
    ] ],
    [ "Getting support", "getting-support.html", null ],
    [ "Important notice", "important-notice.html", null ],
    [ "Covata Documentation Home", "../../", null ]
  ] ]
];

var NAVTREEINDEX =
[
"../../"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';