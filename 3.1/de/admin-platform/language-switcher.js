$(function() {
    // Bind change event to select drop-down
    $('#languageselectdropdown').on('change', function() {
        var baseurl = $(this).val(); // get selected value
        var currenturl = window.location.href; // obtain the current page's URL
        var pageanchor = currenturl.substr(currenturl.lastIndexOf('/') + 1); // obtain only everything downstream of final slash - i.e. HTML page (+ anchor)
        // require a URL
        if (baseurl) {
            window.location = baseurl + pageanchor; // redirect browser to new baseurl + existing HTML page (+ anchor)
        }
        return false;
    });
});
