var organisation_administration =
[
    [ "Zugriff auf die Safe Share-Organisationsverwaltung", "accessing-organisation-administration.html", [
      [ "Anmeldung bei der Organisationsverwaltung", "accessing-organisation-administration.html#signing-in-to-organisation-administration", null ],
      [ "Die Oberfläche Organisationsverwaltung", "accessing-organisation-administration.html#the-organisation-administration-interface", null ],
      [ "Das Dashboard der Organisationsverwaltung", "accessing-organisation-administration.html#the-organisation-administration-dashboard", null ],
      [ "Abmeldung von der Organisationsverwaltung", "accessing-organisation-administration.html#signing-out-of-organisation-administration", null ]
    ] ],
    [ "Verwaltung von Benutzern innerhalb einer Organisation", "administering-users-within-an-organisation.html", [
      [ "Die Benutzerfelder einer Organisation", "administering-users-within-an-organisation.html#an-organisation-users-fields", null ],
      [ "Die Benutzerrollen einer Organisation", "administering-users-within-an-organisation.html#an-organisation-users-roles", null ],
      [ "Hinzufügen eines Benutzer-Accounts für eine Organisation", "administering-users-within-an-organisation.html#adding-an-organisation-user-account", null ],
      [ "Hinzufügen mehrerer Benutzer-Accounts für eine Organisation in einem Schritt (von einer CSV-Datei)", "administering-users-within-an-organisation.html#adding-organisation-user-accounts-in-bulk-from-csv", null ],
      [ "Suche nach Benutzer-Accounts in Ihrer Organisation", "administering-users-within-an-organisation.html#finding-user-accounts-within-your-organisation", null ],
      [ "Bearbeitung eines Benutzer-Accounts für eine Organisation", "administering-users-within-an-organisation.html#editing-an-organisation-user-account", null ],
      [ "Entfernen eines Benutzer-Accounts aus Ihrer Organisation", "administering-users-within-an-organisation.html#removing-a-user-account-from-your-organisation", null ],
      [ "Bearbeiten Ihres Organisations-Administrator-Accounts", "administering-users-within-an-organisation.html#editing-your-organisation-administrator-account", null ],
      [ "Änderung des Passworts für Ihren Organisations-Administrator-Account", "administering-users-within-an-organisation.html#changing-your-organisation-administrator-account-password", null ],
      [ "Beendigung von Sitzungen Ihres Organisations-Administrator-Accounts", "administering-users-within-an-organisation.html#terminating-your-organisation-administrator-accounts-sessions", null ],
      [ "Aktivierung und Deaktivierung von 2FA für Benutzer-Accounts einer Organisation", "administering-users-within-an-organisation.html#enabling-or-disabling-2fa-on-organisation-user-accounts", null ],
      [ "Neukonfiguration von 2FA für einen Benutzer-Account in einer Organisation", "administering-users-within-an-organisation.html#re-configuring-2fa-for-an-organisation-user-account", null ],
      [ "Konfiguration von Spalten auf der Seite „Benutzer“ für Organisations-Benutzer", "administering-users-within-an-organisation.html#configuring-users-page-columns-for-organisation-users", null ]
    ] ],
    [ "Verwaltung von Benutzer-Positivlisten", "managing-user-whitelists.html", [
      [ "Verwaltung der Benutzer-Positivliste", "managing-user-whitelists.html#managing-the-user-whitelist", null ],
      [ "Hinzufügen neuer Regeln für die Benutzer-Positivliste", "managing-user-whitelists.html#adding-new-user-whitelist-rules", null ],
      [ "Bearbeitung der Beschreibung einer bestehenden Regel für die Benutzer-Positivliste", "managing-user-whitelists.html#editing-an-existing-user-whitelist-rules-description", null ],
      [ "Entfernen einer bestehenden Regel für die Benutzer-Positivliste", "managing-user-whitelists.html#removing-an-existing-user-whitelist-rule", null ]
    ] ],
    [ "Verwaltung von Klassifikationen", "managing-classifications.html", [
      [ "Hinzufügen einer neuen Klassifikation", "managing-classifications.html#adding-a-new-classification", null ],
      [ "Entfernen einer bestehenden Klassifikation", "managing-classifications.html#removing-an-existing-classification", null ]
    ] ],
    [ "Verwaltung von Gruppen", "managing-groups.html", [
      [ "Hinzufügen einer neuen Gruppe", "managing-groups.html#adding-a-new-group", null ],
      [ "Änderung der Benutzer in einer bestehenden Gruppe", "managing-groups.html#modifying-users-in-an-existing-group", null ],
      [ "Änderung der Klassifikationen einer bestehenden Gruppe", "managing-groups.html#modifying-classifications-on-an-existing-group", null ],
      [ "Bearbeiten einer bestehenden Gruppe", "managing-groups.html#editing-an-existing-group", null ],
      [ "Entfernen einer bestehenden Gruppe", "managing-groups.html#removing-an-existing-group", null ]
    ] ],
    [ "Verwaltung der Speicherkontingente von Benutzern anhand von Plänen", "managing-users-storage-quotas-through-plans.html", [
      [ "Hinzufügen eines neuen Plans", "managing-users-storage-quotas-through-plans.html#adding-a-new-plan", null ],
      [ "Bearbeitung eines bestehenden Plans", "managing-users-storage-quotas-through-plans.html#editing-an-existing-plan", null ],
      [ "Entfernen eines bestehenden Plans", "managing-users-storage-quotas-through-plans.html#removing-an-existing-plan", null ]
    ] ],
    [ "Verwaltung von Dateien", "administering-files.html", [
      [ "Auffinden sicherer Objekte", "administering-files.html#finding-secure-objects", null ],
      [ "Felder eines sicheren Objekts", "administering-files.html#a-secure-objects-fields", null ],
      [ "Anzeige von an sicheren Objekten ausgeführten Aktivitäten", "administering-files.html#viewing-activities-on-secure-objects", null ],
      [ "Deaktivierung und (Re-)Aktivierung sicherer Objekte", "administering-files.html#disabling-or-re-enabling-secure-objects", null ]
    ] ],
    [ "Erzeugung von Berichten", "generating-reports.html", [
      [ "Aktivitätsfelder", "generating-reports.html#activity-fields", null ],
      [ "Erzeugung eines Berichts", "generating-reports.html#generating-a-report", null ]
    ] ],
    [ "Konfiguration von Eigenschaften der Safe Share-Organisationsverwaltung", "configuring-organisation-administration-properties.html", [
      [ "Änderung eines Eigenschaftswerts der Safe Share-Organisationsverwaltung", "configuring-organisation-administration-properties.html#modifying-an-organisation-administration-property-value", null ]
    ] ]
];