var NAVTREE =
[
  [ "Safe Share Administrator's Guide for Safe Share 3.1", "index.html", [
    [ "Accessing Safe Share Administration", "accessing-platform-administration.html", [
      [ "Signing in to Safe Share Administration", "accessing-platform-administration.html#signing-in-to-platform-administration", null ],
      [ "The Safe Share Administration interface", "accessing-platform-administration.html#the-platform-administration-interface", null ],
      [ "The Safe Share Administration dashboard", "accessing-platform-administration.html#the-platform-administration-dashboard", null ],
      [ "Signing out of Safe Share Administration", "accessing-platform-administration.html#signing-out-of-platform-administration", null ],
      [ "Accessing Safe Share Administration for the first time", "accessing-platform-administration.html#accessing-platform-administration-for-the-first-time", null ]
    ] ],
    [ "Administering Safe Share administrator users", "administering-platform-administrator-users.html", [
      [ "A Safe Share administrator user's fields", "administering-platform-administrator-users.html#a-platform-administrator-users-fields", null ],
      [ "Safe Share administrators and user roles", "administering-platform-administrator-users.html#platform-administrators-and-user-roles", null ],
      [ "Adding a Safe Share administrator account", "administering-platform-administrator-users.html#adding-a-platform-administrator-account", null ],
      [ "Removing Safe Share administrators", "administering-platform-administrator-users.html#removing-platform-administrators", null ],
      [ "Editing your Safe Share administrator account", "administering-platform-administrator-users.html#editing-your-platform-administrator-account", null ],
      [ "Changing your Safe Share administrator account password", "administering-platform-administrator-users.html#changing-your-platform-administrator-account-password", null ],
      [ "Terminating your Safe Share administrator account's sessions", "administering-platform-administrator-users.html#terminating-your-platform-administrator-accounts-sessions", null ],
      [ "Enabling or disabling 2FA for a Safe Share administrator", "administering-platform-administrator-users.html#enabling-or-disabling-2fa-for-a-platform-administrator", null ],
      [ "Re-configuring 2FA for a Safe Share administrator", "administering-platform-administrator-users.html#re-configuring-2fa-for-a-platform-administrator", null ]
    ] ],
    [ "Administering organizations", "administering-organisations.html", [
      [ "Administering an organization", "administering-organisations.html#administering-an-organisation", null ],
      [ "Adding a new organization", "administering-organisations.html#adding-a-new-organisation", null ],
      [ "Finding existing organizations", "administering-organisations.html#finding-existing-organisations", null ],
      [ "Editing an existing organization", "administering-organisations.html#editing-an-existing-organisation", null ],
      [ "Removing an organization", "administering-organisations.html#removing-an-organisation", null ]
    ] ],
    [ "Configuring LDAP", "configuring-ldap.html", [
      [ "Configuring an LDAP connection", "configuring-ldap.html#configuring-an-ldap-connection", null ],
      [ "Adding a new LDAP connection", "configuring-ldap.html#adding-a-new-ldap-connection", null ],
      [ "Editing an existing LDAP connection", "configuring-ldap.html#editing-an-existing-ldap-connection", null ],
      [ "Disabling or re-enabling an LDAP connection", "configuring-ldap.html#disabling-or-re-enabling-an-ldap-connection", null ]
    ] ],
    [ "Configuring single sign-on using an IdP", "configuring-single-sign-on-using-an-idp.html", [
      [ "Configuring an IdP service", "configuring-single-sign-on-using-an-idp.html#configuring-an-idp-service", null ],
      [ "Adding an IdP service configuration", "configuring-single-sign-on-using-an-idp.html#adding-an-idp-service-configuration", null ],
      [ "Editing the IdP service configuration", "configuring-single-sign-on-using-an-idp.html#editing-the-idp-service-configuration", null ],
      [ "Disabling or re-enabling the IdP service configuration", "configuring-single-sign-on-using-an-idp.html#disabling-or-re-enabling-the-idp-service-configuration", null ]
    ] ],
    [ "Configuring client applications", "configuring-client-applications.html", [
      [ "Configuring a client application", "configuring-client-applications.html#configuring-a-client-application", null ],
      [ "Registering a new client application", "configuring-client-applications.html#registering-a-new-client-application", null ],
      [ "Editing a registered client application", "configuring-client-applications.html#editing-a-registered-client-application", null ],
      [ "Disabling and re-enabling a client application", "configuring-client-applications.html#disabling-or-re-enabling-a-client-application", null ]
    ] ],
    [ "Configuring Safe Share Administration properties", "configuring-platform-administration-properties.html", [
      [ "Modifying a Safe Share Administration property value", "configuring-platform-administration-properties.html#modifying-a-platform-administration-property-value", null ]
    ] ],
    [ "Re-theming the Safe Share web app. and admin. areas", "re-theming-the-application-and-administration.html", [
      [ "Modifying a theme property value", "re-theming-the-application-and-administration.html#modifying-a-theme-property-value", null ],
      [ "Modifying a logo", "re-theming-the-application-and-administration.html#modifying-a-logo", null ],
      [ "Sign-in page logo", "re-theming-the-application-and-administration.html#sign-in-page-logo", null ],
      [ "Safe Share web application logos, name and color", "re-theming-the-application-and-administration.html#the-web-application-logos-name-and-color", null ]
    ] ],
    [ "Managing internationalization", "managing-internationalization.html", [
      [ "Changing the 'System Default' language", "managing-internationalization.html#changing-the-system-default-language", null ],
      [ "Exporting a language bundle", "managing-internationalization.html#exporting-a-language-bundle", null ],
      [ "Uploading a customized/external language bundle", "managing-internationalization.html#uploading-a-customised-external-language-bundle", null ],
      [ "Deleting or resetting a language bundle", "managing-internationalization.html#deleting-or-resetting-a-language-bundle", null ]
    ] ],
    [ "Safe Share Organization Administration", "organisation-administration.html", "organisation-administration" ],
    [ "Appendix", "appendix.html", [
      [ "Managing Safe Share for iOS through MDM software", "appendix.html#managing-safe-share-for-ios-through-mdm-software", [
        [ "Step 1 - Install Safe Share for iOS via MDM software", "appendix.html#step-1-install-safe-share-for-ios-via-mdm-software", null ],
        [ "Step 2 - Configure Safe Share for iOS via MDM software", "appendix.html#step-2-configure-safe-share-for-ios-via-mdm-software", null ],
        [ "The 'mdm-configuration.plist' file", "appendix.html#the-mdm-configuration-plist-file", null ]
      ] ]
    ] ],
    [ "Getting Support", "getting-support.html", null ],
    [ "Important notice", "important-notice.html", null ],
    [ "Covata Documentation Home", "../../", null ]
  ] ]
];

var NAVTREEINDEX =
[
"../../"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';