var NAVTREEINDEX0 =
{
"../../":[12],
"accessing-organisation-administration.html":[0],
"accessing-organisation-administration.html#signing-in-to-organisation-administration":[0,0],
"accessing-organisation-administration.html#signing-out-of-organisation-administration":[0,3],
"accessing-organisation-administration.html#the-organisation-administration-dashboard":[0,2],
"accessing-organisation-administration.html#the-organisation-administration-interface":[0,1],
"administering-files.html":[6],
"administering-files.html#a-secure-objects-fields":[6,1],
"administering-files.html#disabling-or-re-enabling-secure-objects":[6,3],
"administering-files.html#finding-secure-objects":[6,0],
"administering-files.html#viewing-activities-on-secure-objects":[6,2],
"administering-users-within-an-organisation.html":[1],
"administering-users-within-an-organisation.html#adding-an-organisation-user-account":[1,2],
"administering-users-within-an-organisation.html#adding-organisation-user-accounts-in-bulk-from-csv":[1,3],
"administering-users-within-an-organisation.html#an-organisation-users-fields":[1,0],
"administering-users-within-an-organisation.html#an-organisation-users-roles":[1,1],
"administering-users-within-an-organisation.html#changing-your-organisation-administrator-account-password":[1,8],
"administering-users-within-an-organisation.html#configuring-users-page-columns-for-organisation-users":[1,12],
"administering-users-within-an-organisation.html#editing-an-organisation-user-account":[1,5],
"administering-users-within-an-organisation.html#editing-your-organisation-administrator-account":[1,7],
"administering-users-within-an-organisation.html#enabling-or-disabling-2fa-on-organisation-user-accounts":[1,10],
"administering-users-within-an-organisation.html#finding-user-accounts-within-your-organisation":[1,4],
"administering-users-within-an-organisation.html#re-configuring-2fa-for-an-organisation-user-account":[1,11],
"administering-users-within-an-organisation.html#removing-a-user-account-from-your-organisation":[1,6],
"administering-users-within-an-organisation.html#terminating-your-organisation-administrator-accounts-sessions":[1,9],
"appendix.html":[9],
"configuring-organisation-administration-properties.html":[8],
"configuring-organisation-administration-properties.html#modifying-an-organisation-administration-property-value":[8,0],
"configuring-organisation-administration-properties.html#organisation-administration-properties":[8,1],
"generating-reports.html":[7],
"generating-reports.html#activity-fields":[7,0],
"generating-reports.html#generating-a-report":[7,1],
"getting-support.html":[10],
"important-notice.html":[11],
"index.html":[],
"managing-classifications.html":[3],
"managing-classifications.html#adding-a-new-classification":[3,0],
"managing-classifications.html#removing-an-existing-classification":[3,1],
"managing-groups.html":[4],
"managing-groups.html#adding-a-new-group":[4,0],
"managing-groups.html#editing-an-existing-group":[4,3],
"managing-groups.html#modifying-classifications-on-an-existing-group":[4,2],
"managing-groups.html#modifying-users-in-an-existing-group":[4,1],
"managing-groups.html#removing-an-existing-group":[4,4],
"managing-safe-share-for-ios-through-mdm-software.html":[9,1],
"managing-safe-share-for-ios-through-mdm-software.html#step-1-install-safe-share-for-ios-via-mdm-software":[9,1,0],
"managing-safe-share-for-ios-through-mdm-software.html#step-2-configure-safe-share-for-ios-via-mdm-software":[9,1,1],
"managing-safe-share-for-ios-through-mdm-software.html#the-mdm-configuration-plist-file":[9,1,2],
"managing-user-whitelists.html":[2],
"managing-user-whitelists.html#adding-new-user-whitelist-rules":[2,1],
"managing-user-whitelists.html#editing-an-existing-user-whitelist-rules-description":[2,2],
"managing-user-whitelists.html#managing-the-user-whitelist":[2,0],
"managing-user-whitelists.html#removing-an-existing-user-whitelist-rule":[2,3],
"managing-users-storage-quotas-through-plans.html":[5],
"managing-users-storage-quotas-through-plans.html#adding-a-new-plan":[5,0],
"managing-users-storage-quotas-through-plans.html#editing-an-existing-plan":[5,1],
"managing-users-storage-quotas-through-plans.html#removing-an-existing-plan":[5,2],
"pages.html":[],
"theming-the-applications-web-based-interfaces.html":[9,0],
"theming-the-applications-web-based-interfaces.html#other-themable-application-areas":[9,0,2],
"theming-the-applications-web-based-interfaces.html#themable-areas-of-the-web-application":[9,0,0],
"theming-the-applications-web-based-interfaces.html#themable-web-based-administration-areas":[9,0,1]
};
