var ldap_connection_endpoints =
[
    [ "<access-service>/api/v1/ldap", "access-service-api-v1-ldap.html", [
      [ "URL structure", "access-service-api-v1-ldap.html#access-service-api-v1-ldap-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-ldap.html#access-service-api-v1-ldap-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-ldap.html#access-service-api-v1-ldap-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-ldap.html#access-service-api-v1-ldap-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-ldap.html#access-service-api-v1-ldap-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-ldap.html#access-service-api-v1-ldap-required-parameters", null ],
      [ "Returns", "access-service-api-v1-ldap.html#access-service-api-v1-ldap-returns", null ]
    ] ],
    [ "<access-service>/api/v1/ldap/{ldapconnectionId}", "access-service-api-v1-ldap-ldapconnectionid.html", [
      [ "URL structure", "access-service-api-v1-ldap-ldapconnectionid.html#access-service-api-v1-ldap-ldapconnectionid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-ldap-ldapconnectionid.html#access-service-api-v1-ldap-ldapconnectionid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-ldap-ldapconnectionid.html#access-service-api-v1-ldap-ldapconnectionid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-ldap-ldapconnectionid.html#access-service-api-v1-ldap-ldapconnectionid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-ldap-ldapconnectionid.html#access-service-api-v1-ldap-ldapconnectionid-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-ldap-ldapconnectionid.html#access-service-api-v1-ldap-ldapconnectionid-required-parameters", null ],
      [ "Returns", "access-service-api-v1-ldap-ldapconnectionid.html#access-service-api-v1-ldap-ldapconnectionid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/ldap/{ldapconnectionId}/enable", "access-service-api-v1-ldap-ldapconnectionid-enable.html", [
      [ "URL structure", "access-service-api-v1-ldap-ldapconnectionid-enable.html#access-service-api-v1-ldap-ldapconnectionid-enable-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-ldap-ldapconnectionid-enable.html#access-service-api-v1-ldap-ldapconnectionid-enable-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-ldap-ldapconnectionid-enable.html#access-service-api-v1-ldap-ldapconnectionid-enable-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-ldap-ldapconnectionid-enable.html#access-service-api-v1-ldap-ldapconnectionid-enable-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-ldap-ldapconnectionid-enable.html#access-service-api-v1-ldap-ldapconnectionid-enable-required-headers", null ],
      [ "Returns", "access-service-api-v1-ldap-ldapconnectionid-enable.html#access-service-api-v1-ldap-ldapconnectionid-enable-returns", null ]
    ] ],
    [ "<access-service>/api/v1/ldap/verifyconnection", "access-service-api-v1-ldap-verifyconnection.html", [
      [ "URL structure", "access-service-api-v1-ldap-verifyconnection.html#access-service-api-v1-ldap-verifyconnection-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-ldap-verifyconnection.html#access-service-api-v1-ldap-verifyconnection-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-ldap-verifyconnection.html#access-service-api-v1-ldap-verifyconnection-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-ldap-verifyconnection.html#access-service-api-v1-ldap-verifyconnection-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-ldap-verifyconnection.html#access-service-api-v1-ldap-verifyconnection-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-ldap-verifyconnection.html#access-service-api-v1-ldap-verifyconnection-required-parameters", null ],
      [ "Returns", "access-service-api-v1-ldap-verifyconnection.html#access-service-api-v1-ldap-verifyconnection-returns", null ]
    ] ]
];