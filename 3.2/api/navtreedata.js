var NAVTREE =
[
  [ "Covata API 3.2 Documentation", "index.html", [
    [ "Getting Started Guide", "getting-started-guide.html", "getting-started-guide" ],
    [ "API Reference Guide", "api-reference-guide.html", "api-reference-guide" ],
    [ "Getting Support", "getting-support.html", null ],
    [ "Important notice", "important-notice.html", null ],
    [ "Covata Documentation Home", "../../", null ]
  ] ]
];

var NAVTREEINDEX =
[
"../../",
"access-service-api-v1-i18n-bundles-localecode-reset.html",
"access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-url-structure",
"content-service-api-v1-discovery.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';