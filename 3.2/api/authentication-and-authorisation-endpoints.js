var authentication_and_authorisation_endpoints =
[
    [ "<access-service>/api/oauth/authorize", "access-service-api-oauth-authorize.html", [
      [ "URL structure", "access-service-api-oauth-authorize.html#access-service-api-oauth-authorize-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-oauth-authorize.html#access-service-api-oauth-authorize-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-oauth-authorize.html#access-service-api-oauth-authorize-detailed-description", null ],
      [ "Required parameters", "access-service-api-oauth-authorize.html#access-service-api-oauth-authorize-required-parameters", null ],
      [ "Returns", "access-service-api-oauth-authorize.html#access-service-api-authorize-token-returns", null ]
    ] ],
    [ "<access-service>/api/oauth/token", "access-service-api-oauth-token.html", [
      [ "URL structure", "access-service-api-oauth-token.html#access-service-api-oauth-token-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-oauth-token.html#access-service-api-oauth-token-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-oauth-token.html#access-service-api-oauth-token-detailed-description", null ],
      [ "Required headers", "access-service-api-oauth-token.html#access-service-api-oauth-token-required-headers", null ],
      [ "Required parameters", "access-service-api-oauth-token.html#access-service-api-oauth-token-required-parameters", null ],
      [ "Grant type-specific parameters", "access-service-api-oauth-token.html#access-service-api-oauth-token-grant-type-specific-parameters", null ],
      [ "Returns", "access-service-api-oauth-token.html#access-service-api-oauth-token-returns", null ]
    ] ]
];