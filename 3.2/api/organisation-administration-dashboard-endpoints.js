var organisation_administration_dashboard_endpoints =
[
    [ "<access-service>/api/v1/organisations/{orgGroupId}/statistics/items", "access-service-api-v1-organisations-orggroupid-statistics-items.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-statistics-items.html#access-service-api-v1-organisations-orggroupid-statistics-items-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-statistics-items.html#access-service-api-v1-organisations-orggroupid-statistics-items-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-statistics-items.html#access-service-api-v1-organisations-orggroupid-statistics-items-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orggroupid-statistics-items.html#access-service-api-v1-organisations-orggroupid-statistics-items-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-statistics-items.html#access-service-api-v1-organisations-orggroupid-statistics-items-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-statistics-items.html#access-service-api-v1-organisations-orggroupid-statistics-items-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/statistics/items/storage", "access-service-api-v1-organisations-orggroupid-statistics-items-storage.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-statistics-items-storage.html#access-service-api-v1-organisations-orggroupid-statistics-items-storage-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-statistics-items-storage.html#access-service-api-v1-organisations-orggroupid-statistics-items-storage-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-statistics-items-storage.html#access-service-api-v1-organisations-orggroupid-statistics-items-storage-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orggroupid-statistics-items-storage.html#access-service-api-v1-organisations-orggroupid-statistics-items-storage-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-statistics-items-storage.html#access-service-api-v1-organisations-orggroupid-statistics-items-storage-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-statistics-items-storage.html#access-service-api-v1-organisations-orggroupid-statistics-items-storage-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/statistics/items/extensions", "access-service-api-v1-organisations-orggroupid-statistics-items-extensions.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-statistics-items-extensions.html#access-service-api-v1-organisations-orggroupid-statistics-items-extensions-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-statistics-items-extensions.html#access-service-api-v1-organisations-orggroupid-statistics-items-extensions-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-statistics-items-extensions.html#access-service-api-v1-organisations-orggroupid-statistics-items-extensions-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orggroupid-statistics-items-extensions.html#access-service-api-v1-organisations-orggroupid-statistics-items-extensions-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-statistics-items-extensions.html#access-service-api-v1-organisations-orggroupid-statistics-items-extensions-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-statistics-items-extensions.html#access-service-api-v1-organisations-orggroupid-statistics-items-extensions-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/statistics/plans", "access-service-api-v1-organisations-orggroupid-statistics-plans.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-statistics-plans.html#access-service-api-v1-organisations-orggroupid-statistics-plans-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-statistics-plans.html#access-service-api-v1-organisations-orggroupid-statistics-plans-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-statistics-plans.html#access-service-api-v1-organisations-orggroupid-statistics-plans-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orggroupid-statistics-plans.html#access-service-api-v1-organisations-orggroupid-statistics-plans-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-statistics-plans.html#access-service-api-v1-organisations-orggroupid-statistics-plans-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-statistics-plans.html#access-service-api-v1-organisations-orggroupid-statistics-plans-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/statistics/users", "access-service-api-v1-organisations-orggroupid-statistics-users.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-statistics-users.html#access-service-api-v1-organisations-orggroupid-statistics-users-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-statistics-users.html#access-service-api-v1-organisations-orggroupid-statistics-users-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-statistics-users.html#access-service-api-v1-organisations-orggroupid-statistics-users-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orggroupid-statistics-users.html#access-service-api-v1-organisations-orggroupid-statistics-users-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-statistics-users.html#access-service-api-v1-organisations-orggroupid-statistics-users-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-statistics-users.html#access-service-api-v1-organisations-orggroupid-statistics-users-returns", null ]
    ] ]
];