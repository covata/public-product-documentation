var user_content_handling_endpoints =
[
    [ "<content-service>/api/v1/users", "content-service-api-v1-users.html", [
      [ "URL structure", "content-service-api-v1-users.html#content-service-api-v1-users-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-users.html#content-service-api-v1-users-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-users.html#content-service-api-v1-users-detailed-description", null ],
      [ "Supported roles", "content-service-api-v1-users.html#content-service-api-v1-users-supported-roles", null ],
      [ "Required headers", "content-service-api-v1-users.html#content-service-api-v1-users-required-headers", null ],
      [ "Optional parameters", "content-service-api-v1-users.html#content-service-api-v1-users-optional-parameters", null ],
      [ "Returns", "content-service-api-v1-users.html#content-service-api-v1-users-returns", null ]
    ] ],
    [ "<content-service>/api/v1/users/{userid}/contents", "content-service-api-v1-users-userid-contents.html", [
      [ "URL structure", "content-service-api-v1-users-userid-contents.html#content-service-api-v1-users-userid-contents-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-users-userid-contents.html#content-service-api-v1-users-userid-contents-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-users-userid-contents.html#content-service-api-v1-users-userid-contents-detailed-description", null ],
      [ "Supported roles", "content-service-api-v1-users-userid-contents.html#content-service-api-users-userid-contents-supported-roles", null ],
      [ "Required headers", "content-service-api-v1-users-userid-contents.html#content-service-api-v1-users-userid-contents-required-headers", null ],
      [ "Returns", "content-service-api-v1-users-userid-contents.html#content-service-api-v1-users-userid-contents-returns", null ]
    ] ],
    [ "<content-service>/api/v1/users/me", "content-service-api-v1-users-me.html", [
      [ "URL structure", "content-service-api-v1-users-me.html#content-service-api-v1-users-me-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-users-me.html#content-service-api-v1-users-me-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-users-me.html#content-service-api-v1-users-me-detailed-description", null ],
      [ "Supported roles", "content-service-api-v1-users-me.html#content-service-api-v1-users-me-supported-roles", null ],
      [ "Required headers", "content-service-api-v1-users-me.html#content-service-api-v1-users-me-required-headers", null ],
      [ "Returns", "content-service-api-v1-users-me.html#content-service-api-v1-users-me-returns", null ]
    ] ]
];