var auditing_reporting_endpoints =
[
    [ "<access-service>/api/v1/audit", "access-service-api-v1-audit.html", [
      [ "URL structure", "access-service-api-v1-audit.html#access-service-api-v1-audit-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-audit.html#access-service-api-v1-audit-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-audit.html#access-service-api-v1-audit-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-audit.html#access-service-api-v1-audit-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-audit.html#access-service-api-v1-audit-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-audit.html#access-service-api-v1-audit-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-audit.html#access-service-api-v1-audit-returns", null ]
    ] ],
    [ "<access-service>/api/v1/audit/exportReport", "access-service-api-v1-audit-exportreport.html", [
      [ "URL structure", "access-service-api-v1-audit-exportreport.html#access-service-api-v1-audit-exportreport-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-audit-exportreport.html#access-service-api-v1-audit-exportreport-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-audit-exportreport.html#access-service-api-v1-audit-exportreport-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-audit-exportreport.html#access-service-api-v1-audit-exportreport-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-audit-exportreport.html#access-service-api-v1-audit-exportreport-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-audit-exportreport.html#access-service-api-v1-audit-exportreport-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-audit-exportreport.html#access-service-api-v1-audit-exportreport-returns", null ]
    ] ]
];