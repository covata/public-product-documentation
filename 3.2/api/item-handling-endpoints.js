var item_handling_endpoints =
[
    [ "<access-service>/api/v1/collaborations/items/{itemId}", "access-service-api-v1-collaborations-items-itemid.html", [
      [ "URL structure", "access-service-api-v1-collaborations-items-itemid.html#access-service-api-v1-collaborations-items-itemid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-collaborations-items-itemid.html#access-service-api-v1-collaborations-items-itemid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-collaborations-items-itemid.html#access-service-api-v1-collaborations-items-itemid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-collaborations-items-itemid.html#access-service-api-v1-collaborations-items-itemid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-collaborations-items-itemid.html#access-service-api-v1-collaborations-items-itemid-required-headers", null ],
      [ "Returns", "access-service-api-v1-collaborations-items-itemid.html#access-service-api-v1-collaborations-items-itemid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/items", "access-service-api-v1-items.html", [
      [ "URL structure", "access-service-api-v1-items.html#access-service-api-v1-items-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-items.html#access-service-api-v1-items-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-items.html#access-service-api-v1-items-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-items.html#access-service-api-v1-items-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-items.html#access-service-api-v1-items-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-items.html#access-service-api-v1-items-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-items.html#access-service-api-v1-items-returns", null ]
    ] ],
    [ "<access-service>/api/v1/items/{itemId}", "access-service-api-v1-items-itemid.html", [
      [ "URL structure", "access-service-api-v1-items-itemid.html#access-service-api-v1-items-itemid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-items-itemid.html#access-service-api-v1-items-itemid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-items-itemid.html#access-service-api-v1-items-itemid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-items-itemid.html#access-service-api-items-itemid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-items-itemid.html#access-service-api-v1-items-itemid-required-headers", null ],
      [ "Returns", "access-service-api-v1-items-itemid.html#access-service-api-v1-items-itemid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/items/{itemId}/ancestry", "access-service-api-v1-items-itemid-ancestry.html", [
      [ "URL structure", "access-service-api-v1-items-itemid-ancestry.html#access-service-api-v1-items-itemid-ancestry-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-items-itemid-ancestry.html#access-service-api-v1-items-itemid-ancestry-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-items-itemid-ancestry.html#access-service-api-v1-items-itemid-ancestry-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-items-itemid-ancestry.html#access-service-api-v1-items-itemid-ancestry-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-items-itemid-ancestry.html#access-service-api-v1-items-itemid-ancestry-required-headers", null ],
      [ "Returns", "access-service-api-v1-items-itemid-ancestry.html#access-service-api-v1-items-itemid-ancestry-returns", null ]
    ] ],
    [ "<access-service>/api/v1/items/{itemId}/history", "access-service-api-v1-items-itemid-history.html", [
      [ "URL structure", "access-service-api-v1-items-itemid-history.html#access-service-api-v1-items-itemid-history-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-items-itemid-history.html#access-service-api-v1-items-itemid-history-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-items-itemid-history.html#access-service-api-v1-items-itemid-history-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-items-itemid-history.html#access-service-api-v1-items-itemid-history-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-items-itemid-history.html#access-service-api-v1-items-itemid-history-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-items-itemid-history.html#access-service-api-v1-items-itemid-history-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-items-itemid-history.html#access-service-api-v1-items-itemid-history-returns", null ]
    ] ],
    [ "<access-service>/api/v1/items/{itemId}/unshare", "access-service-api-v1-items-itemid-unshare.html", [
      [ "URL structure", "access-service-api-v1-items-itemid-unshare.html#access-service-api-v1-items-itemid-unshare-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-items-itemid-unshare.html#access-service-api-v1-items-itemid-unshare-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-items-itemid-unshare.html#access-service-api-v1-items-itemid-unshare-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-items-itemid-unshare.html#access-service-api-items-itemid-unshare-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-items-itemid-unshare.html#access-service-api-v1-items-itemid-unshare-required-headers", null ],
      [ "Returns", "access-service-api-v1-items-itemid-unshare.html#access-service-api-v1-items-itemid-unshare-returns", null ]
    ] ],
    [ "<access-service>/api/v1/items/delta", "access-service-api-v1-items-delta.html", [
      [ "URL structure", "access-service-api-v1-items-delta.html#access-service-api-v1-items-delta-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-items-delta.html#access-service-api-v1-items-delta-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-items-delta.html#access-service-api-v1-items-delta-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-items-delta.html#access-service-api-items-delta-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-items-delta.html#access-service-api-v1-items-delta-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-items-delta.html#access-service-api-v1-items-delta-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-items-delta.html#access-service-api-v1-items-delta-returns", null ]
    ] ],
    [ "<access-service>/api/v1/items/status", "access-service-api-v1-items-status.html", [
      [ "URL structure", "access-service-api-v1-items-status.html#access-service-api-v1-items-status-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-items-status.html#access-service-api-v1-items-status-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-items-status.html#access-service-api-v1-items-status-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-items-status.html#access-service-api-items-status-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-items-status.html#access-service-api-v1-items-status-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-items-status.html#access-service-api-v1-items-status-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-items-status.html#access-service-api-v1-items-status-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/items/transfer", "access-service-api-v1-organisations-orgid-items-transfer.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-items-transfer.html#access-service-api-v1-organisations-orgid-items-transfer-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-items-transfer.html#access-service-api-v1-organisations-orgid-items-transfer-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-items-transfer.html#access-service-api-v1-organisations-orgid-items-transfer-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-items-transfer.html#access-service-api-v1-organisations-orgid-items-transfer-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-items-transfer.html#access-service-api-v1-organisations-orgid-items-transfer-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-organisations-orgid-items-transfer.html#access-service-api-v1-organisations-orgid-items-transfer-required-parameters", null ],
      [ "Optional parameters", "access-service-api-v1-organisations-orgid-items-transfer.html#access-service-api-v1-organisations-orgid-items-transfer-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-items-transfer.html#access-service-api-v1-organisations-orgid-items-transfer-returns", null ]
    ] ],
    [ "<access-service>/api/v1/permissions/sets", "access-service-api-v1-permissions-sets.html", [
      [ "URL structure", "access-service-api-v1-permissions-sets.html#access-service-api-v1-permissions-sets-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-permissions-sets.html#access-service-api-v1-permissions-sets-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-permissions-sets.html#access-service-api-v1-permissions-sets-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-permissions-sets.html#access-service-api-v1-permissions-sets-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-permissions-sets.html#access-service-api-v1-permissions-sets-required-headers", null ],
      [ "Returns", "access-service-api-v1-permissions-sets.html#access-service-api-v1-permissions-sets-returns", null ]
    ] ]
];