var user_account_handling_endpoints =
[
    [ "<access-service>/api/v1/users", "access-service-api-v1-users.html", [
      [ "URL structure", "access-service-api-v1-users.html#access-service-api-v1-users-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users.html#access-service-api-v1-users-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users.html#access-service-api-v1-users-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-users.html#access-service-api-v1-users-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-users.html#access-service-api-v1-users-required-headers", null ],
      [ "Required parameters on POST", "access-service-api-v1-users.html#access-service-api-v1-users-required-parameters-on-POST", null ],
      [ "Optional parameters on POST", "access-service-api-v1-users.html#access-service-api-v1-users-optional-parameters-on-POST", null ],
      [ "Optional parameters on GET", "access-service-api-v1-users.html#access-service-api-v1-users-optional-parameters-on-GET", null ],
      [ "Returns from a POST request", "access-service-api-v1-users.html#access-service-api-v1-users-returns-from-a-POST-request", null ],
      [ "Returns from a GET request", "access-service-api-v1-users.html#access-service-api-v1-users-returns-from-a-GET-request", null ]
    ] ],
    [ "<access-service>/api/v1/users/{userid}", "access-service-api-v1-users-userid.html", [
      [ "URL structure", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-userid.html#access-service-api-users-userid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/{userId}/enable", "access-service-api-v1-users-userid-enable.html", [
      [ "URL structure", "access-service-api-v1-users-userid-enable.html#access-service-api-v1-users-userid-enable-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-userid-enable.html#access-service-api-v1-users-userid-enable-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-userid-enable.html#access-service-api-v1-users-userid-enable-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-userid-enable.html#access-service-api-v1-users-userid-enable-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-userid-enable.html#access-service-api-v1-users-userid-enable-required-headers", null ],
      [ "Returns", "access-service-api-v1-users-userid-enable.html#access-service-api-v1-users-userid-enable-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/{userId}/expire", "access-service-api-v1-users-userid-expire.html", [
      [ "URL structure", "access-service-api-v1-users-userid-expire.html#access-service-api-v1-users-userid-expire-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-userid-expire.html#access-service-api-v1-users-userid-expire-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-userid-expire.html#access-service-api-v1-users-userid-expire-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-userid-expire.html#access-service-api-v1-users-userid-expire-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-userid-expire.html#access-service-api-v1-users-userid-expire-required-headers", null ],
      [ "Returns", "access-service-api-v1-users-userid-expire.html#access-service-api-v1-users-userid-expire-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/{userId}/items/count", "access-service-api-v1-users-userid-items-count.html", [
      [ "URL structure", "access-service-api-v1-users-userid-items-count.html#access-service-api-v1-users-userid-items-count-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-userid-items-count.html#access-service-api-v1-users-userid-items-count-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-userid-items-count.html#access-service-api-v1-users-userid-items-count-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-userid-items-count.html#access-service-api-v1-users-userid-items-count-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-userid-items-count.html#access-service-api-v1-users-userid-items-count-required-headers", null ],
      [ "Returns", "access-service-api-v1-users-userid-items-count.html#access-service-api-v1-users-userid-items-count-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/{userId}/unlock", "access-service-api-v1-users-userid-unlock.html", [
      [ "URL structure", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-required-headers", null ],
      [ "Returns", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/bulk", "access-service-api-v1-users-bulk.html", [
      [ "URL structure", "access-service-api-v1-users-bulk.html#access-service-api-v1-users-bulk-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-bulk.html#access-service-api-v1-users-bulk-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-bulk.html#access-service-api-v1-users-bulk-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-bulk.html#access-service-api-users-bulk-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-bulk.html#access-service-api-v1-users-bulk-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-users-bulk.html#access-service-api-v1-users-bulk-required-parameters", null ],
      [ "Optional parameters", "access-service-api-v1-users-bulk.html#access-service-api-v1-users-bulk-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-users-bulk.html#access-service-api-v1-users-bulk-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/import", "access-service-api-v1-users-import.html", [
      [ "URL structure", "access-service-api-v1-users-import.html#access-service-api-v1-users-import-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-import.html#access-service-api-v1-users-import-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-import.html#access-service-api-v1-users-import-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-import.html#access-service-api-users-import-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-import.html#access-service-api-v1-users-import-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-users-import.html#access-service-api-v1-users-import-required-parameters", null ],
      [ "Returns", "access-service-api-v1-users-import.html#access-service-api-v1-users-import-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me", "access-service-api-v1-users-me.html", [
      [ "URL structure", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/applications/approvals", "access-service-api-v1-users-me-applications-approvals.html", [
      [ "URL structure", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-v1-users-me-applications-approvals-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-v1-users-me-applications-approvals-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-v1-users-me-applications-approvals-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-applications-approvals-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-v1-users-me-applications-approvals-required-headers", null ],
      [ "Returns", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-v1-users-me-applications-approvals-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/applications/approvals/{approvalId}", "access-service-api-v1-users-me-applications-approvals-approvalid.html", [
      [ "URL structure", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-v1-users-me-applications-approvals-approvalid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-v1-users-me-applications-approvals-approvalid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-v1-users-me-applications-approvals-approvalid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-applications-approvals-approvalid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-v1-users-me-applications-approvals-approvalid-required-headers", null ],
      [ "Returns", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-v1-users-me-applications-approvals-approvalid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/contacts", "access-service-api-v1-users-me-contacts.html", [
      [ "URL structure", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-me-contacts.html#access-service-api-users-me-contacts-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-required-headers", null ],
      [ "Required parameters on POST", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-required-parameters-on-POST", null ],
      [ "Optional parameters on POST", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-optional-parameters-on-POST", null ],
      [ "Optional parameters on GET", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-optional-parameters-on-GET", null ],
      [ "Returns", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/contacts/{contactId}", "access-service-api-v1-users-me-contacts-contactid.html", [
      [ "URL structure", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-users-me-contacts-contactid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-required-parameters", null ],
      [ "Optional parameters", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-optional-parameters", null ],
      [ "Returns from a PUT request", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-returns-from-a-PUT-request", null ],
      [ "Returns from a DELETE request", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-returns-from-a-DELETE-request", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/items/count", "access-service-api-v1-users-me-items-count.html", [
      [ "URL structure", "access-service-api-v1-users-me-items-count.html#access-service-api-v1-users-me-items-count-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-items-count.html#access-service-api-v1-users-me-items-count-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-items-count.html#access-service-api-v1-users-me-items-count-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-users-me-items-count.html#access-service-api-v1-users-me-items-count-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-users-me-items-count.html#access-service-api-v1-users-me-items-count-required-headers", null ],
      [ "Returns", "access-service-api-v1-users-me-items-count.html#access-service-api-v1-users-me-items-count-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/notifications/email", "access-service-api-v1-users-me-notifications-email.html", [
      [ "URL structure", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-returns", null ]
    ] ]
];