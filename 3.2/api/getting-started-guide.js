var getting_started_guide =
[
    [ "What the Covata API can do", "getting-started-guide.html#what-the-covata-api-can-do", null ],
    [ "Tools for accessing the Covata API", "getting-started-guide.html#tools-for-accessing-the-product-api", null ],
    [ "Authentication and authorization", "authentication-and-authorisation.html", [
      [ "Registering your application on the Covata Platform", "authentication-and-authorisation.html#registering-your-application-on-the-platform", null ],
      [ "Overview of the OAuth 2.0 process", "authentication-and-authorisation.html#overview-of-the-oauth-2-0-process", null ],
      [ "Initiating authentication and authorization", "authentication-and-authorisation.html#initiating-authentication-and-authorisation", [
        [ "Using the authorization code grant type", "authentication-and-authorisation.html#using-the-authorization-code-grant-type", null ],
        [ "Using the implicit grant type", "authentication-and-authorisation.html#using-the-implicit-grant-type", null ],
        [ "Using the password grant type", "authentication-and-authorisation.html#using-the-password-grant-type", null ]
      ] ],
      [ "Receiving the access token", "authentication-and-authorisation.html#receiving-the-access-token", [
        [ "From authorization code or password grant type requests", "authentication-and-authorisation.html#from-authorization-code-or-password-grant-type-requests", null ],
        [ "From implicit grant type requests", "authentication-and-authorisation.html#from-implicit-grant-type-requests", null ]
      ] ]
    ] ],
    [ "Creating files and folders", "creating-files-and-folders.html", [
      [ "Definitions", "creating-files-and-folders.html#definitions", [
        [ "Secure Object", "creating-files-and-folders.html#secure-object", null ],
        [ "Collection", "creating-files-and-folders.html#collection", null ],
        [ "Item", "creating-files-and-folders.html#item", null ],
        [ "Organization", "creating-files-and-folders.html#organisation", null ]
      ] ],
      [ "Obtaining a cryptographic key", "creating-files-and-folders.html#obtaining-a-cryptographic-key", null ],
      [ "Initializing a new 'Incomplete' file", "creating-files-and-folders.html#initializing-a-new-incomplete-file-object", null ],
      [ "Creating a completed Secure Object (data encrypted locally)", "creating-files-and-folders.html#creating-a-completed-file-object-data-encrypted-locally", null ],
      [ "Uploading a completed Secure Object's encrypted data", "creating-files-and-folders.html#uploading-a-completed-files-encrypted-data", null ],
      [ "Creating a completed Secure Object (uploading unencrypted data)", "creating-files-and-folders.html#creating-a-completed-file-object-uploading-unencrypted-data", null ],
      [ "Creating a collection for managing Secure Objects", "creating-files-and-folders.html#creating-a-folder-for-managing-files", null ]
    ] ],
    [ "Retrieving information about items from the Covata Platform", "retrieving-information-about-items-from-the-platform.html", [
      [ "Retrieving info about all immediate items of a common parent", "retrieving-information-about-items-from-the-platform.html#retrieving-info-about-all-immediate-items-of-a-common-parent-folder", null ],
      [ "Retrieving info about all items filtered by text search", "retrieving-information-about-items-from-the-platform.html#retrieving-info-about-all-items-filtered-by-text-search", null ],
      [ "Retrieving info about all items based on ownership or sharing status", "retrieving-information-about-items-from-the-platform.html#retrieving-info-about-items-based-on-ownership-or-sharing-status", null ],
      [ "Retrieving info about all ancestral collections of an item", "retrieving-information-about-items-from-the-platform.html#retrieving-info-about-all-ancestral-folders-of-an-item", null ],
      [ "Sorting, ordering and paginating the list of item info", "retrieving-information-about-items-from-the-platform.html#sorting-ordering-and-paginating-the-list-of-item-info", null ]
    ] ],
    [ "Sharing items", "sharing-items.html", [
      [ "Sharing a Secure Object with collaborators", "sharing-items.html#sharing-a-file-with-collaborators", null ],
      [ "Sharing a collection with collaborators", "sharing-items.html#sharing-a-folder-with-collaborators", null ],
      [ "Determining existing collaborators on an item", "sharing-items.html#determining-existing-collaborators-on-an-item", null ],
      [ "Modifying existing collaborators on an item", "sharing-items.html#modifying-existing-collaborators-on-an-item", null ]
    ] ]
];