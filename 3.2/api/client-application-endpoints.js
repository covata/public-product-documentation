var client_application_endpoints =
[
    [ "<access-service>/api/v1/clientapps", "access-service-api-v1-clientapps.html", [
      [ "URL structure", "access-service-api-v1-clientapps.html#access-service-api-v1-clientapps-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-clientapps.html#access-service-api-v1-clientapps-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-clientapps.html#access-service-api-v1-clientapps-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-clientapps.html#access-service-api-clientapps-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-clientapps.html#access-service-api-v1-clientapps-required-headers", null ],
      [ "Required parameters on POST", "access-service-api-v1-clientapps.html#access-service-api-v1-clientapps-required-parameters-on-POST", null ],
      [ "Optional parameters on POST", "access-service-api-v1-clientapps.html#access-service-api-v1-clientapps-optional-parameters-on-POST", null ],
      [ "Optional parameters on GET", "access-service-api-v1-clientapps.html#access-service-api-v1-clientapps-required-parameters-on-GET", null ],
      [ "Returns from a POST request", "access-service-api-v1-clientapps.html#access-service-api-v1-clientapps-returns-from-a-POST-request", null ],
      [ "Returns from a GET request", "access-service-api-v1-clientapps.html#access-service-api-v1-clientapps-returns-from-a-GET-request", null ]
    ] ],
    [ "<access-service>/api/v1/clientapps/{clientInfoId}", "access-service-api-v1-clientapps-clientinfoid.html", [
      [ "URL structure", "access-service-api-v1-clientapps-clientinfoid.html#access-service-api-v1-clientapps-clientinfoid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-clientapps-clientinfoid.html#access-service-api-v1-clientapps-clientinfoid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-clientapps-clientinfoid.html#access-service-api-v1-clientapps-clientinfoid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-clientapps-clientinfoid.html#access-service-api-clientapps-clientappid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-clientapps-clientinfoid.html#access-service-api-v1-clientapps-clientinfoid-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-clientapps-clientinfoid.html#access-service-api-v1-clientapps-clientinfoid-optional-parameters", null ],
      [ "Returns from a GET or PUT request", "access-service-api-v1-clientapps-clientinfoid.html#access-service-api-v1-clientapps-clientinfoid-returns-from-a-GET-or-PUT-request", null ],
      [ "Returns from a DELETE request", "access-service-api-v1-clientapps-clientinfoid.html#access-service-api-v1-clientapps-clientinfoid-returns-from-a-DELETE-request", null ]
    ] ],
    [ "<access-service>/api/v1/clientapps/{clientInfoId}/enable", "access-service-api-v1-clientapps-clientinfoid-enable.html", [
      [ "URL structure", "access-service-api-v1-clientapps-clientinfoid-enable.html#access-service-api-v1-clientapps-clientinfoid-enable-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-clientapps-clientinfoid-enable.html#access-service-api-v1-clientapps-clientinfoid-enable-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-clientapps-clientinfoid-enable.html#access-service-api-v1-clientapps-clientinfoid-enable-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-clientapps-clientinfoid-enable.html#access-service-api-clientapps-clientappid-enable-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-clientapps-clientinfoid-enable.html#access-service-api-v1-clientapps-clientinfoid-enable-required-headers", null ],
      [ "Returns", "access-service-api-v1-clientapps-clientinfoid-enable.html#access-service-api-v1-clientapps-clientinfoid-enable-returns", null ]
    ] ]
];