var plan_endpoints =
[
    [ "<access-service>/api/v1/plans", "access-service-api-v1-plans.html", [
      [ "URL structure", "access-service-api-v1-plans.html#access-service-api-v1-plans-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-plans.html#access-service-api-v1-plans-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-plans.html#access-service-api-v1-plans-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-plans.html#access-service-api-plans-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-plans.html#access-service-api-v1-plans-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-plans.html#access-service-api-v1-plans-required-parameters", null ],
      [ "Optional parameters", "access-service-api-v1-plans.html#access-service-api-v1-plans-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-plans.html#access-service-api-v1-plans-returns", null ]
    ] ],
    [ "<access-service>/api/v1/plans/{planId}", "access-service-api-v1-plans-planid.html", [
      [ "URL structure", "access-service-api-v1-plans-planid.html#access-service-api-v1-plans-planid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-plans-planid.html#access-service-api-v1-plans-planid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-plans-planid.html#access-service-api-v1-plans-planid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-plans-planid.html#access-service-api-plans-planid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-plans-planid.html#access-service-api-v1-plans-planid-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-plans-planid.html#access-service-api-v1-plans-planid-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-plans-planid.html#access-service-api-v1-plans-planid-returns", null ]
    ] ]
];