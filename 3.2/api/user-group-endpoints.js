var user_group_endpoints =
[
    [ "<access-service>/api/v1/organisations/{orgId}/groups", "access-service-api-v1-organisations-orgid-groups.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-groups-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-required-headers", null ],
      [ "Required parameters on POST", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-required-parameters", null ],
      [ "Optional parameters on POST", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-optional-parameters-on-POST", null ],
      [ "Optional parameters on GET", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-optional-parameters-on-GET", null ],
      [ "Returns from a POST request", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-returns-from-a-POST-request", null ],
      [ "Returns from a GET request", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-returns-from-a-GET-request", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/groups/{groupId}", "access-service-api-v1-organisations-orgid-groups-groupid.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-groups-groupid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-optional-parameters", null ],
      [ "Returns from a GET or PUT request", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-returns-from-a-GET-or-PUT-request", null ],
      [ "Returns from a DELETE request", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-returns-from-DELETE-request", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/groups/{groupId}/labels", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-v1-organisations-orgid-groups-groupid-labels-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-v1-organisations-orgid-groups-groupid-labels-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-v1-organisations-orgid-groups-groupid-labels-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-groups-groupid-labels-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-v1-organisations-orgid-groups-groupid-labels-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-v1-organisations-orgid-groups-groupid-labels-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/groups/{groupId}/users", "access-service-api-v1-organisations-orgid-groups-groupid-users.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-v1-organisations-orgid-groups-groupid-users-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-v1-organisations-orgid-groups-groupid-users-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-v1-organisations-orgid-groups-groupid-users-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-groups-groupid-users-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-v1-organisations-orgid-groups-groupid-users-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-v1-organisations-orgid-groups-groupid-users-returns", null ]
    ] ]
];