var file_handling_endpoints =
[
    [ "<access-service>/api/v1/keys", "access-service-api-v1-keys.html", [
      [ "URL structure", "access-service-api-v1-keys.html#access-service-api-v1-keys-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-keys.html#access-service-api-v1-keys-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-keys.html#access-service-api-v1-keys-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-keys.html#access-service-api-v1-keys-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-keys.html#access-service-api-v1-keys-required-headers", null ],
      [ "Required body", "access-service-api-v1-keys.html#access-service-api-v1-keys-required-body", null ],
      [ "Optional parameters", "access-service-api-v1-keys.html#access-service-api-v1-keys-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-keys.html#access-service-api-v1-keys-returns", null ]
    ] ],
    [ "<access-service>/api/v1/keys/{keyId}", "access-service-api-v1-keys-keyid.html", [
      [ "URL structure", "access-service-api-v1-keys-keyid.html#access-service-api-v1-keys-keyid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-keys-keyid.html#access-service-api-v1-keys-keyid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-keys-keyid.html#access-service-api-v1-keys-keyid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-keys-keyid.html#access-service-api-keys-keyid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-keys-keyid.html#access-service-api-v1-keys-keyid-required-headers", null ],
      [ "Returns", "access-service-api-v1-keys-keyid.html#access-service-api-v1-keys-keyid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/objects", "access-service-api-v1-organisations-orggroupid-objects.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-required-headers", null ],
      [ "Required POST-request parameters", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-required-post-request-parameters", null ],
      [ "Optional POST-request parameters", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-optional-post-request-parameters", null ],
      [ "Optional GET-request parameters", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-optional-get-request-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-returns", null ]
    ] ],
    [ "<access-service>/api/v1/objects", "access-service-api-v1-objects.html", [
      [ "URL structure", "access-service-api-v1-objects.html#access-service-api-v1-objects-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-objects.html#access-service-api-v1-objects-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-objects.html#access-service-api-v1-objects-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-objects.html#access-service-api-v1-objects-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-objects.html#access-service-api-v1-objects-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-objects.html#access-service-api-v1-objects-required-parameters", null ],
      [ "Returns", "access-service-api-v1-objects.html#access-service-api-v1-objects-returns", null ]
    ] ],
    [ "<access-service>/api/v1/objects/{objectId}", "access-service-api-v1-objects-objectid.html", [
      [ "URL structure", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/objects/{objectId}/enable", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-returns", null ]
    ] ],
    [ "<access-service>/api/v1/objects/{objectId}/keys", "access-service-api-v1-objects-objectid-keys.html", [
      [ "URL structure", "access-service-api-v1-objects-objectid-keys.html#access-service-api-v1-objects-objectid-keys-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-objects-objectid-keys.html#access-service-api-v1-objects-objectid-keys-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-objects-objectid-keys.html#access-service-api-v1-objects-objectid-keys-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-objects-objectid-keys.html#access-service-api-objects-objectid-keys-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-objects-objectid-keys.html#access-service-api-v1-objects-objectid-keys-required-headers", null ],
      [ "Returns", "access-service-api-v1-objects-objectid-keys.html#access-service-api-v1-objects-objectid-keys-returns", null ]
    ] ],
    [ "<access-service>/api/v1/objects/{objectId}/keys/view", "access-service-api-v1-objects-objectid-keys-view.html", [
      [ "URL structure", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-v1-objects-objectid-keys-view-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-v1-objects-objectid-keys-view-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-v1-objects-objectid-keys-view-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-objects-objectid-keys-view-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-v1-objects-objectid-keys-view-required-headers", null ],
      [ "Returns", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-v1-objects-objectid-keys-view-returns", null ]
    ] ],
    [ "<access-service>/api/v1/objects/bulk", "access-service-api-v1-objects-bulk.html", [
      [ "URL structure", "access-service-api-v1-objects-bulk.html#access-service-api-v1-objects-bulk-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-objects-bulk.html#access-service-api-v1-objects-bulk-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-objects-bulk.html#access-service-api-v1-objects-bulk-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-objects-bulk.html#access-service-api-objects-bulk-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-objects-bulk.html#access-service-api-v1-objects-bulk-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-objects-bulk.html#access-service-api-v1-objects-bulk-required-parameters", null ],
      [ "Optional parameters", "access-service-api-v1-objects-bulk.html#access-service-api-v1-objects-bulk-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-objects-bulk.html#access-service-api-v1-objects-bulk-returns", null ]
    ] ]
];