var plan_content_endpoints =
[
    [ "<content-service>/api/v1/plans/usage", "content-service-api-v1-plans-usage.html", [
      [ "URL structure", "content-service-api-v1-plans-usage.html#content-service-api-v1-plans-usage-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-plans-usage.html#content-service-api-v1-plans-usage-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-plans-usage.html#content-service-api-v1-plans-usage-detailed-description", null ],
      [ "Supported roles", "content-service-api-v1-plans-usage.html#content-service-api-plans-usage-supported-roles", null ],
      [ "Required headers", "content-service-api-v1-plans-usage.html#content-service-api-v1-plans-usage-required-headers", null ],
      [ "Returns", "content-service-api-v1-plans-usage.html#content-service-api-v1-plans-usage-returns", null ]
    ] ],
    [ "<content-service>/api/v1/plans/usage/{userid}", "content-service-api-v1-plans-usage-userid.html", [
      [ "URL structure", "content-service-api-v1-plans-usage-userid.html#content-service-api-v1-plans-usage-userid-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-plans-usage-userid.html#content-service-api-v1-plans-usage-userid-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-plans-usage-userid.html#content-service-api-v1-plans-usage-userid-detailed-description", null ],
      [ "Supported roles", "content-service-api-v1-plans-usage-userid.html#content-service-api-plans-usage-userid-supported-roles", null ],
      [ "Required headers", "content-service-api-v1-plans-usage-userid.html#content-service-api-v1-plans-usage-userid-required-headers", null ],
      [ "Returns", "content-service-api-v1-plans-usage-userid.html#content-service-api-v1-plans-usage-userid-returns", null ]
    ] ]
];