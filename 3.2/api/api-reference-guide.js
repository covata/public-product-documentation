var api_reference_guide =
[
    [ "Authentication and authorization endpoints", "authentication-and-authorisation-endpoints.html", "authentication-and-authorisation-endpoints" ],
    [ "Access Service discovery endpoints", "access-service-discovery-endpoints.html", "access-service-discovery-endpoints" ],
    [ "File handling endpoints", "file-handling-endpoints.html", "file-handling-endpoints" ],
    [ "Folder handling endpoints", "folder-handling-endpoints.html", "folder-handling-endpoints" ],
    [ "Item handling endpoints", "item-handling-endpoints.html", "item-handling-endpoints" ],
    [ "Safe Share Administration dashboard endpoints", "platform-administration-dashboard-endpoints.html", "platform-administration-dashboard-endpoints" ],
    [ "Organization Administration dashboard endpoints", "organisation-administration-dashboard-endpoints.html", "organisation-administration-dashboard-endpoints" ],
    [ "User account handling endpoints", "user-account-handling-endpoints.html", "user-account-handling-endpoints" ],
    [ "User whitelist endpoints", "user-whitelist-endpoints.html", "user-whitelist-endpoints" ],
    [ "LDAP connection endpoints", "ldap-connection-endpoints.html", "ldap-connection-endpoints" ],
    [ "Classification endpoints", "classification-endpoints.html", "classification-endpoints" ],
    [ "User group endpoints", "user-group-endpoints.html", "user-group-endpoints" ],
    [ "Plan endpoints", "plan-endpoints.html", "plan-endpoints" ],
    [ "Client application endpoints", "client-application-endpoints.html", "client-application-endpoints" ],
    [ "Auditing/reporting endpoints", "auditing-reporting-endpoints.html", "auditing-reporting-endpoints" ],
    [ "Safe Share Administration configuration endpoints", "platform-administration-configuration-endpoints.html", "platform-administration-configuration-endpoints" ],
    [ "Organization Administration configuration endpoints", "organisation-administration-configuration-endpoints.html", null ],
    [ "Internationalization (i18n) endpoints", "i18n-endpoints.html", "i18n-endpoints" ],
    [ "Content Service discovery endpoints", "content-service-discovery-endpoints.html", "content-service-discovery-endpoints" ],
    [ "File content handling endpoints", "file-content-handling-endpoints.html", "file-content-handling-endpoints" ],
    [ "Folder content handling endpoints", "folder-content-handling-endpoints.html", "folder-content-handling-endpoints" ],
    [ "User content handling endpoints", "user-content-handling-endpoints.html", "user-content-handling-endpoints" ],
    [ "Plan content endpoints", "plan-content-endpoints.html", "plan-content-endpoints" ]
];