var classification_endpoints =
[
    [ "<access-service>/api/v1/organisations/{orgId}/labels", "access-service-api-v1-organisations-orgid-labels.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-labels-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-required-headers", null ],
      [ "Required parameters on POST", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-required-parameters-on-POST", null ],
      [ "Optional parameters on POST", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-optional-parameters-on-POST", null ],
      [ "Optional parameters on GET", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-optional-parameters-on-GET", null ],
      [ "Returns from a POST request", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-returns-from-a-POST-request", null ],
      [ "Returns from a GET request", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-returns-from-a-GET-request", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/labels/{labelId}", "access-service-api-v1-organisations-orgid-labels-labelid.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-labels-labelid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-required-headers", null ],
      [ "Returns from a GET request", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-returns-from-a-GET-request", null ],
      [ "Returns from a DELETE request", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-returns-from-a-DELETE-request", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/labels/{labelId}/priority", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-labels-labelid-priority-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-required-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-returns", null ]
    ] ]
];