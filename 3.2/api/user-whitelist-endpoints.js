var user_whitelist_endpoints =
[
    [ "<access-service>/api/v1/userwhitelist", "access-service-api-v1-userwhitelist.html", [
      [ "URL structure", "access-service-api-v1-userwhitelist.html#access-service-api-v1-userwhitelist-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-userwhitelist.html#access-service-api-v1-userwhitelist-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-userwhitelist.html#access-service-api-v1-userwhitelist-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-userwhitelist.html#access-service-api-v1-userwhitelist-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-userwhitelist.html#access-service-api-v1-userwhitelist-required-headers", null ],
      [ "Required parameters on POST", "access-service-api-v1-userwhitelist.html#access-service-api-v1-userwhitelist-required-parameters-on-POST", null ],
      [ "Optional parameters on GET", "access-service-api-v1-userwhitelist.html#access-service-api-v1-userwhitelist-optional-parameters-on-GET", null ],
      [ "Returns from a POST request", "access-service-api-v1-userwhitelist.html#access-service-api-v1-userwhitelist-returns-from-a-POST-request", null ],
      [ "Returns from a GET request", "access-service-api-v1-userwhitelist.html#access-service-api-v1-userwhitelist-returns-from-a-GET-request", null ]
    ] ],
    [ "<access-service>/api/v1/userwhitelist/{userwhitelistId}", "access-service-api-v1-userwhitelist-userwhitelistid.html", [
      [ "URL structure", "access-service-api-v1-userwhitelist-userwhitelistid.html#access-service-api-v1-userwhitelist-userwhitelistid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-userwhitelist-userwhitelistid.html#access-service-api-v1-userwhitelist-userwhitelistid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-userwhitelist-userwhitelistid.html#access-service-api-v1-userwhitelist-userwhitelistid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-userwhitelist-userwhitelistid.html#access-service-api-v1-userwhitelist-userwhitelistid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-userwhitelist-userwhitelistid.html#access-service-api-v1-userwhitelist-userwhitelistid-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-userwhitelist-userwhitelistid.html#access-service-api-v1-userwhitelist-userwhitelistid-required-parameters", null ],
      [ "Returns", "access-service-api-v1-userwhitelist-userwhitelistid.html#access-service-api-v1-userwhitelist-userwhitelistid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/userwhitelist/bulk", "access-service-api-v1-userwhitelist-bulk.html", [
      [ "URL structure", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-required-parameters", null ],
      [ "Returns", "access-service-api-v1-userwhitelist-bulk.html#access-service-api-v1-userwhitelist-bulk-returns", null ]
    ] ],
    [ "<access-service>/api/v1/userwhitelist/check", "access-service-api-v1-userwhitelist-check.html", [
      [ "URL structure", "access-service-api-v1-userwhitelist-check.html#access-service-api-v1-userwhitelist-check-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-userwhitelist-check.html#access-service-api-v1-userwhitelist-check-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-userwhitelist-check.html#access-service-api-v1-userwhitelist-check-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-userwhitelist-check.html#access-service-api-v1-userwhitelist-check-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-userwhitelist-check.html#access-service-api-v1-userwhitelist-check-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-userwhitelist-check.html#access-service-api-v1-userwhitelist-check-required-parameters", null ],
      [ "Returns", "access-service-api-v1-userwhitelist-check.html#access-service-api-v1-userwhitelist-check-returns", null ]
    ] ]
];