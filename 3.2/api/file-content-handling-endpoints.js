var file_content_handling_endpoints =
[
    [ "<content-service>/api/v1/objects/{objectId}/contents", "content-service-api-v1-objects-objectid-contents.html", [
      [ "URL structure", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-detailed-description", null ],
      [ "Supported roles and conditions", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-supported-roles-and-conditions", null ],
      [ "Required headers", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-headers", null ],
      [ "Required parameters on POST", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-parameters-on-POST", null ],
      [ "Required parameters for multi-chunk uploads", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-parameters-for-multi-chunk-uploads", null ],
      [ "Required parameters on GET", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-parameters-on-GET", null ],
      [ "Returns from a POST request", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-returns-from-a-POST-request", null ],
      [ "Returns from a GET or DELETE request", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-returns-from-a-GET-or-DELETE-request", null ]
    ] ],
    [ "<content-service>/api/v1/objects/{objectId}/view", "content-service-api-v1-objects-objectid-view.html", [
      [ "URL structure", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-detailed-description", null ],
      [ "Supported roles and conditions", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-supported-roles-and-conditions", null ],
      [ "Required headers", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-required-headers", null ],
      [ "Optional parameters", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-optional-parameters-on-GET", null ],
      [ "Returns", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-returns-from-a-GET-or-PUT-request", null ]
    ] ]
];