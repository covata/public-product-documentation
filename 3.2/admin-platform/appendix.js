var appendix =
[
    [ "Theming Safe Share's web-based interfaces", "theming-the-applications-web-based-interfaces.html", [
      [ "Themable areas of the Safe Share web application", "theming-the-applications-web-based-interfaces.html#themable-areas-of-the-web-application", null ],
      [ "Themable web-based administration areas", "theming-the-applications-web-based-interfaces.html#themable-web-based-administration-areas", null ],
      [ "Other themable Safe Share areas", "theming-the-applications-web-based-interfaces.html#other-themable-application-areas", null ]
    ] ],
    [ "Managing Safe Share for iOS through MDM software", "managing-safe-share-for-ios-through-mdm-software.html", [
      [ "Step 1 - Install Safe Share for iOS via MDM software", "managing-safe-share-for-ios-through-mdm-software.html#step-1-install-safe-share-for-ios-via-mdm-software", null ],
      [ "Step 2 - Configure Safe Share for iOS via MDM software", "managing-safe-share-for-ios-through-mdm-software.html#step-2-configure-safe-share-for-ios-via-mdm-software", null ],
      [ "The 'mdm-configuration.plist' file", "managing-safe-share-for-ios-through-mdm-software.html#the-mdm-configuration-plist-file", null ]
    ] ]
];