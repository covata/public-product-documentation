var NAVTREE =
[
  [ "Safe Share for Windows 3.2 Benutzeranleitung", "index.html", [
    [ "Installation und Aktualisierung von Safe Share for Windows", "installing-and-upgrading-the-application.html", [
      [ "Systemanforderungen", "installing-and-upgrading-the-application.html#system-requirements", null ],
      [ "Installation", "installing-and-upgrading-the-application.html#installing", null ],
      [ "Konfiguration Ihrer Instanz der Covata-Plattform", "installing-and-upgrading-the-application.html#configuring-your-server-url", null ],
      [ "Aktualisierung", "installing-and-upgrading-the-application.html#upgrading", null ],
      [ "Deinstallation", "installing-and-upgrading-the-application.html#uninstalling", null ],
      [ "Änderung der URL der Covata-Plattform", "installing-and-upgrading-the-application.html#modifying-the-server-url", null ]
    ] ],
    [ "Start von Safe Share for Windows und Anmeldung", "starting-the-application-and-signing-in.html", [
      [ "Start von Safe Share for Windows", "starting-the-application-and-signing-in.html#starting-the-application", null ],
      [ "Manuelle Anmeldung", "starting-the-application-and-signing-in.html#signing-in-manually", null ],
      [ "Konfiguration eines Mobilgeräts für Zwei-Faktor-Authentifizierung", "starting-the-application-and-signing-in.html#configuring-a-mobile-device-for-two-factor-authentication", null ],
      [ "Zugriff auf die Webanwendung", "starting-the-application-and-signing-in.html#accessing-the-web-application", null ],
      [ "Abmeldung", "starting-the-application-and-signing-in.html#signing-out", null ],
      [ "Verlassen von Safe Share for Windows", "starting-the-application-and-signing-in.html#exiting-the-application", null ]
    ] ],
    [ "Sicheres Upload, Speichern und Freigeben von Inhalten", "uploading-storing-and-sharing-content-securely.html", [
      [ "Zugriff auf den „Safe Share“-Ordner", "uploading-storing-and-sharing-content-securely.html#accessing-the-applications-folder", null ],
      [ "Änderung von Organisationen", "uploading-storing-and-sharing-content-securely.html#changing-organisations", null ],
      [ "Upload und Speichern von Inhalten im persönlichen Besitz", "uploading-storing-and-sharing-content-securely.html#uploading-and-storing-personally-owned-content", null ],
      [ "Freigabe sicherer Inhalte", "uploading-storing-and-sharing-content-securely.html#sharing-secure-content", null ],
      [ "Upload, Speichern und Freigeben von Inhalten „in einem Schritt“", "uploading-storing-and-sharing-content-securely.html#uploading-storing-and-sharing-content-in-one-go", null ],
      [ "Upload von Inhalten zur Freigabe über den Ordner eines anderen Benutzers", "uploading-storing-and-sharing-content-securely.html#uploading-content-to-share-through-another-users-folder", null ]
    ] ],
    [ "Zugriff auf sichere Inhalte", "accessing-secure-content.html", [
      [ "Anzeige, Ausdruck und Download Ihrer eigenen Inhalte", "accessing-secure-content.html#viewing-printing-downloading-your-own-content", null ],
      [ "Anzeige, Ausdruck und Download von (Ihnen freigegebenen) Inhalten", "accessing-secure-content.html#viewing-printing-downloading-content-shared-with-you", null ],
      [ "Der schreibgeschützte Content-Viewer von Safe Share for Windows", "accessing-secure-content.html#read-only-content-viewer", null ]
    ] ],
    [ "Verwaltung sicherer Inhalte", "managing-secure-content.html", [
      [ "Suchen sicherer Inhalte", "managing-secure-content.html#finding-secure-content", null ],
      [ "Organisation von Inhalten in Ordnern", "managing-secure-content.html#organising-content-with-folders", null ],
      [ "Aufhebung der Freigabe sicherer Inhalte", "managing-secure-content.html#unsharing-secure-content", null ],
      [ "Umbenennung von Elementen", "managing-secure-content.html#renaming-an-item", null ],
      [ "Entfernen von Inhalten im persönlichen Besitz", "managing-secure-content.html#removing-personally-owned-content", null ],
      [ "Entfernen über Ordner anderer Benutzer freigegebener Inhalte", "managing-secure-content.html#removing-content-shared-through-another-users-folder", null ],
      [ "Entfernen Ihres Zugriffs auf (Ihnen freigegebene) Inhalte", "managing-secure-content.html#removing-your-access-to-content-shared-with-you", null ]
    ] ],
    [ "Verwaltung der Einstellungen für die Cache-Richtlinien", "managing-cache-policy-settings.html", [
      [ "Zugriff auf die Einstellungen für die Cache-Richtlinien", "managing-cache-policy-settings.html#accessing-cache-policy-settings", null ],
      [ "Änderung der Einstellungen für die Cache-Richtlinien", "managing-cache-policy-settings.html#modifying-cache-policy-settings", null ]
    ] ],
    [ "Verwaltung Ihres Accounts", "managing-your-account.html", [
      [ "Verwaltung Ihrer Kontakte", "managing-your-account.html#managing-your-contacts", null ]
    ] ],
    [ "Support", "getting-support.html", null ],
    [ "Wichtiger Hinweis", "important-notice.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"accessing-secure-content.html"
];

var SYNCONMSG = 'Klicken um Panelsynchronisation auszuschalten';
var SYNCOFFMSG = 'Klicken um Panelsynchronisation einzuschalten';