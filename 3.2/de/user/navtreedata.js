var NAVTREE =
[
  [ "Safe Share-Webanwendung 3.2 Benutzeranleitung", "index.html", [
    [ "Anmeldung", "signing-in.html", [
      [ "Konfiguration eines Mobilgeräts für Zwei-Faktor-Authentifizierung", "signing-in.html#configuring-a-mobile-device-for-two-factor-authentication", null ],
      [ "Anmeldung mit Einzelanmeldung", "signing-in.html#signing-in-using-single-sign-on", null ],
      [ "Abmeldung", "signing-in.html#signing-out", null ]
    ] ],
    [ "Sicheres Upload, Speichern und Freigeben von Inhalten", "uploading-storing-and-sharing-content-securely.html", [
      [ "Upload und Speichern von Inhalten im persönlichen Besitz", "uploading-storing-and-sharing-content-securely.html#uploading-and-storing-personally-owned-content", null ],
      [ "Änderung von Organisationen", "uploading-storing-and-sharing-content-securely.html#changing-organisations", null ],
      [ "Freigabe sicherer Inhalte", "uploading-storing-and-sharing-content-securely.html#sharing-secure-content", null ],
      [ "Upload von Inhalten zur Freigabe über den Ordner eines anderen Benutzers", "uploading-storing-and-sharing-content-securely.html#uploading-content-to-share-through-another-users-folder", null ]
    ] ],
    [ "Zugriff auf sichere Inhalte", "accessing-secure-content.html", [
      [ "Anzeige und Ausdruck von Inhalten", "accessing-secure-content.html#viewing-and-printing-content", null ],
      [ "Vom Content-Viewer unterstützte Dateiformate", "accessing-secure-content.html#file-formats-supported-by-the-content-viewer", null ],
      [ "Download sicherer Inhalte", "accessing-secure-content.html#downloading-secure-content", null ]
    ] ],
    [ "Verwaltung sicherer Inhalte", "managing-secure-content.html", [
      [ "Suchen sicherer Inhalte", "managing-secure-content.html#finding-secure-content", null ],
      [ "Organisation von Inhalten in Ordnern", "managing-secure-content.html#organising-content-with-folders", null ],
      [ "Aufhebung der Freigabe sicherer Inhalte", "managing-secure-content.html#unsharing-secure-content", null ],
      [ "Umbenennung von Elementen", "managing-secure-content.html#renaming-an-item", null ],
      [ "Verschieben von Elementen", "managing-secure-content.html#moving-an-item", null ],
      [ "Entfernen von Inhalten im persönlichen Besitz", "managing-secure-content.html#removing-personally-owned-content", null ],
      [ "Entfernen über Ordner anderer Benutzer freigegebener Inhalte", "managing-secure-content.html#removing-content-shared-through-another-users-folder", null ],
      [ "Entfernen Ihres Zugriffs auf (Ihnen freigegebene) Inhalte", "managing-secure-content.html#removing-your-access-to-content-shared-with-you", null ]
    ] ],
    [ "Anzeige der an sicheren Inhalten ausgeführten Aktivitäten", "viewing-activities-on-secure-content.html", [
      [ "Arten der angezeigten Aktivitäten", "viewing-activities-on-secure-content.html#types-of-activities-indicated", null ]
    ] ],
    [ "Verwaltung Ihres Accounts", "managing-your-account.html", [
      [ "Aktualisierung Ihrer Account-Details", "managing-your-account.html#updating-your-account-details", null ],
      [ "Änderung Ihres Passworts", "managing-your-account.html#changing-your-password", null ],
      [ "Aktivierung und Deaktivierung von 2FA für Ihren Account", "managing-your-account.html#enabling-or-disabling-2fa-on-your-account", null ],
      [ "Beendigung von Sitzungen Ihres Benutzer-Accounts", "managing-your-account.html#terminating-your-user-accounts-sessions", null ],
      [ "Anzeige der Details Ihrer letzten Anmeldung", "managing-your-account.html#viewing-your-last-sign-in-details", null ],
      [ "Anzeige Ihrer Speichernutzung und Ihrer Speicherkontingente", "managing-your-account.html#viewing-your-storage-usages-and-quotas", null ],
      [ "Änderung Ihrer E-Mail-Benachrichtigungseinstellungen", "managing-your-account.html#modifying-your-email-notification-preferences", null ],
      [ "Widerruf von Kennungen auf registrierten Geräten", "managing-your-account.html#revoking-passcodes-on-registered-devices", null ],
      [ "Widerruf von Client-Anwendungen", "managing-your-account.html#revoking-client-applications", null ],
      [ "Verwaltung Ihrer Kontakte", "managing-your-account.html#managing-your-contacts", null ]
    ] ],
    [ "Support", "getting-support.html", null ],
    [ "Wichtiger Hinweis", "important-notice.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"accessing-secure-content.html"
];

var SYNCONMSG = 'Klicken um Panelsynchronisation auszuschalten';
var SYNCOFFMSG = 'Klicken um Panelsynchronisation einzuschalten';