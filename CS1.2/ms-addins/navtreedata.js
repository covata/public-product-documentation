var NAVTREE =
[
  [ "Microsoft Add-ins for SafeShare CS1.2", "index.html", [
    [ "Overview of Microsoft Add-ins", "microsoft-add-ins-overview.html", [
      [ "What is a Microsoft Add-in?", "microsoft-add-ins-overview.html#what-is-a-ms-addin", null ],
      [ "How to install a Microsoft Add-in?", "microsoft-add-ins-overview.html#how-to-install-a-ms-addin", null ],
      [ "Benefits of a SafeShare Microsoft Add-in", "microsoft-add-ins-overview.html#benefits-of-a-safeshare-ms-addin", null ]
    ] ],
    [ "Using the Microsoft Office Add-in", "using-the-ms-office-addin.html", null ],
    [ "Using the Microsoft Outlook Add-in", "using-the-ms-outlook-addin.html", [
      [ "Composing new E-mails", "using-the-ms-outlook-addin.html#composing-new-emails", null ],
      [ "Uploading and Securing Attachments from E-mails", "using-the-ms-outlook-addin.html#uploading-attachments-from-emails", null ]
    ] ],
    [ "Getting support", "getting-support.html", null ],
    [ "Important notice", "important-notice.html", null ],
    [ "SafeShare Manuals Home", "^https://docs.covata.com/", null ],
    [ "Support Home", "^https://support.covata.com/", null ]
  ] ]
];

var NAVTREEINDEX =
[
"getting-support.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';