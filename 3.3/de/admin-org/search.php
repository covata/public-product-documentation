<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/xhtml;charset=UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=9"/>
<meta name="generator" content="Doxygen 1.8.10"/>
<title>Organisations-Administratorenanleitung für Safe Share 3.3: Search</title>
<link href="tabs.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="dynsections.js"></script>
<!-- <link href="navtree.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="resize.js"></script>
<script type="text/javascript" src="navtreedata.js"></script>
<script type="text/javascript" src="navtree.js"></script>
<script type="text/javascript">
  $(document).ready(initResizable);
  $(window).load(resizeHeight);
</script> -->
<link href="navtree.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="resize.js"></script>
<script type="text/javascript" src="navtreedata.js"></script>
<script type="text/javascript" src="navtree-custom.js"></script>
<script type="text/javascript">
  $(document).ready(initResizable);
  $(window).load(resizeHeight);
</script>
<link href="search/search.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="search/search.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    if ($('.searchresults').length > 0) { searchBox.DOMSearchField().focus(); }
  });
</script>
<link rel="search" href="search_opensearch.php?v=opensearch.xml" type="application/opensearchdescription+xml" title="Organisations-Administratorenanleitung für Safe Share 3.3"/>
<link href="doxygen.css" rel="stylesheet" type="text/css" />
<!-- Load up CSS for Lightbox image expansion feature -->
<link href="lightbox.css" rel="stylesheet" type="text/css"/>
<link href="admin-guide.css" rel="stylesheet" type="text/css"/>
<!-- Load up correct CSS that defines TOC size based on screen/window size (non-jQuery/JavaScript version) -->
<link rel="stylesheet" media="only screen and (min-width: 1200px)" href="big-screen.css" type="text/css"/>
<link rel="stylesheet" media="only screen and (max-width: 1199px)" href="smaller-screen.css" type="text/css"/>
<!-- JQuery/JavaScript functionality for toggling TOC panel -->
<script type="text/javascript" src="toc-panel.js"></script>
<!-- JQuery/JavaScript functionality for switching languages -->
<script type="text/javascript" src="language-switcher.js"></script>
<link rel="author" href="../../humans.txt" />
</head>
<body>
<div id="top"><!-- do not remove this div, it is closed by doxygen! -->
<div id="titlearea">
<table cellspacing="0" cellpadding="0">
 <tbody>
 <tr style="height: 56px;">
  <td id="projectlogo"><img alt="Logo" src="covata-header-logo.png"/></td>
  <td id="projectalign" style="padding: 0px 0px 2px 15px;">
   <div id="projectname">Organisations-Administratorenanleitung
   </div>
   <!-- Language selection drop-down -->
   <div id="languageselection">
    <select id="languageselectdropdown">
     <option value="../de/admin-org/">Deutsch</option>
     <option value="../../admin-org/">English</option>
    </select>
   </div>
   <!-- Toggle TOC panel icon -->
   <div id="tocpaneltoggle">
    <img id="tocpaneltoggleimg" alt="Toggle TOC panel" src="images/toc-panel-toggle-icon.png">
   </div>
  </td>
 </tr>
 </tbody>
</table>
</div>
<!-- end header part -->
<!-- Erzeugt von Doxygen 1.8.10 -->
<script type="text/javascript">
var searchBox = new SearchBox("searchBox", "search",false,'Suchen');
</script>
  <div id="navrow1" class="tabs">
    <ul class="tablist">
      <li><a href="index.html"><span>Anfangsseite</span></a></li>
      <li><a href="pages.html"><span>Wichtigste&#160;Themen</span></a></li>
      <li><a href="https://docs.covata.com/de"><span>Covata-Dokumentation:&#160;Start</span></a></li>
      <li>
        <div id="MSearchBox" class="MSearchBoxInactive">
          <div class="left">
            <form id="FSearchBox" action="search.php" method="get">
              <img id="MSearchSelect" src="search/mag.png" alt=""/>
<script language="php">
require_once "search_functions.php";
main();
</script>
</div><!-- doc-contents -->
<!-- start footer part -->
<div id="nav-path" class="navpath"><!-- id is needed for treeview function! -->
  <ul>
    <li class="footer">Organisations-Administratorenanleitung für Safe Share 3.3, erstellt am Mit Nov 9 2016 von
    <a href="https://www.covata.com" target="_blank">
    <img class="footer" src="images/covata-footer-logo.png" alt="covata"/></a> </li>
  </ul>
</div>
<script src="lightbox.js"></script>
</body>
</html>
