<script language="php">

$config = array(
  'PROJECT_NAME' => "Organisations-Administratorenanleitung für Safe Share 3.3",
  'GENERATE_TREEVIEW' => true,
  'DISABLE_INDEX' => false,
);

$translator = array(
  'search_results_title' => "Suchergebnisse",
  'search_results' => array(
    0 => "Es wurden keine Dokumente zu Ihrer Suchanfrage gefunden.",
    1 => "Es wurde <b>1</b> Dokument zu Ihrer Suchanfrage gefunden.",
    2 => "Es wurden <b>\$num</b> Dokumente zu Ihrer Suchanfrage gefunden. Die besten Treffer werden zuerst angezeigt.",
  ),
  'search_matches' => "Treffer:",
  'search' => "Suchen",
  'split_bar' => "<div id=\"side-nav\" class=\"ui-resizable side-nav-resizable\">\n  <div id=\"nav-tree\">\n    <div id=\"nav-tree-contents\">\n      <div id=\"nav-sync\" class=\"sync\"></div>\n    </div>\n  </div>\n  <div id=\"splitbar\" style=\"-moz-user-select:none;\" \n       class=\"ui-resizable-handle\">\n  </div>\n</div>\n<script type=\"text/javascript\">\n$(document).ready(function(){initNavTree('search.html','');});\n</script>\n<div id=\"doc-content\">\n",
  'logo' => "Erzeugt am Mit Nov 9 2016 15:29:50 für Organisations-Administratorenanleitung für Safe Share 3.3 von&#160;\n<a href=\"http://www.doxygen.org/index.html\">\n<img class=\"footer\" src=\"doxygen.png\" alt=\"doxygen\"/></a> 1.8.10 ",
);

</script>
