var platform_administration_configuration_endpoints =
[
    [ "<access-service>/api/v1/config/server", "access-service-api-v1-config-server.html", [
      [ "URL structure", "access-service-api-v1-config-server.html#access-service-api-v1-config-server-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-server.html#access-service-api-v1-config-server-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-server.html#access-service-api-v1-config-server-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-server.html#access-service-api-config-server-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-server.html#access-service-api-v1-config-server-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-config-server.html#access-service-api-v1-config-server-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-config-server.html#access-service-api-v1-config-server-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/server/{configId}", "access-service-api-v1-config-server-configid.html", [
      [ "URL structure", "access-service-api-v1-config-server-configid.html#access-service-api-v1-config-server-configid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-server-configid.html#access-service-api-v1-config-server-configid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-server-configid.html#access-service-api-v1-config-server-configid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-server-configid.html#access-service-api-config-server-configid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-server-configid.html#access-service-api-v1-config-server-configid-required-headers", null ],
      [ "Parameters", "access-service-api-v1-config-server-configid.html#access-service-api-v1-config-server-configid-parameters", null ],
      [ "Returns", "access-service-api-v1-config-server-configid.html#access-service-api-v1-config-server-configid-returns", null ]
    ] ],
    [ "<access-service>/theme...", "access-service-theme.html", [
      [ "URL structure", "access-service-theme.html#access-service-theme-url-structure", null ],
      [ "Supported methods and overview", "access-service-theme.html#access-service-theme-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-theme.html#access-service-theme-detailed-description", null ],
      [ "Supported roles", "access-service-theme.html#access-service-theme-supported-roles", null ],
      [ "Required headers", "access-service-theme.html#access-service-theme-required-headers", null ],
      [ "Returns", "access-service-theme.html#access-service-theme-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/adminEmail", "access-service-api-v1-config-theme-adminemail.html", [
      [ "URL structure", "access-service-api-v1-config-theme-adminemail.html#access-service-api-v1-config-theme-adminemail-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-adminemail.html#access-service-api-v1-config-theme-adminemail-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-adminemail.html#access-service-api-v1-config-theme-adminemail-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-adminemail.html#access-service-api-v1-config-theme-adminemail-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-adminemail.html#access-service-api-v1-config-theme-adminemail-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-adminemail.html#access-service-api-v1-config-theme-adminemail-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-adminemail.html#access-service-api-v1-config-theme-adminemail-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/adminHelpUrl", "access-service-api-v1-config-theme-adminhelpurl.html", [
      [ "URL structure", "access-service-api-v1-config-theme-adminhelpurl.html#access-service-api-v1-config-theme-adminhelpurl-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-adminhelpurl.html#access-service-api-v1-config-theme-adminhelpurl-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-adminhelpurl.html#access-service-api-v1-config-theme-adminhelpurl-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-adminhelpurl.html#access-service-api-v1-config-theme-adminhelpurl-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-adminhelpurl.html#access-service-api-v1-config-theme-adminhelpurl-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-adminhelpurl.html#access-service-api-v1-config-theme-adminhelpurl-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-adminhelpurl.html#access-service-api-v1-config-theme-adminhelpurl-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/androidAuthenticatorUrl", "access-service-api-v1-config-theme-androidauthenticatorurl.html", [
      [ "URL structure", "access-service-api-v1-config-theme-androidauthenticatorurl.html#access-service-api-v1-config-theme-androidauthenticatorurl-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-androidauthenticatorurl.html#access-service-api-v1-config-theme-androidauthenticatorurl-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-androidauthenticatorurl.html#access-service-api-v1-config-theme-androidauthenticatorurl-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-androidauthenticatorurl.html#access-service-api-v1-config-theme-androidauthenticatorurl-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-androidauthenticatorurl.html#access-service-api-v1-config-theme-androidauthenticatorurl-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-androidauthenticatorurl.html#access-service-api-v1-config-theme-androidauthenticatorurl-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-androidauthenticatorurl.html#access-service-api-v1-config-theme-androidauthenticatorurl-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/applicationName", "access-service-api-v1-config-theme-applicationname.html", [
      [ "URL structure", "access-service-api-v1-config-theme-applicationname.html#access-service-api-v1-config-theme-applicationname-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-applicationname.html#access-service-api-v1-config-theme-applicationname-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-applicationname.html#access-service-api-v1-config-theme-applicationname-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-applicationname.html#access-service-api-v1-config-theme-applicationname-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-applicationname.html#access-service-api-v1-config-theme-applicationname-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-applicationname.html#access-service-api-v1-config-theme-applicationname-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-applicationname.html#access-service-api-v1-config-theme-applicationname-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/companyName", "access-service-api-v1-config-theme-companyname.html", [
      [ "URL structure", "access-service-api-v1-config-theme-companyname.html#access-service-api-v1-config-theme-companyname-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-companyname.html#access-service-api-v1-config-theme-companyname-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-companyname.html#access-service-api-v1-config-theme-companyname-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-companyname.html#access-service-api-v1-config-theme-companyname-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-companyname.html#access-service-api-v1-config-theme-companyname-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-companyname.html#access-service-api-v1-config-theme-companyname-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-companyname.html#access-service-api-v1-config-theme-companyname-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/copyright", "access-service-api-v1-config-theme-copyright.html", [
      [ "URL structure", "access-service-api-v1-config-theme-copyright.html#access-service-api-v1-config-theme-copyright-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-copyright.html#access-service-api-v1-config-theme-copyright-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-copyright.html#access-service-api-v1-config-theme-copyright-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-copyright.html#access-service-api-v1-config-theme-copyright-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-copyright.html#access-service-api-v1-config-theme-copyright-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-copyright.html#access-service-api-v1-config-theme-copyright-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-copyright.html#access-service-api-v1-config-theme-copyright-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/iphoneAuthenticatorUrl", "access-service-api-v1-config-theme-iphoneauthenticatorurl.html", [
      [ "URL structure", "access-service-api-v1-config-theme-iphoneauthenticatorurl.html#access-service-api-v1-config-theme-iphoneauthenticatorurl-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-iphoneauthenticatorurl.html#access-service-api-v1-config-theme-iphoneauthenticatorurl-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-iphoneauthenticatorurl.html#access-service-api-v1-config-theme-iphoneauthenticatorurl-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-iphoneauthenticatorurl.html#access-service-api-v1-config-theme-iphoneauthenticatorurl-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-iphoneauthenticatorurl.html#access-service-api-v1-config-theme-iphoneauthenticatorurl-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-iphoneauthenticatorurl.html#access-service-api-v1-config-theme-iphoneauthenticatorurl-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-iphoneauthenticatorurl.html#access-service-api-v1-config-theme-iphoneauthenticatorurl-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/legalUrl", "access-service-api-v1-config-theme-legalurl.html", [
      [ "URL structure", "access-service-api-v1-config-theme-legalurl.html#access-service-api-v1-config-theme-legalurl-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-legalurl.html#access-service-api-v1-config-theme-legalurl-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-legalurl.html#access-service-api-v1-config-theme-legalurl-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-legalurl.html#access-service-api-v1-config-theme-legalurl-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-legalurl.html#access-service-api-v1-config-theme-legalurl-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-legalurl.html#access-service-api-v1-config-theme-legalurl-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-legalurl.html#access-service-api-v1-config-theme-legalurl-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/orgAdminHelpUrl", "access-service-api-v1-config-theme-orgadminhelpurl.html", [
      [ "URL structure", "access-service-api-v1-config-theme-orgadminhelpurl.html#access-service-api-v1-config-theme-orgadminhelpurl-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-orgadminhelpurl.html#access-service-api-v1-config-theme-orgadminhelpurl-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-orgadminhelpurl.html#access-service-api-v1-config-theme-orgadminhelpurl-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-orgadminhelpurl.html#access-service-api-v1-config-theme-orgadminhelpurl-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-orgadminhelpurl.html#access-service-api-v1-config-theme-orgadminhelpurl-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-orgadminhelpurl.html#access-service-api-v1-config-theme-orgadminhelpurl-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-orgadminhelpurl.html#access-service-api-v1-config-theme-orgadminhelpurl-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/privacyUrl", "access-service-api-v1-config-theme-privacyurl.html", [
      [ "URL structure", "access-service-api-v1-config-theme-privacyurl.html#access-service-api-v1-config-theme-privacyurl-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-privacyurl.html#access-service-api-v1-config-theme-privacyurl-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-privacyurl.html#access-service-api-v1-config-theme-privacyurl-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-privacyurl.html#access-service-api-v1-config-theme-privacyurl-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-privacyurl.html#access-service-api-v1-config-theme-privacyurl-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-privacyurl.html#access-service-api-v1-config-theme-privacyurl-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-privacyurl.html#access-service-api-v1-config-theme-privacyurl-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/supportLink", "access-service-api-v1-config-theme-supportlink.html", [
      [ "URL structure", "access-service-api-v1-config-theme-supportlink.html#access-service-api-v1-config-theme-supportlink-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-supportlink.html#access-service-api-v1-config-theme-supportlink-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-supportlink.html#access-service-api-v1-config-theme-supportlink-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-supportlink.html#access-service-api-v1-config-theme-supportlink-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-supportlink.html#access-service-api-v1-config-theme-supportlink-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-supportlink.html#access-service-api-v1-config-theme-supportlink-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-supportlink.html#access-service-api-v1-config-theme-supportlink-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/versionUrl", "access-service-api-v1-config-theme-versionurl.html", [
      [ "URL structure", "access-service-api-v1-config-theme-versionurl.html#access-service-api-v1-config-theme-versionurl-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-versionurl.html#access-service-api-v1-config-theme-versionurl-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-versionurl.html#access-service-api-v1-config-theme-versionurl-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-versionurl.html#access-service-api-v1-config-theme-versionurl-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-versionurl.html#access-service-api-v1-config-theme-versionurl-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-versionurl.html#access-service-api-v1-config-theme-versionurl-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-versionurl.html#access-service-api-v1-config-theme-versionurl-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/webappHelpUrl", "access-service-api-v1-config-theme-webapphelpurl.html", [
      [ "URL structure", "access-service-api-v1-config-theme-webapphelpurl.html#access-service-api-v1-config-theme-webapphelpurl-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-webapphelpurl.html#access-service-api-v1-config-theme-webapphelpurl-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-webapphelpurl.html#access-service-api-v1-config-theme-webapphelpurl-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-webapphelpurl.html#access-service-api-v1-config-theme-webapphelpurl-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-webapphelpurl.html#access-service-api-v1-config-theme-webapphelpurl-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-webapphelpurl.html#access-service-api-v1-config-theme-webapphelpurl-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-webapphelpurl.html#access-service-api-v1-config-theme-webapphelpurl-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config/theme/windowsAuthenticatorUrl", "access-service-api-v1-config-theme-windowsauthenticatorurl.html", [
      [ "URL structure", "access-service-api-v1-config-theme-windowsauthenticatorurl.html#access-service-api-v1-config-theme-windowsauthenticatorurl-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config-theme-windowsauthenticatorurl.html#access-service-api-v1-config-theme-windowsauthenticatorurl-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config-theme-windowsauthenticatorurl.html#access-service-api-v1-config-theme-windowsauthenticatorurl-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-config-theme-windowsauthenticatorurl.html#access-service-api-v1-config-theme-windowsauthenticatorurl-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-config-theme-windowsauthenticatorurl.html#access-service-api-v1-config-theme-windowsauthenticatorurl-required-headers", null ],
      [ "Required body", "access-service-api-v1-config-theme-windowsauthenticatorurl.html#access-service-api-v1-config-theme-windowsauthenticatorurl-required-body", null ],
      [ "Returns", "access-service-api-v1-config-theme-windowsauthenticatorurl.html#access-service-api-v1-config-theme-windowsauthenticatorurl-returns", null ]
    ] ],
    [ "<access-service>/api/v1/config", "access-service-api-v1-config.html", [
      [ "URL structure", "access-service-api-v1-config.html#access-service-api-v1-config-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-config.html#access-service-api-v1-config-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-config.html#access-service-api-v1-config-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-config.html#access-service-api-config-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-config.html#access-service-api-v1-config-required-headers", null ],
      [ "Returns", "access-service-api-v1-config.html#access-service-api-v1-config-returns", null ]
    ] ]
];