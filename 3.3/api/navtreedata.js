var NAVTREE =
[
  [ "Covata API 3.3 Documentation", "index.html", [
    [ "Developer's Guide", "developers-guide.html", "developers-guide" ],
    [ "API Reference Guide", "api-reference-guide.html", "api-reference-guide" ],
    [ "Getting Support", "getting-support.html", null ],
    [ "Important notice", "important-notice.html", null ],
    [ "Covata Documentation Home", "^https://docs.covata.com/index.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"access-service-api-oauth-authorize.html",
"access-service-api-v1-i18n-bundles-localecode-delete.html#access-service-api-v1-i18n-bundles-localecode-delete-required-headers",
"access-service-api-v1-organisations-orggroupid-statistics-plans.html",
"access-service-api-v1-users-me.html#access-service-api-v1-users-me-supported-roles"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';