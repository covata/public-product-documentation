var file_content_handling_endpoints =
[
    [ "<content-service>/api/v1/objects/{objectId}/contents", "content-service-api-v1-objects-objectid-contents.html", [
      [ "URL structure", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-detailed-description", null ],
      [ "Supported roles and conditions", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-supported-roles-and-conditions", null ],
      [ "Required headers", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-headers", null ],
      [ "Required POST-request parameters", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-post-request-parameters", null ],
      [ "Required parameters for multi-chunk uploads", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-parameters-for-multi-chunk-uploads", null ],
      [ "Required GET-request parameters", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-required-get-request-parameters", null ],
      [ "Optional GET-request parameters", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-optional-get-request-parameters", null ],
      [ "Returns", "content-service-api-v1-objects-objectid-contents.html#content-service-api-v1-objects-objectid-contents-returns", null ]
    ] ],
    [ "<content-service>/api/v1/objects/{objectId}/view", "content-service-api-v1-objects-objectid-view.html", [
      [ "URL structure", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-detailed-description", null ],
      [ "Supported roles and conditions", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-supported-roles-and-conditions", null ],
      [ "Required headers", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-required-headers", null ],
      [ "Optional parameters", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-optional-parameters-on-GET", null ],
      [ "Returns", "content-service-api-v1-objects-objectid-view.html#content-service-api-v1-objects-objectid-view-returns-from-a-GET-or-PUT-request", null ]
    ] ],
    [ "<content-service>/api/v1/objects/{objectId}/versions", "content-service-api-v1-objects-objectid-versions.html", [
      [ "URL structure", "content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-detailed-description", null ],
      [ "Supported roles and conditions", "content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-supported-roles-and-conditions", null ],
      [ "Required headers", "content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-required-headers", null ],
      [ "Required parameters", "content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-required-parameters", null ],
      [ "Required parameters for multi-chunk uploads", "content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-required-parameters-for-multi-chunk-uploads", null ],
      [ "Returns", "content-service-api-v1-objects-objectid-versions.html#content-service-api-v1-objects-objectid-versions-returns", null ]
    ] ],
    [ "<content-service>/api/v1/versions/{versionId}/contents", "content-service-api-v1-versions-versionid-contents.html", [
      [ "URL structure", "content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-detailed-description", null ],
      [ "Supported roles and conditions", "content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-supported-roles-and-conditions", null ],
      [ "Required headers", "content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-required-headers", null ],
      [ "Required parameters", "content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-required-parameters", null ],
      [ "Optional parameters", "content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-optional-parameters", null ],
      [ "Returns", "content-service-api-v1-versions-versionid-contents.html#content-service-api-v1-versions-versionid-contents-returns", null ]
    ] ],
    [ "<content-service>/api/v1/versions/{versionId}/view", "content-service-api-v1-versions-versionid-view.html", [
      [ "URL structure", "content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-detailed-description", null ],
      [ "Supported roles and conditions", "content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-supported-roles-and-conditions", null ],
      [ "Required headers", "content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-required-headers", null ],
      [ "Required parameters", "content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-required-parameters", null ],
      [ "Optional parameters", "content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-optional-parameters", null ],
      [ "Returns", "content-service-api-v1-versions-versionid-view.html#content-service-api-v1-versions-versionid-view-returns", null ]
    ] ]
];