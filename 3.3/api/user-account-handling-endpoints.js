var user_account_handling_endpoints =
[
    [ "<access-service>/api/v1/users/me", "access-service-api-v1-users-me.html", [
      [ "URL structure", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-users-me.html#access-service-api-v1-users-me-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/expire", "access-service-api-v1-users-me-expire.html", [
      [ "URL structure", "access-service-api-v1-users-me-expire.html#access-service-api-v1-users-me-expire-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-expire.html#access-service-api-v1-users-me-expire-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-expire.html#access-service-api-v1-users-me-expire-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-me-expire.html#access-service-api-v1-users-me-expire-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-me-expire.html#access-service-api-v1-users-me-expire-required-headers", null ],
      [ "Returns", "access-service-api-v1-users-me-expire.html#access-service-api-v1-users-me-expire-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/applications/approvals", "access-service-api-v1-users-me-applications-approvals.html", [
      [ "URL structure", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-v1-users-me-applications-approvals-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-v1-users-me-applications-approvals-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-v1-users-me-applications-approvals-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-v1-users-me-applications-approvals-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-v1-users-me-applications-approvals-required-headers", null ],
      [ "Returns", "access-service-api-v1-users-me-applications-approvals.html#access-service-api-v1-users-me-applications-approvals-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/applications/approvals/{approvalId}", "access-service-api-v1-users-me-applications-approvals-approvalid.html", [
      [ "URL structure", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-v1-users-me-applications-approvals-approvalid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-v1-users-me-applications-approvals-approvalid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-v1-users-me-applications-approvals-approvalid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-v1-users-me-applications-approvals-approvalid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-v1-users-me-applications-approvals-approvalid-required-headers", null ],
      [ "Returns", "access-service-api-v1-users-me-applications-approvals-approvalid.html#access-service-api-v1-users-me-applications-approvals-approvalid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/contacts", "access-service-api-v1-users-me-contacts.html", [
      [ "URL structure", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-required-headers", null ],
      [ "Required POST-request parameters", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-required-post-request-parameters", null ],
      [ "Optional POST-request parameters", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-optional-post-request-parameters", null ],
      [ "Optional GET-request parameters", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-optional-get-request-parameters", null ],
      [ "Returns", "access-service-api-v1-users-me-contacts.html#access-service-api-v1-users-me-contacts-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/contacts/{contactId}", "access-service-api-v1-users-me-contacts-contactid.html", [
      [ "URL structure", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-required-headers", null ],
      [ "Required PUT-request parameters", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-required-put-request-parameters", null ],
      [ "Optional PUT-request parameters", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-optional-put-request-parameters", null ],
      [ "Returns", "access-service-api-v1-users-me-contacts-contactid.html#access-service-api-v1-users-me-contacts-contactid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/me/notifications/email", "access-service-api-v1-users-me-notifications-email.html", [
      [ "URL structure", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-required-headers", null ],
      [ "Required PUT-request parameters", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-required-put-request-parameters", null ],
      [ "Returns", "access-service-api-v1-users-me-notifications-email.html#access-service-api-v1-users-me-notifications-email-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/users", "access-service-api-v1-organisations-orggroupid-users.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-users.html#access-service-api-v1-organisations-orggroupid-users-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-users.html#access-service-api-v1-organisations-orggroupid-users-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-users.html#access-service-api-v1-organisations-orggroupid-users-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orggroupid-users.html#access-service-api-v1-organisations-orggroupid-users-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-users.html#access-service-api-v1-organisations-orggroupid-users-required-headers", null ],
      [ "Required POST-request parameters", "access-service-api-v1-organisations-orggroupid-users.html#access-service-api-v1-organisations-orggroupid-users-required-post-request-parameters", null ],
      [ "Optional GET-request parameters", "access-service-api-v1-organisations-orggroupid-users.html#access-service-api-v1-organisations-orggroupid-users-optional-get-request-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-users.html#access-service-api-v1-organisations-orggroupid-users-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/users/{userId}", "access-service-api-v1-organisations-orggroupid-users-userid.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-users-userid.html#access-service-api-v1-organisations-orggroupid-users-userid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-users-userid.html#access-service-api-v1-organisations-orggroupid-users-userid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-users-userid.html#access-service-api-v1-organisations-orggroupid-users-userid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orggroupid-users-userid.html#access-service-api-v1-organisations-orggroupid-users-userid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-users-userid.html#access-service-api-v1-organisations-orggroupid-users-userid-required-headers", null ],
      [ "Required PUT-request parameters", "access-service-api-v1-organisations-orggroupid-users-userid.html#access-service-api-v1-organisations-orggroupid-users-userid-required-put-request-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-users-userid.html#access-service-api-v1-organisations-orggroupid-users-userid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/users/{userId}/itemCount", "access-service-api-v1-organisations-orggroupid-users-userid-itemcount.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-users-userid-itemcount.html#access-service-api-v1-organisations-orggroupid-users-userid-itemcount-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-users-userid-itemcount.html#access-service-api-v1-organisations-orggroupid-users-userid-itemcount-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-users-userid-itemcount.html#access-service-api-v1-organisations-orggroupid-users-userid-itemcount-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orggroupid-users-userid-itemcount.html#access-service-api-v1-organisations-orggroupid-users-userid-itemcount-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-users-userid-itemcount.html#access-service-api-v1-organisations-orggroupid-users-userid-itemcount-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-users-userid-itemcount.html#access-service-api-v1-organisations-orggroupid-users-userid-itemcount-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/users/import", "access-service-api-v1-organisations-orggroupid-users-import.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-users-import.html#access-service-api-v1-organisations-orggroupid-users-import-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-users-import.html#access-service-api-v1-organisations-orggroupid-users-import-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-users-import.html#access-service-api-v1-organisations-orggroupid-users-import-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orggroupid-users-import.html#access-service-api-v1-organisations-orggroupid-users-import-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-users-import.html#access-service-api-v1-organisations-orggroupid-users-import-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-organisations-orggroupid-users-import.html#access-service-api-v1-organisations-orggroupid-users-import-required-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-users-import.html#access-service-api-v1-organisations-orggroupid-users-import-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users", "access-service-api-v1-users.html", [
      [ "URL structure", "access-service-api-v1-users.html#access-service-api-v1-users-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users.html#access-service-api-v1-users-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users.html#access-service-api-v1-users-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-users.html#access-service-api-v1-users-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-users.html#access-service-api-v1-users-required-headers", null ],
      [ "Required PUT-request parameters", "access-service-api-v1-users.html#access-service-api-v1-users-required-put-request-parameters", null ],
      [ "Optional GET-request parameters", "access-service-api-v1-users.html#access-service-api-v1-users-optional-get-request-parameters", null ],
      [ "Returns", "access-service-api-v1-users.html#access-service-api-v1-users-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/{userId}", "access-service-api-v1-users-userid.html", [
      [ "URL structure", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-required-parameters", null ],
      [ "Returns", "access-service-api-v1-users-userid.html#access-service-api-v1-users-userid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/users/{userId}/unlock", "access-service-api-v1-users-userid-unlock.html", [
      [ "URL structure", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-required-headers", null ],
      [ "Returns", "access-service-api-v1-users-userid-unlock.html#access-service-api-v1-users-userid-unlock-returns", null ]
    ] ]
];