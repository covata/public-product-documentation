var getting_started_guide =
[
    [ "What the Covata API can do", "getting-started-guide.html#what-the-covata-api-can-do", null ],
    [ "Tools for accessing the Covata API", "getting-started-guide.html#tools-for-accessing-the-covata-api", null ],
    [ "Authentication and authorization", "authentication-and-authorization.html", [
      [ "Registering your application on the Covata Platform", "authentication-and-authorization.html#registering-your-application-on-the-covata-platform", null ],
      [ "Overview of the OAuth 2.0 process", "authentication-and-authorization.html#overview-of-the-oauth-2-0-process", null ],
      [ "Authorizing your application", "authentication-and-authorization.html#authorizing-your-application", [
        [ "Using the password grant type", "authentication-and-authorization.html#using-the-password-grant-type", null ]
      ] ],
      [ "Completing authorization", "authentication-and-authorization.html#completing-authorization", [
        [ "Completing password grant type authorization", "authentication-and-authorization.html#completing-password-grant-type-authorization", null ]
      ] ]
    ] ],
    [ "Creating Secure Objects and collections", "creating-secure-objects-and-collections.html", [
      [ "Definitions", "creating-secure-objects-and-collections.html#definitions", [
        [ "Secure Object", "creating-secure-objects-and-collections.html#secure-object", null ],
        [ "Collection", "creating-secure-objects-and-collections.html#collection", null ],
        [ "Item", "creating-secure-objects-and-collections.html#item", null ]
      ] ],
      [ "Obtaining a cryptographic key", "creating-secure-objects-and-collections.html#obtaining-a-cryptographic-key", null ],
      [ "Initializing a new 'Incomplete' Secure Object", "creating-secure-objects-and-collections.html#initializing-a-new-incomplete-secure-object", null ],
      [ "Creating a completed Secure Object (data encrypted locally)", "creating-secure-objects-and-collections.html#creating-a-completed-secure-object-data-encrypted-locally", null ],
      [ "Uploading a completed Secure Object's encrypted data", "creating-secure-objects-and-collections.html#uploading-a-completed-secure-objects-encrypted-data", null ],
      [ "Creating a completed Secure Object (uploading unencrypted data)", "creating-secure-objects-and-collections.html#creating-a-completed-secure-object-uploading-unencrypted-data", null ],
      [ "Creating a collection for managing Secure Objects", "creating-secure-objects-and-collections.html#creating-a-collection-for-managing-secure-objects", null ]
    ] ],
    [ "Retrieving information about items from the Covata Platform", "retrieving-information-about-items-from-the-covata-platform.html", [
      [ "Retrieving info about all immediate items of a common parent", "retrieving-information-about-items-from-the-covata-platform.html#retrieving-info-about-all-immediate-items-of-a-common-parent", null ],
      [ "Retrieving info about all items filtered by text search", "retrieving-information-about-items-from-the-covata-platform.html#retrieving-info-about-all-items-filtered-by-text-search", null ],
      [ "Retrieving info about all items based on ownership or sharing status", "retrieving-information-about-items-from-the-covata-platform.html#retrieving-info-about-items-based-on-ownership-or-sharing-status", null ],
      [ "Retrieving info about all ancestral collections of an item", "retrieving-information-about-items-from-the-covata-platform.html#retrieving-info-about-all-ancestral-collections-of-an-item", null ],
      [ "Sorting, ordering and paginating the list of item info", "retrieving-information-about-items-from-the-covata-platform.html#sorting-ordering-and-paginating-the-list-of-item-info", null ]
    ] ],
    [ "Sharing items", "sharing-items.html", [
      [ "Sharing a Secure Object with collaborators", "sharing-items.html#sharing-a-secure-object-with-collaborators", null ],
      [ "Sharing a collection with collaborators", "sharing-items.html#sharing-a-collection-with-collaborators", null ],
      [ "Determining existing collaborators on an item", "sharing-items.html#determining-existing-collaborators-on-an-item", null ],
      [ "Modifying existing collaborators on an item", "sharing-items.html#modifying-existing-collaborators-on-an-item", null ]
    ] ]
];