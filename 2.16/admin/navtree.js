var NAVTREE =
[
  [ "Covata Platform 2.16 Administrator's Guide", "index.html", [
    [ "Accessing Covata Administration", "accessing-administration.html", [
      [ "Signing in", "accessing-administration.html#signing-in", null ],
      [ "The Covata Administration interface", "accessing-administration.html#the-administration-interface", null ],
      [ "The dashboard", "accessing-administration.html#the-dashboard", null ],
      [ "Signing out", "accessing-administration.html#signing-out", null ],
      [ "Accessing for the first time", "accessing-administration.html#accessing-for-the-first-time", null ]
    ] ],
    [ "Administering users", "administering-users.html", [
      [ "A user's fields", "administering-users.html#a-users-fields", null ],
      [ "A user's roles", "administering-users.html#a-users-roles", null ],
      [ "Adding a new user", "administering-users.html#adding-a-new-user", null ],
      [ "Adding new users in bulk (from CSV)", "administering-users.html#adding-new-users-in-bulk-from-csv", null ],
      [ "Finding existing users", "administering-users.html#finding-existing-users", null ],
      [ "Editing an existing user", "administering-users.html#editing-an-existing-user", null ],
      [ "Changing your password", "administering-users.html#changing-your-password", null ],
      [ "Deleting users", "administering-users.html#deleting-users", null ],
      [ "Expiring users' tokens/sessions", "administering-users.html#expiring-users-tokens-sessions", null ],
      [ "Disabling or re-enabling users", "administering-users.html#disabling-or-re-enabling-users", null ],
      [ "Forcing users to reset their passwords", "administering-users.html#forcing-users-to-reset-their-passwords", null ],
      [ "Unlocking users' accounts", "administering-users.html#unlocking-users-accounts", null ],
      [ "Enabling 2FA on a user account", "administering-users.html#enabling-2fa-on-a-user-account", null ],
      [ "Re-configuring 2FA for a user account", "administering-users.html#re-configuring-2fa-for-a-user-account", null ],
      [ "Configuring columns on the 'Users' page", "administering-users.html#configuring-columns-on-the-users-page", null ]
    ] ],
    [ "Managing user whitelists", "managing-user-whitelists.html", [
      [ "Managing the user whitelist", "managing-user-whitelists.html#managing-the-user-whitelist", [
        [ "Adding new user whitelist rules", "managing-user-whitelists.html#adding-new-user-whitelist-rules", null ],
        [ "Editing an existing user whitelist rule's description", "managing-user-whitelists.html#editing-an-existing-user-whitelist-rules-description", null ],
        [ "Deleting existing user whitelist rules", "managing-user-whitelists.html#deleting-existing-user-whitelist-rules", null ]
      ] ]
    ] ],
    [ "Configuring LDAP", "configuring-ldap.html", [
      [ "Configuring an LDAP connection", "configuring-ldap.html#configuring-an-ldap-connection", [
        [ "Adding a new LDAP connection", "configuring-ldap.html#adding-a-new-ldap-connection", null ],
        [ "Editing an existing LDAP connection", "configuring-ldap.html#editing-an-existing-ldap-connection", null ]
      ] ],
      [ "Disabling or re-enabling an LDAP connection", "configuring-ldap.html#disabling-or-re-enabling-an-ldap-connection", null ]
    ] ],
    [ "Configuring single sign-on using an IdP", "configuring-single-sign-on-using-an-idp.html", [
      [ "Configuring an IdP service", "configuring-single-sign-on-using-an-idp.html#configuring-an-idp-service", [
        [ "Adding an IdP service configuration", "configuring-single-sign-on-using-an-idp.html#adding-an-idp-service-configuration", null ],
        [ "Editing the IdP service configuration", "configuring-single-sign-on-using-an-idp.html#editing-the-idp-service-configuration", null ]
      ] ],
      [ "Disabling or re-enabling the IdP service configuration", "configuring-single-sign-on-using-an-idp.html#disabling-or-re-enabling-the-idp-service-configuration", null ]
    ] ],
    [ "Managing labels", "managing-labels.html", [
      [ "Adding a new label", "managing-labels.html#adding-a-new-label", null ],
      [ "Deleting an existing label", "managing-labels.html#deleting-an-existing-label", null ]
    ] ],
    [ "Managing groups", "managing-groups.html", [
      [ "Adding a new group", "managing-groups.html#adding-a-new-group", null ],
      [ "Modifying users on an existing group", "managing-groups.html#modifying-users-on-an-existing-group", null ],
      [ "Modifying labels on an existing group", "managing-groups.html#modifying-labels-on-an-existing-group", null ],
      [ "Editing an existing group", "managing-groups.html#editing-an-existing-group", null ],
      [ "Deleting an existing group", "managing-groups.html#deleting-an-existing-group", null ]
    ] ],
    [ "Managing users' storage quotas through plans", "managing-users-storage-quotas-through-plans.html", [
      [ "Adding a new plan", "managing-users-storage-quotas-through-plans.html#adding-a-new-plan", null ],
      [ "Editing an existing plan", "managing-users-storage-quotas-through-plans.html#editing-an-existing-plan", null ],
      [ "Deleting an existing plan", "managing-users-storage-quotas-through-plans.html#deleting-an-existing-plan", null ]
    ] ],
    [ "Transferring user ownership", "transferring-user-ownership.html", [
      [ "Transferring ownership from one user to another", "transferring-user-ownership.html#transferring-ownership-from-one-user-to-another", null ]
    ] ],
    [ "Administering files", "administering-files.html", [
      [ "Finding Secure Objects", "administering-files.html#finding-secure-objects", null ],
      [ "A Secure Object's fields", "administering-files.html#a-secure-objects-fields", null ],
      [ "Viewing activities on Secure Objects", "administering-files.html#viewing-activities-on-secure-objects", null ],
      [ "Disabling or re-enabling Secure Objects", "administering-files.html#disabling-or-re-enabling-secure-objects", null ]
    ] ],
    [ "Generating reports", "generating-reports.html", [
      [ "View a report", "generating-reports.html#view-reports", null ],
      [ "Report parameters", "generating-reports.html#report-parameters", null ]
    ] ],
    [ "Configuring client applications", "configuring-client-applications.html", [
      [ "Configuring a client application", "configuring-client-applications.html#configuring-a-client-application", [
        [ "Registering a new client application", "configuring-client-applications.html#registering-a-new-client-application", null ],
        [ "Editing a registered client application", "configuring-client-applications.html#editing-a-registered-client-application", null ]
      ] ],
      [ "Grant type details", "configuring-client-applications.html#grant-type-details", null ],
      [ "Disabling and re-enabling a client application", "configuring-client-applications.html#disabling-or-re-enabling-a-client-application", null ]
    ] ],
    [ "Configuring Covata Administration properties", "configuring-administration-properties.html", [
      [ "Modifying a Covata Administration property value", "configuring-administration-properties.html#modifying-an-administration-property-value", null ]
    ] ],
    [ "Re-theming Safe Share and Covata Administration", "re-theming-the-application-and-administration.html", [
      [ "Modifying a theme property value", "re-theming-the-application-and-administration.html#modifying-a-theme-property-value", null ],
      [ "Modifying a logo", "re-theming-the-application-and-administration.html#modifying-a-logo", null ],
      [ "Sign-in page logo", "re-theming-the-application-and-administration.html#sign-in-page-logo", null ],
      [ "Safe Share web application logos, name and color", "re-theming-the-application-and-administration.html#the-web-application-logos-name-and-color", null ]
    ] ],
    [ "Managing internationalization", "managing-internationalization.html", [
      [ "Changing the 'System Default' language", "managing-internationalization.html#changing-the-system-default-language", null ],
      [ "Exporting a language bundle", "managing-internationalization.html#exporting-a-language-bundle", null ],
      [ "Uploading a custom/external language bundle", "managing-internationalization.html#uploading-a-custom-external-language-bundle", null ],
      [ "Deleting or resetting a language bundle", "managing-internationalization.html#deleting-or-resetting-a-language-bundle", null ]
    ] ],
    [ "Appendix", "appendix.html", [
      [ "Managing Safe Share for iOS through MDM software", "appendix.html#managing-safe-share-for-ios-through-mdm-software", [
        [ "Step 1 - Install Safe Share for iOS via MDM software", "appendix.html#step-1-install-safe-share-for-ios-via-mdm-software", null ],
        [ "Step 2 - Configure Safe Share for iOS via MDM software", "appendix.html#step-2-configure-safe-share-for-ios-via-mdm-software", null ],
        [ "The 'mdm-configuration.plist' file", "appendix.html#the-mdm-configuration-plist-file", null ]
      ] ]
    ] ],
    [ "Getting Support", "getting-support.html", null ],
    [ "Important notice", "important-notice.html", null ],
    [ "Covata Documentation Home", "^https://docs.covata.com/index.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"accessing-administration.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
var navTreeSubIndices = new Array();

function getData(varName)
{
  var i = varName.lastIndexOf('/');
  var n = i>=0 ? varName.substring(i+1) : varName;
  return eval(n.replace(/\-/g,'_'));
}

function stripPath(uri)
{
  return uri.substring(uri.lastIndexOf('/')+1);
}

function stripPath2(uri)
{
  var i = uri.lastIndexOf('/');
  var s = uri.substring(i+1);
  var m = uri.substring(0,i+1).match(/\/d\w\/d\w\w\/$/);
  return m ? uri.substring(i-6) : s;
}

function hashValue()
{
  return $(location).attr('hash').substring(1).replace(/[^\w\-]/g,'');
}

function hashUrl()
{
  return '#'+hashValue();
}

function pathName()
{
  return $(location).attr('pathname').replace(/[^-A-Za-z0-9+&@#/%?=~_|!:,.;\(\)]/g, '');
}

function localStorageSupported()
{
  try {
    return 'localStorage' in window && window['localStorage'] !== null && window.localStorage.getItem;
  }
  catch(e) {
    return false;
  }
}


function storeLink(link)
{
  if (!$("#nav-sync").hasClass('sync') && localStorageSupported()) {
      window.localStorage.setItem('navpath',link);
  }
}

function deleteLink()
{
  if (localStorageSupported()) {
    window.localStorage.setItem('navpath','');
  }
}

function cachedLink()
{
  if (localStorageSupported()) {
    return window.localStorage.getItem('navpath');
  } else {
    return '';
  }
}

function getScript(scriptName,func,show)
{
  var head = document.getElementsByTagName("head")[0]; 
  var script = document.createElement('script');
  script.id = scriptName;
  script.type = 'text/javascript';
  script.onload = func; 
  script.src = scriptName+'.js'; 
  if ($.browser.msie && $.browser.version<=8) { 
    // script.onload does not work with older versions of IE
    script.onreadystatechange = function() {
      if (script.readyState=='complete' || script.readyState=='loaded') { 
        func(); if (show) showRoot(); 
      }
    }
  }
  head.appendChild(script); 
}

function createIndent(o,domNode,node,level)
{
  var level=-1;
  var n = node;
  while (n.parentNode) { level++; n=n.parentNode; }
  if (node.childrenData) {
    var imgNode = document.createElement("img");
    imgNode.style.paddingLeft=(16*level).toString()+'px';
    imgNode.width  = 16;
    imgNode.height = 22;
    imgNode.border = 0;
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() {
      if (node.expanded) {
        $(node.getChildrenUL()).slideUp("fast");
        node.plus_img.src = node.relpath+"ftv2pnode.png";
        node.expanded = false;
      } else {
        expandNode(o, node, false, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
    imgNode.src = node.relpath+"ftv2pnode.png";
  } else {
    var span = document.createElement("span");
    span.style.display = 'inline-block';
    span.style.width   = 16*(level+1)+'px';
    span.style.height  = '22px';
    span.innerHTML = '&#160;';
    domNode.appendChild(span);
  } 
}

var animationInProgress = false;

function gotoAnchor(anchor,aname,updateLocation)
{
  var pos, docContent = $('#doc-content');
  var ancParent = $(anchor.parent());
  if (ancParent.hasClass('memItemLeft') ||
      ancParent.hasClass('fieldname') ||
      ancParent.hasClass('fieldtype') ||
      ancParent.is(':header'))
  {
    pos = ancParent.position().top;
  } else if (anchor.position()) {
    pos = anchor.position().top;
  }
  if (pos) {
    var dist = Math.abs(Math.min(
               pos-docContent.offset().top,
               docContent[0].scrollHeight-
               docContent.height()-docContent.scrollTop()));
    animationInProgress=true;
    docContent.animate({
      scrollTop: pos + docContent.scrollTop() - docContent.offset().top
    },Math.max(50,Math.min(500,dist)),function(){
      if (updateLocation) window.location.href=aname;
      animationInProgress=false;
    });
  }
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  node.expanded = false;
  a.appendChild(node.label);
  if (link) {
    var url;
    if (link.substring(0,1)=='^') {
      url = link.substring(1);
      link = url;
    } else {
      url = node.relpath+link;
    }
    a.className = stripPath(link.replace('#',':'));
    if (link.indexOf('#')!=-1) {
      var aname = '#'+link.split('#')[1];
      var srcPage = stripPath(pathName());
      var targetPage = stripPath(link.split('#')[0]);
      a.href = srcPage!=targetPage ? url : "javascript:void(0)"; 
      a.onclick = function(){
        storeLink(link);
        if (!$(a).parent().parent().hasClass('selected'))
        {
          $('.item').removeClass('selected');
          $('.item').removeAttr('id');
          $(a).parent().parent().addClass('selected');
          $(a).parent().parent().attr('id','selected');
        }
        var anchor = $(aname);
        gotoAnchor(anchor,aname,true);
      };
    } else {
      a.href = url;
      a.onclick = function() { storeLink(link); }
    }
  } else {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() {
    if (!node.childrenUL) {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  (function (){ // retry until we can scroll to the selected item
    try {
      var navtree=$('#nav-tree');
      navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
    } catch (err) {
      setTimeout(arguments.callee, 0);
    }
  })();
}

function expandNode(o, node, imm, showRoot)
{
  if (node.childrenData && !node.expanded) {
    if (typeof(node.childrenData)==='string') {
      var varName    = node.childrenData;
      getScript(node.relpath+varName,function(){
        node.childrenData = getData(varName);
        expandNode(o, node, imm, showRoot);
      }, showRoot);
    } else {
      if (!node.childrenVisited) {
        getNode(o, node);
      } if (imm || ($.browser.msie && $.browser.version>8)) { 
        // somehow slideDown jumps to the start of tree for IE9 :-(
        $(node.getChildrenUL()).show();
      } else {
        $(node.getChildrenUL()).slideDown("fast");
      }
      if (node.isLast) {
        node.plus_img.src = node.relpath+"ftv2mlastnode.png";
      } else {
        node.plus_img.src = node.relpath+"ftv2mnode.png";
      }
      node.expanded = true;
    }
  }
}

function glowEffect(n,duration)
{
  n.addClass('glow').delay(duration).queue(function(next){
    $(this).removeClass('glow');next();
  });
}

function highlightAnchor()
{
  var aname = hashUrl();
  var anchor = $(aname);
  if (anchor.parent().attr('class')=='memItemLeft'){
    var rows = $('.memberdecls tr[class$="'+hashValue()+'"]');
    glowEffect(rows.children(),300); // member without details
  } else if (anchor.parent().attr('class')=='fieldname'){
    glowEffect(anchor.parent().parent(),1000); // enum value
  } else if (anchor.parent().attr('class')=='fieldtype'){
    glowEffect(anchor.parent().parent(),1000); // struct field
  } else if (anchor.parent().is(":header")) {
    glowEffect(anchor.parent(),1000); // section header
  } else {
    glowEffect(anchor.next(),1000); // normal member
  }
  gotoAnchor(anchor,aname,false);
}

function selectAndHighlight(hash,n)
{
  var a;
  if (hash) {
    var link=stripPath(pathName())+':'+hash.substring(1);
    a=$('.item a[class$="'+link+'"]');
  }
  if (a && a.length) {
    a.parent().parent().addClass('selected');
    a.parent().parent().attr('id','selected');
    highlightAnchor();
  } else if (n) {
    $(n.itemDiv).addClass('selected');
    $(n.itemDiv).attr('id','selected');
  }
  if ($('#nav-tree-contents .item:first').hasClass('selected')) {
    $('#nav-sync').css('top','30px');
  } else {
    $('#nav-sync').css('top','5px');
  }
  showRoot();
}

function showNode(o, node, index, hash)
{
  if (node && node.childrenData) {
    if (typeof(node.childrenData)==='string') {
      var varName    = node.childrenData;
      getScript(node.relpath+varName,function(){
        node.childrenData = getData(varName);
        showNode(o,node,index,hash);
      },true);
    } else {
      if (!node.childrenVisited) {
        getNode(o, node);
      }
      $(node.getChildrenUL()).css({'display':'block'});
      if (node.isLast) {
        node.plus_img.src = node.relpath+"ftv2mlastnode.png";
      } else {
        node.plus_img.src = node.relpath+"ftv2mnode.png";
      }
      node.expanded = true;
      var n = node.children[o.breadcrumbs[index]];
      if (index+1<o.breadcrumbs.length) {
        showNode(o,n,index+1,hash);
      } else {
        if (typeof(n.childrenData)==='string') {
          var varName = n.childrenData;
          getScript(n.relpath+varName,function(){
            n.childrenData = getData(varName);
            node.expanded=false;
            showNode(o,node,index,hash); // retry with child node expanded
          },true);
        } else {
          var rootBase = stripPath(o.toroot.replace(/\..+$/, ''));
          if (rootBase=="index" || rootBase=="pages" || rootBase=="search") {
            expandNode(o, n, true, true);
          }
          selectAndHighlight(hash,n);
        }
      }
    }
  } else {
    selectAndHighlight(hash);
  }
}

function removeToInsertLater(element) {
  var parentNode = element.parentNode;
  var nextSibling = element.nextSibling;
  parentNode.removeChild(element);
  return function() {
    if (nextSibling) {
      parentNode.insertBefore(element, nextSibling);
    } else {
      parentNode.appendChild(element);
    }
  };
}

function getNode(o, po)
{
  var insertFunction = removeToInsertLater(po.li);
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
      i==l);
  }
  insertFunction();
}

function gotoNode(o,subIndex,root,hash,relpath)
{
  var nti = navTreeSubIndices[subIndex][root+hash];
  o.breadcrumbs = $.extend(true, [], nti ? nti : navTreeSubIndices[subIndex][root]);
  if (!o.breadcrumbs && root!=NAVTREE[0][1]) { // fallback: show index
    navTo(o,NAVTREE[0][1],"",relpath);
    $('.item').removeClass('selected');
    $('.item').removeAttr('id');
  }
  if (o.breadcrumbs) {
    o.breadcrumbs.unshift(0); // add 0 for root node
    showNode(o, o.node, 0, hash);
  }
}

function navTo(o,root,hash,relpath)
{
  var link = cachedLink();
  if (link) {
    var parts = link.split('#');
    root = parts[0];
    if (parts.length>1) hash = '#'+parts[1].replace(/[^\w\-]/g,'');
    else hash='';
  }
  if (hash.match(/^#l\d+$/)) {
    var anchor=$('a[name='+hash.substring(1)+']');
    glowEffect(anchor.parent(),1000); // line number
    hash=''; // strip line number anchors
  }
  var url=root+hash;
  var i=-1;
  while (NAVTREEINDEX[i+1]<=url) i++;
  if (i==-1) { i=0; root=NAVTREE[0][1]; } // fallback: show index
  if (navTreeSubIndices[i]) {
    gotoNode(o,i,root,hash,relpath)
  } else {
    getScript(relpath+'navtreeindex'+i,function(){
      navTreeSubIndices[i] = eval('NAVTREEINDEX'+i);
      if (navTreeSubIndices[i]) {
        gotoNode(o,i,root,hash,relpath);
      }
    },true);
  }
}

function showSyncOff(n,relpath)
{
    n.html('<img src="'+relpath+'sync_off.png" title="'+SYNCOFFMSG+'"/>');
}

function showSyncOn(n,relpath)
{
    n.html('<img src="'+relpath+'sync_on.png" title="'+SYNCONMSG+'"/>');
}

function toggleSyncButton(relpath)
{
  var navSync = $('#nav-sync');
  if (navSync.hasClass('sync')) {
    navSync.removeClass('sync');
    showSyncOff(navSync,relpath);
    storeLink(stripPath2(pathName())+hashUrl());
  } else {
    navSync.addClass('sync');
    showSyncOn(navSync,relpath);
    deleteLink();
  }
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;
  o.node.expanded = false;
  o.node.isLast = true;
  o.node.plus_img = document.createElement("img");
  o.node.plus_img.src = relpath+"ftv2pnode.png";
  o.node.plus_img.width = 16;
  o.node.plus_img.height = 22;

  if (localStorageSupported()) {
    var navSync = $('#nav-sync');
    if (cachedLink()) {
      showSyncOff(navSync,relpath);
      navSync.removeClass('sync');
    } else {
      showSyncOn(navSync,relpath);
    }
    navSync.click(function(){ toggleSyncButton(relpath); });
  }

  $(window).load(function(){
    navTo(o,toroot,hashUrl(),relpath);
    showRoot();
  });

  $(window).bind('hashchange', function(){
     if (window.location.hash && window.location.hash.length>1){
       var a;
       if ($(location).attr('hash')){
         var clslink=stripPath(pathName())+':'+hashValue();
         a=$('.item a[class$="'+clslink.replace(/</g,'\\3c ')+'"]');
       }
       if (a==null || !$(a).parent().parent().hasClass('selected')){
         $('.item').removeClass('selected');
         $('.item').removeAttr('id');
       }
       var link=stripPath2(pathName());
       navTo(o,link,hashUrl(),relpath);
     } else if (!animationInProgress) {
       $('#doc-content').scrollTop(0);
       $('.item').removeClass('selected');
       $('.item').removeAttr('id');
       navTo(o,toroot,hashUrl(),relpath);
     }
  })
}

