var folder_handling_endpoints =
[
    [ "<access-service>/api/v1/organisations/{orgId}/collections", "access-service-api-v1-organisations-orgid-collections.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-collections.html#access-service-api-v1-organisations-orgid-collections-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-collections.html#access-service-api-v1-organisations-orgid-collections-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-collections.html#access-service-api-v1-organisations-orgid-collections-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-collections.html#access-service-api-v1-organisations-orgid-collections-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-collections.html#access-service-api-v1-organisations-orgid-collections-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-organisations-orgid-collections.html#access-service-api-v1-organisations-orgid-collections-required-parameters", null ],
      [ "Optional parameters", "access-service-api-v1-organisations-orgid-collections.html#access-service-api-v1-organisations-orgid-collections-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-collections.html#access-service-api-v1-organisations-orgid-collections-returns", null ]
    ] ],
    [ "<access-service>/api/v1/collections/{collectionId}", "access-service-api-v1-collections-collectionid.html", [
      [ "URL structure", "access-service-api-v1-collections-collectionid.html#access-service-api-v1-collections-collectionid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-collections-collectionid.html#access-service-api-v1-collections-collectionid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-collections-collectionid.html#access-service-api-v1-collections-collectionid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-collections-collectionid.html#access-service-api-v1-collections-collectionid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-collections-collectionid.html#access-service-api-v1-collections-collectionid-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-collections-collectionid.html#access-service-api-v1-collections-collectionid-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-collections-collectionid.html#access-service-api-v1-collections-collectionid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/collections/{collectionId}/collections", "access-service-api-v1-collections-collectionid-collections.html", [
      [ "URL structure", "access-service-api-v1-collections-collectionid-collections.html#access-service-api-v1-collections-collectionid-collections-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-collections-collectionid-collections.html#access-service-api-v1-collections-collectionid-collections-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-collections-collectionid-collections.html#access-service-api-v1-collections-collectionid-collections-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-collections-collectionid-collections.html#access-service-api-v1-collections-collectionid-collections-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-collections-collectionid-collections.html#access-service-api-v1-collections-collectionid-collections-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-collections-collectionid-collections.html#access-service-api-v1-collections-collectionid-collections-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-collections-collectionid-collections.html#access-service-api-v1-collections-collectionid-collections-returns", null ]
    ] ]
];