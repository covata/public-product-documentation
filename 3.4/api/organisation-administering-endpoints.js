var organisation_administering_endpoints =
[
    [ "<access-service>/api/v1/organisations", "access-service-api-v1-organisations.html", [
      [ "URL structure", "access-service-api-v1-organisations.html#access-service-api-v1-organisations-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations.html#access-service-api-v1-organisations-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations.html#access-service-api-v1-organisations-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations.html#access-service-api-v1-organisations-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations.html#access-service-api-v1-organisations-required-headers", null ],
      [ "Required POST-request parameters", "access-service-api-v1-organisations.html#access-service-api-v1-organisations-required-post-request-parameters", null ],
      [ "Optional POST-request parameters", "access-service-api-v1-organisations.html#access-service-api-v1-organisations-optional-post-request-parameters", null ],
      [ "Optional GET-request parameters", "access-service-api-v1-organisations.html#access-service-api-v1-organisations-optional-get-request-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations.html#access-service-api-v1-organisations-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{organisationId}", "access-service-api-v1-organisations-organisationid.html", [
      [ "URL structure", "access-service-api-v1-organisations-organisationid.html#access-service-api-v1-organisations-organisationid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-organisationid.html#access-service-api-v1-organisations-organisationid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-organisationid.html#access-service-api-v1-organisations-organisationid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-organisationid.html#access-service-api-v1-organisations-organisationid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-organisationid.html#access-service-api-v1-organisations-organisationid-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-organisationid.html#access-service-api-v1-organisations-organisationid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{organisationId}/removalRequest", "access-service-api-v1-organisations-organisationid-removalrequest.html", [
      [ "URL structure", "access-service-api-v1-organisations-organisationid-removalrequest.html#access-service-api-v1-organisations-organisationid-removalrequest-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-organisationid-removalrequest.html#access-service-api-v1-organisations-organisationid-removalrequest-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-organisationid-removalrequest.html#access-service-api-v1-organisations-organisationid-removalrequest-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-organisations-organisationid-removalrequest.html#access-service-api-v1-organisations-organisationid-removalrequest-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-organisations-organisationid-removalrequest.html#access-service-api-v1-organisations-organisationid-removalrequest-required-headers", null ],
      [ "Required POST-request parameters", "access-service-api-v1-organisations-organisationid-removalrequest.html#access-service-api-v1-organisations-organisationid-removalrequest-required-post-request-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-organisationid-removalrequest.html#access-service-api-v1-organisations-organisationid-removalrequest-returns", null ]
    ] ]
];