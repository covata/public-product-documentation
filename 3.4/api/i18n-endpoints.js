var i18n_endpoints =
[
    [ "<access-service>/api/v1/i18n/adminpreference", "access-service-api-v1-i18n-adminpreference.html", [
      [ "URL structure", "access-service-api-v1-i18n-adminpreference.html#access-service-api-v1-i18n-adminpreference-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-i18n-adminpreference.html#access-service-api-v1-i18n-adminpreference-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-i18n-adminpreference.html#access-service-api-v1-i18n-adminpreference-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-i18n-adminpreference.html#access-service-api-v1-i18n-adminpreference-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-i18n-adminpreference.html#access-service-api-v1-i18n-adminpreference-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-i18n-adminpreference.html#access-service-api-v1-i18n-adminpreference-required-parameters", null ],
      [ "Returns", "access-service-api-v1-i18n-adminpreference.html#access-service-api-v1-i18n-adminpreference-returns", null ]
    ] ],
    [ "<access-service>/api/v1/i18n/bundles/{localeCode}", "access-service-api-v1-i18n-bundles-localecode.html", [
      [ "URL structure", "access-service-api-v1-i18n-bundles-localecode.html#access-service-api-v1-i18n-bundles-localecode-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-i18n-bundles-localecode.html#access-service-api-v1-i18n-bundles-localecode-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-i18n-bundles-localecode.html#access-service-api-v1-i18n-bundles-localecode-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-i18n-bundles-localecode.html#access-service-api-v1-i18n-bundles-localecode-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-i18n-bundles-localecode.html#access-service-api-v1-i18n-bundles-localecode-required-headers", null ],
      [ "Required parameters on POST", "access-service-api-v1-i18n-bundles-localecode.html#access-service-api-v1-i18n-bundles-localecode-required-parameters-on-POST", null ],
      [ "Returns from a GET request", "access-service-api-v1-i18n-bundles-localecode.html#access-service-api-v1-i18n-bundles-localecode-returns-from-a-GET-request", null ],
      [ "Returns from a POST request", "access-service-api-v1-i18n-bundles-localecode.html#access-service-api-v1-i18n-bundles-localecode-returns-from-a-POST-request", null ]
    ] ],
    [ "<access-service>/api/v1/i18n/bundles/{localeCode}/delete", "access-service-api-v1-i18n-bundles-localecode-delete.html", [
      [ "URL structure", "access-service-api-v1-i18n-bundles-localecode-delete.html#access-service-api-v1-i18n-bundles-localecode-delete-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-i18n-bundles-localecode-delete.html#access-service-api-v1-i18n-bundles-localecode-delete-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-i18n-bundles-localecode-delete.html#access-service-api-v1-i18n-bundles-localecode-delete-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-i18n-bundles-localecode-delete.html#access-service-api-v1-i18n-bundles-localecode-delete-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-i18n-bundles-localecode-delete.html#access-service-api-v1-i18n-bundles-localecode-delete-required-headers", null ],
      [ "Returns", "access-service-api-v1-i18n-bundles-localecode-delete.html#access-service-api-v1-i18n-bundles-localecode-delete-returns", null ]
    ] ],
    [ "<access-service>/api/v1/i18n/bundles/{localeCode}/download", "access-service-api-v1-i18n-bundles-localecode-download.html", [
      [ "URL structure", "access-service-api-v1-i18n-bundles-localecode-download.html#access-service-api-v1-i18n-bundles-localecode-download-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-i18n-bundles-localecode-download.html#access-service-api-v1-i18n-bundles-localecode-download-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-i18n-bundles-localecode-download.html#access-service-api-v1-i18n-bundles-localecode-download-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-i18n-bundles-localecode-download.html#access-service-api-v1-i18n-bundles-localecode-download-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-i18n-bundles-localecode-download.html#access-service-api-v1-i18n-bundles-localecode-download-required-headers", null ],
      [ "Returns", "access-service-api-v1-i18n-bundles-localecode-download.html#access-service-api-v1-i18n-bundles-localecode-download-returns", null ]
    ] ],
    [ "<access-service>/api/v1/i18n/bundles/{localeCode}/reset", "access-service-api-v1-i18n-bundles-localecode-reset.html", [
      [ "URL structure", "access-service-api-v1-i18n-bundles-localecode-reset.html#access-service-api-v1-i18n-bundles-localecode-reset-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-i18n-bundles-localecode-reset.html#access-service-api-v1-i18n-bundles-localecode-reset-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-i18n-bundles-localecode-reset.html#access-service-api-v1-i18n-bundles-localecode-reset-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-i18n-bundles-localecode-reset.html#access-service-api-v1-i18n-bundles-localecode-reset-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-i18n-bundles-localecode-reset.html#access-service-api-v1-i18n-bundles-localecode-reset-required-headers", null ],
      [ "Returns", "access-service-api-v1-i18n-bundles-localecode-reset.html#access-service-api-v1-i18n-bundles-localecode-reset-returns", null ]
    ] ],
    [ "<access-service>/api/v1/i18n/localedetails", "access-service-api-v1-i18n-localedetails.html", [
      [ "URL structure", "access-service-api-v1-i18n-localedetails.html#access-service-api-v1-i18n-localedetails-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-i18n-localedetails.html#access-service-api-v1-i18n-localedetails-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-i18n-localedetails.html#access-service-api-v1-i18n-localedetails-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-i18n-localedetails.html#access-service-api-v1-i18n-localedetails-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-i18n-localedetails.html#access-service-api-v1-i18n-localedetails-required-headers", null ],
      [ "Returns", "access-service-api-v1-i18n-localedetails.html#access-service-api-v1-i18n-localedetails-returns", null ]
    ] ],
    [ "<access-service>/api/v1/i18n/reload", "access-service-api-v1-i18n-reload.html", [
      [ "URL structure", "access-service-api-v1-i18n-reload.html#access-service-api-v1-i18n-reload-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-i18n-reload.html#access-service-api-v1-i18n-reload-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-i18n-reload.html#access-service-api-v1-i18n-reload-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-i18n-reload.html#access-service-api-v1-i18n-reload-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-i18n-reload.html#access-service-api-v1-i18n-reload-required-headers", null ],
      [ "Returns", "access-service-api-v1-i18n-reload.html#access-service-api-v1-i18n-reload-returns", null ]
    ] ],
    [ "<access-service>/api/v1/i18n/bundles", "access-service-api-v1-i18n-bundles.html", [
      [ "URL structure", "access-service-api-v1-i18n-bundles.html#access-service-api-v1-i18n-bundles-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-i18n-bundles.html#access-service-api-v1-i18n-bundles-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-i18n-bundles.html#access-service-api-v1-i18n-bundles-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-i18n-bundles.html#access-service-api-v1-i18n-bundles-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-i18n-bundles.html#access-service-api-v1-i18n-bundles-required-headers", null ],
      [ "Returns", "access-service-api-v1-i18n-bundles.html#access-service-api-v1-i18n-bundles-returns", null ]
    ] ]
];