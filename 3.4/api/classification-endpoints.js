var classification_endpoints =
[
    [ "<access-service>/api/v1/organisations/{orgId}/labels", "access-service-api-v1-organisations-orgid-labels.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-required-headers", null ],
      [ "Required POST-request parameters", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-required-post-request-parameters", null ],
      [ "Optional POST-request parameters", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-optional-post-request-parameters", null ],
      [ "Optional GET-request parameters", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-optional-get-request-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-labels.html#access-service-api-v1-organisations-orgid-labels-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/labels/{labelId}", "access-service-api-v1-organisations-orgid-labels-labelid.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-labels-labelid.html#access-service-api-v1-organisations-orgid-labels-labelid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/labels/{labelId}/priority", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-required-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-labels-labelid-priority.html#access-service-api-v1-organisations-orgid-labels-labelid-priority-returns", null ]
    ] ]
];