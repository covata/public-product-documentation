var file_handling_endpoints =
[
    [ "<access-service>/api/v1/organisations/{orgGroupId}/objects", "access-service-api-v1-organisations-orggroupid-objects.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-required-headers", null ],
      [ "Required POST-request parameters", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-required-post-request-parameters", null ],
      [ "Optional POST-request parameters", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-optional-post-request-parameters", null ],
      [ "Optional GET-request parameters", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-optional-get-request-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-objects.html#access-service-api-v1-organisations-orggroupid-objects-returns", null ]
    ] ],
    [ "<access-service>/api/v1/objects", "access-service-api-v1-objects.html", [
      [ "URL structure", "access-service-api-v1-objects.html#access-service-api-v1-objects-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-objects.html#access-service-api-v1-objects-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-objects.html#access-service-api-v1-objects-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-objects.html#access-service-api-v1-objects-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-objects.html#access-service-api-v1-objects-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-objects.html#access-service-api-v1-objects-required-parameters", null ],
      [ "Returns", "access-service-api-v1-objects.html#access-service-api-v1-objects-returns", null ]
    ] ],
    [ "<access-service>/api/v1/objects/{objectId}", "access-service-api-v1-objects-objectid.html", [
      [ "URL structure", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-required-headers", null ],
      [ "Optional PUT-request parameters", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-optional-put-request-parameters", null ],
      [ "Returns", "access-service-api-v1-objects-objectid.html#access-service-api-v1-objects-objectid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgGroupId}/objects/{objectId}/enable", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid-objects-objectid-enable.html#access-service-api-v1-organisations-orggroupid-objects-objectid-enable-returns", null ]
    ] ],
    [ "<access-service>/api/v1/objects/{itemId}/versions", "access-service-api-v1-objects-itemid-versions.html", [
      [ "URL structure", "access-service-api-v1-objects-itemid-versions.html#access-service-api-v1-objects-itemid-versions-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-objects-itemid-versions.html#access-service-api-v1-objects-itemid-versions-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-objects-itemid-versions.html#access-service-api-v1-objects-itemid-versions-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-objects-itemid-versions.html#access-service-api-v1-objects-itemid-versions-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-objects-itemid-versions.html#access-service-api-v1-objects-itemid-versions-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-objects-itemid-versions.html#access-service-api-v1-objects-itemid-versions-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-objects-itemid-versions.html#access-service-api-v1-objects-itemid-versions-returns", null ]
    ] ],
    [ "<access-service>/api/v1/objects/{itemId}/versions/{versionId}", "access-service-api-v1-objects-objectid-versions-versionid.html", [
      [ "URL structure", "access-service-api-v1-objects-objectid-versions-versionid.html#access-service-api-v1-objects-objectid-versions-versionid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-objects-objectid-versions-versionid.html#access-service-api-v1-objects-objectid-versions-versionid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-objects-objectid-versions-versionid.html#access-service-api-v1-objects-objectid-versions-versionid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-objects-objectid-versions-versionid.html#access-service-api-v1-objects-objectid-versions-versionid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-objects-objectid-versions-versionid.html#access-service-api-v1-objects-objectid-versions-versionid-required-headers", null ],
      [ "Returns", "access-service-api-v1-objects-objectid-versions-versionid.html#access-service-api-v1-objects-objectid-versions-versionid-returns", null ]
    ] ]
];