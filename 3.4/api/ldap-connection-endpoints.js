var ldap_connection_endpoints =
[
    [ "<access-service>/api/v1/ldapconnections", "access-service-api-v1-ldapconnections.html", [
      [ "URL structure", "access-service-api-v1-ldapconnections.html#access-service-api-v1-ldapconnections-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-ldapconnections.html#access-service-api-v1-ldapconnections-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-ldapconnections.html#access-service-api-v1-ldapconnections-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-ldapconnections.html#access-service-api-v1-ldapconnections-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-ldapconnections.html#access-service-api-v1-ldapconnections-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-ldapconnections.html#access-service-api-v1-ldapconnections-required-parameters", null ],
      [ "Returns", "access-service-api-v1-ldapconnections.html#access-service-api-v1-ldapconnections-returns", null ]
    ] ],
    [ "<access-service>/api/v1/ldapconnections/{ldapConnectionId}", "access-service-api-v1-ldapconnections-ldapconnectionid.html", [
      [ "URL structure", "access-service-api-v1-ldapconnections-ldapconnectionid.html#access-service-api-v1-ldapconnections-ldapconnectionid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-ldapconnections-ldapconnectionid.html#access-service-api-v1-ldapconnections-ldapconnectionid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-ldapconnections-ldapconnectionid.html#access-service-api-v1-ldapconnections-ldapconnectionid-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-ldapconnections-ldapconnectionid.html#access-service-api-v1-ldapconnections-ldapconnectionid-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-ldapconnections-ldapconnectionid.html#access-service-api-v1-ldapconnections-ldapconnectionid-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-ldapconnections-ldapconnectionid.html#access-service-api-v1-ldapconnections-ldapconnectionid-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-ldapconnections-ldapconnectionid.html#access-service-api-v1-ldapconnections-ldapconnectionid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/ldapconnections/availability", "access-service-api-v1-ldapconnections-availability.html", [
      [ "URL structure", "access-service-api-v1-ldapconnections-availability.html#access-service-api-v1-ldapconnections-availability-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-ldapconnections-availability.html#access-service-api-v1-ldapconnections-availability-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-ldapconnections-availability.html#access-service-api-v1-ldapconnections-availability-detailed-description", null ],
      [ "Supported roles", "access-service-api-v1-ldapconnections-availability.html#access-service-api-v1-ldapconnections-availability-supported-roles", null ],
      [ "Required headers", "access-service-api-v1-ldapconnections-availability.html#access-service-api-v1-ldapconnections-availability-required-headers", null ],
      [ "Required parameters", "access-service-api-v1-ldapconnections-availability.html#access-service-api-v1-ldapconnections-availability-required-parameters", null ],
      [ "Returns", "access-service-api-v1-ldapconnections-availability.html#access-service-api-v1-ldapconnections-availability-returns", null ]
    ] ]
];