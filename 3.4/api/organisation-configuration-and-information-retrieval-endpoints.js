var organisation_configuration_and_information_retrieval_endpoints =
[
    [ "<access-service>/api/v1/organisations/{orgGroupId}", "access-service-api-v1-organisations-orggroupid.html", [
      [ "URL structure", "access-service-api-v1-organisations-orggroupid.html#access-service-api-v1-organisations-orggroupid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orggroupid.html#access-service-api-v1-organisations-orggroupid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orggroupid.html#access-service-api-v1-organisations-orggroupid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orggroupid.html#access-service-api-v1-organisations-orggroupid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orggroupid.html#access-service-api-v1-organisations-orggroupid-required-headers", null ],
      [ "PUT-request parameters", "access-service-api-v1-organisations-orggroupid.html#access-service-api-v1-organisations-orggroupid-put-request-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orggroupid.html#access-service-api-v1-organisations-orggroupid-returns", null ]
    ] ]
];