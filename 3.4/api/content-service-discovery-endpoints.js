var content_service_discovery_endpoints =
[
    [ "<content-service>/api/v1", "content-service-api-v1-discovery.html", [
      [ "URL structure", "content-service-api-v1-discovery.html#content-service-api-v1-discovery-url-structure", null ],
      [ "Supported methods and overview", "content-service-api-v1-discovery.html#content-service-api-v1-discovery-supported-methods-and-overview", null ],
      [ "Detailed description", "content-service-api-v1-discovery.html#content-service-api-v1-discovery-detailed-description", null ],
      [ "Supported roles", "content-service-api-v1-discovery.html#content-service-api-v1-discovery-supported-roles", null ],
      [ "Required headers", "content-service-api-v1-discovery.html#content-service-api-v1-discovery-required-headers", null ],
      [ "Returns", "content-service-api-v1-discovery.html#content-service-api-v1-discovery-returns", null ]
    ] ]
];