var user_group_endpoints =
[
    [ "<access-service>/api/v1/organisations/{orgId}/groups", "access-service-api-v1-organisations-orgid-groups.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-required-headers", null ],
      [ "Required POST-request parameters", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-required-post-request-parameters", null ],
      [ "Optional POST-request parameters", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-optional-post-request-parameters", null ],
      [ "Optional GET-request parameters", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-optional-get-request-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-groups.html#access-service-api-v1-organisations-orgid-groups-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/groups/{groupId}", "access-service-api-v1-organisations-orgid-groups-groupid.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-required-headers", null ],
      [ "Optional parameters", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-groups-groupid.html#access-service-api-v1-organisations-orgid-groups-groupid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/groups/{groupId}/labels", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-v1-organisations-orgid-groups-groupid-labels-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-v1-organisations-orgid-groups-groupid-labels-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-v1-organisations-orgid-groups-groupid-labels-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-v1-organisations-groups-groupid-labels-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-v1-organisations-orgid-groups-groupid-labels-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-groups-groupid-labels.html#access-service-api-v1-organisations-orgid-groups-groupid-labels-returns", null ]
    ] ],
    [ "<access-service>/api/v1/organisations/{orgId}/groups/{groupId}/users", "access-service-api-v1-organisations-orgid-groups-groupid-users.html", [
      [ "URL structure", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-v1-organisations-orgid-groups-groupid-users-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-v1-organisations-orgid-groups-groupid-users-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-v1-organisations-orgid-groups-groupid-users-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-v1-organisations-orgid-groups-groupid-users-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-v1-organisations-orgid-groups-groupid-users-required-headers", null ],
      [ "Returns", "access-service-api-v1-organisations-orgid-groups-groupid-users.html#access-service-api-v1-organisations-orgid-groups-groupid-users-returns", null ]
    ] ]
];