var key_handling_endpoints =
[
    [ "<access-service>/api/v1/keys", "access-service-api-v1-keys.html", [
      [ "URL structure", "access-service-api-v1-keys.html#access-service-api-v1-keys-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-keys.html#access-service-api-v1-keys-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-keys.html#access-service-api-v1-keys-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-keys.html#access-service-api-v1-keys-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-keys.html#access-service-api-v1-keys-required-headers", null ],
      [ "Required body", "access-service-api-v1-keys.html#access-service-api-v1-keys-required-body", null ],
      [ "Optional parameters", "access-service-api-v1-keys.html#access-service-api-v1-keys-optional-parameters", null ],
      [ "Returns", "access-service-api-v1-keys.html#access-service-api-v1-keys-returns", null ]
    ] ],
    [ "<access-service>/api/v1/keys/{keyId}", "access-service-api-v1-keys-keyid.html", [
      [ "URL structure", "access-service-api-v1-keys-keyid.html#access-service-api-v1-keys-keyid-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-keys-keyid.html#access-service-api-v1-keys-keyid-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-keys-keyid.html#access-service-api-v1-keys-keyid-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-keys-keyid.html#access-service-api-v1-keys-keyid-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-keys-keyid.html#access-service-api-v1-keys-keyid-required-headers", null ],
      [ "Returns", "access-service-api-v1-keys-keyid.html#access-service-api-v1-keys-keyid-returns", null ]
    ] ],
    [ "<access-service>/api/v1/objects/{objectId}/keys", "access-service-api-v1-objects-objectid-keys.html", [
      [ "URL structure", "access-service-api-v1-objects-objectid-keys.html#access-service-api-v1-objects-objectid-keys-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-objects-objectid-keys.html#access-service-api-v1-objects-objectid-keys-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-objects-objectid-keys.html#access-service-api-v1-objects-objectid-keys-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-objects-objectid-keys.html#access-service-api-v1-objects-objectid-keys-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-objects-objectid-keys.html#access-service-api-v1-objects-objectid-keys-required-headers", null ],
      [ "Returns", "access-service-api-v1-objects-objectid-keys.html#access-service-api-v1-objects-objectid-keys-returns", null ]
    ] ],
    [ "<access-service>/api/v1/objects/{objectId}/keys/view", "access-service-api-v1-objects-objectid-keys-view.html", [
      [ "URL structure", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-v1-objects-objectid-keys-view-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-v1-objects-objectid-keys-view-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-v1-objects-objectid-keys-view-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-v1-objects-objectid-keys-view-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-v1-objects-objectid-keys-view-required-headers", null ],
      [ "Returns", "access-service-api-v1-objects-objectid-keys-view.html#access-service-api-v1-objects-objectid-keys-view-returns", null ]
    ] ],
    [ "<access-service>/api/v1/versions/{versionId}/key", "access-service-api-v1-versions-versionid-key.html", [
      [ "URL structure", "access-service-api-v1-versions-versionid-key.html#access-service-api-v1-objects-objectid-key-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-versions-versionid-key.html#access-service-api-v1-versions-versionid-key-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-versions-versionid-key.html#access-service-api-v1-versions-versionid-key-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-versions-versionid-key.html#access-service-api-v1-versions-versionid-keys-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-versions-versionid-key.html#access-service-api-v1-versions-versionid-key-required-headers", null ],
      [ "Returns", "access-service-api-v1-versions-versionid-key.html#access-service-api-v1-versions-versionid-key-returns", null ]
    ] ],
    [ "<access-service>/api/v1/versions/{versionId}/viewKey", "access-service-api-v1-versions-versionid-viewkey.html", [
      [ "URL structure", "access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-url-structure", null ],
      [ "Supported methods and overview", "access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-supported-methods-and-overview", null ],
      [ "Detailed description", "access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-detailed-description", null ],
      [ "Supported roles and conditions", "access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-supported-roles-and-conditions", null ],
      [ "Required headers", "access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-required-headers", null ],
      [ "Returns", "access-service-api-v1-versions-versionid-viewkey.html#access-service-api-v1-versions-versionid-viewkey-returns", null ]
    ] ]
];