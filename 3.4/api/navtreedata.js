var NAVTREE =
[
  [ "Covata API 3.4 Documentation", "index.html", [
    [ "Developer's Guide", "developers-guide.html", "developers-guide" ],
    [ "API Reference Guide", "api-reference-guide.html", "api-reference-guide" ],
    [ "Getting Support", "getting-support.html", null ],
    [ "Important notice", "important-notice.html", null ],
    [ "Covata Documentation Home", "^https://docs.covata.com/", null ]
  ] ]
];

var NAVTREEINDEX =
[
"access-service-api-oauth-authorize.html",
"access-service-api-v1-i18n-adminpreference.html#access-service-api-v1-i18n-adminpreference-required-headers",
"access-service-api-v1-organisations-orggroupid-statistics-items-extensions.html#access-service-api-v1-organisations-orggroupid-statistics-items-extensions-supported-roles-and-conditions",
"access-service-api-v1-users-me-expire.html#access-service-api-v1-users-me-expire-supported-roles"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';