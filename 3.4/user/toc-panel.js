var bigSideNav = 400;
var bigDocContent = 406;
var smallSideNav = 250;
var smallDocContent = 256;
var difference = 6;
var threshold = 1199;

$(function() {
    // Bind click event to image icon
    $('#tocpaneltoggleimg').on('click', function() {
        // Check if the TOC is not yet collapsed and if that's the case, collapse it
        if ($('#side-nav').css('width') != '0px') {
            $('#side-nav').css('width', '0px');
            $("#doc-content").css('margin-left', difference+'px');
            // Write the 'doxygen_width' cookie that keeps the TOC collapsed
            // (this function is defined in Doxygen's 'resize.js' file)
            writeCookie('width',0, null);
            location.reload(false);
        }
        else {
            // Otherwise, if the window-width is less than 'threshold' pixels, reset TOC to 'smallSideNav' pixels
            if ($(window).width() < threshold) {
                $('#side-nav').css('width', smallSideNav+'px');
                $("#doc-content").css('margin-left', smallDocContent+'px');
                // Write the 'doxygen_width' cookie that keeps the TOC at this size
                writeCookie('width',smallSideNav, null);
                location.reload(false);
            } else {
                // Otherwise, if the window-width is bigger than that, reset the TOC to 'bigSideNav' pixels
                $('#side-nav').css('width', bigSideNav+'px');
                $("#doc-content").css('margin-left', bigDocContent+'px');
                // Write the 'doxygen_width' cookie that keeps the TOC at this size
                writeCookie('width',bigSideNav, null);
                location.reload(false);
            }
        }
    });
});
