var NAVTREE =
[
  [ "Safe Share for Web 3.4 User's Guide", "index.html", [
    [ "Signing in", "signing-in.html", [
      [ "Configuring a mobile device for two-factor authentication", "signing-in.html#configuring-a-mobile-device-for-two-factor-authentication", null ],
      [ "Signing in using single sign-on", "signing-in.html#signing-in-using-single-sign-on", null ],
      [ "Signing out", "signing-in.html#signing-out", null ]
    ] ],
    [ "Uploading, storing and sharing content securely", "uploading-storing-and-sharing-content-securely.html", [
      [ "Uploading and storing personally owned content", "uploading-storing-and-sharing-content-securely.html#uploading-and-storing-personally-owned-content", null ],
      [ "Changing organizations", "uploading-storing-and-sharing-content-securely.html#changing-organisations", null ],
      [ "Sharing secure content", "uploading-storing-and-sharing-content-securely.html#sharing-secure-content", null ],
      [ "Uploading content to share through another user's folder", "uploading-storing-and-sharing-content-securely.html#uploading-content-to-share-through-another-users-folder", null ]
    ] ],
    [ "Accessing secure content", "accessing-secure-content.html", [
      [ "Viewing and printing content", "accessing-secure-content.html#viewing-and-printing-content", null ],
      [ "File formats supported by the content viewer", "accessing-secure-content.html#file-formats-supported-by-the-content-viewer", null ],
      [ "Downloading secure content", "accessing-secure-content.html#downloading-secure-content", null ]
    ] ],
    [ "Managing secure content", "managing-secure-content.html", [
      [ "Finding secure content", "managing-secure-content.html#finding-secure-content", null ],
      [ "Organizing content with folders", "managing-secure-content.html#organising-content-with-folders", null ],
      [ "Unsharing secure content", "managing-secure-content.html#unsharing-secure-content", null ],
      [ "Renaming an item", "managing-secure-content.html#renaming-an-item", null ],
      [ "Moving an item", "managing-secure-content.html#moving-an-item", null ],
      [ "Removing personally owned content", "managing-secure-content.html#removing-personally-owned-content", null ],
      [ "Removing content shared through another user's folder", "managing-secure-content.html#removing-content-shared-through-another-users-folder", null ],
      [ "Removing your access to content (shared with you)", "managing-secure-content.html#removing-your-access-to-content-shared-with-you", null ],
      [ "Accessing the Recycle Bin", "managing-secure-content.html#accessing-the-recycle-bin", null ],
      [ "Deleting and restoring content in the Recycle Bin", "managing-secure-content.html#deleting-and-restoring-content-in-the-recycle-bin", null ]
    ] ],
    [ "Managing file versions", "managing-file-versions.html", [
      [ "Accessing other versions of a file", "managing-file-versions.html#accessing-other-versions-of-a-file", null ],
      [ "Viewing and downloading another version of a file", "managing-file-versions.html#viewing-and-downloading-another-version-of-a-file", null ],
      [ "Uploading a new version of a file", "managing-file-versions.html#uploading-a-new-version-of-a-file", null ],
      [ "Deleting a version of a file", "managing-file-versions.html#deleting-a-version-of-a-file", null ],
      [ "Changing a file's 'Active' version", "managing-file-versions.html#changing-a-files-active-version", null ]
    ] ],
    [ "Viewing activities on secure content", "viewing-activities-on-secure-content.html", [
      [ "Types of activities indicated", "viewing-activities-on-secure-content.html#types-of-activities-indicated", null ]
    ] ],
    [ "Managing your account", "managing-your-account.html", [
      [ "Updating your account details", "managing-your-account.html#updating-your-account-details", null ],
      [ "Changing your password", "managing-your-account.html#changing-your-password", null ],
      [ "Enabling or disabling 2FA on your account", "managing-your-account.html#enabling-or-disabling-2fa-on-your-account", null ],
      [ "Terminating your user account's sessions", "managing-your-account.html#terminating-your-user-accounts-sessions", null ],
      [ "Viewing your last sign-in details", "managing-your-account.html#viewing-your-last-sign-in-details", null ],
      [ "Viewing your storage usages and quotas", "managing-your-account.html#viewing-your-storage-usages-and-quotas", null ],
      [ "Modifying your email notification preferences", "managing-your-account.html#modifying-your-email-notification-preferences", null ],
      [ "Viewing your account history", "managing-your-account.html#viewing-your-account-history", null ],
      [ "Revoking passcodes on registered devices", "managing-your-account.html#revoking-passcodes-on-registered-devices", null ],
      [ "Revoking client applications", "managing-your-account.html#revoking-client-applications", null ],
      [ "Managing your contacts", "managing-your-account.html#managing-your-contacts", null ]
    ] ],
    [ "Getting support", "getting-support.html", null ],
    [ "Important notice", "important-notice.html", null ],
    [ "Covata Documentation Home", "^https://docs.covata.com/", null ]
  ] ]
];

var NAVTREEINDEX =
[
"accessing-secure-content.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';