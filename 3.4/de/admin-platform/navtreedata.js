var NAVTREE =
[
  [ "Safe Share-Administratorenanleitung für Safe Share 3.4", "index.html", [
    [ "Zugriff auf die Safe Share-Verwaltung", "accessing-platform-administration.html", [
      [ "Anmeldung bei der Safe Share-Verwaltung", "accessing-platform-administration.html#signing-in-to-platform-administration", null ],
      [ "Die Oberfläche Safe Share-Verwaltung", "accessing-platform-administration.html#the-platform-administration-interface", null ],
      [ "Das Dashboard der Safe Share-Verwaltung", "accessing-platform-administration.html#the-platform-administration-dashboard", null ],
      [ "Abmeldung von der Safe Share-Verwaltung", "accessing-platform-administration.html#signing-out-of-platform-administration", null ],
      [ "Erster Zugriff auf die Safe Share-Verwaltung", "accessing-platform-administration.html#accessing-platform-administration-for-the-first-time", null ]
    ] ],
    [ "Verwaltung von Safe Share-Administrator-Benutzern", "administering-platform-administrator-users.html", [
      [ "Felder eines Safe Share-Administrator-Benutzers", "administering-platform-administrator-users.html#a-platform-administrator-users-fields", null ],
      [ "Safe Share-Administratoren und Benutzerrollen", "administering-platform-administrator-users.html#platform-administrators-and-user-roles", null ],
      [ "Hinzufügen eines Safe Share-Administrator-Accounts", "administering-platform-administrator-users.html#adding-a-platform-administrator-account", null ],
      [ "Entfernen von Safe Share-Administratoren", "administering-platform-administrator-users.html#removing-platform-administrators", null ],
      [ "Bearbeitung Ihres Safe Share-Administrator-Accounts", "administering-platform-administrator-users.html#editing-your-platform-administrator-account", null ],
      [ "Änderung des Passworts für Ihren Safe Share-Administrator-Account", "administering-platform-administrator-users.html#changing-your-platform-administrator-account-password", null ],
      [ "Beendigung von Sitzungen Ihres Safe Share-Administrator-Accounts", "administering-platform-administrator-users.html#terminating-your-platform-administrator-accounts-sessions", null ],
      [ "Aktivierung und Deaktivierung von 2FA für einen Safe Share-Administrator", "administering-platform-administrator-users.html#enabling-or-disabling-2fa-for-a-platform-administrator", null ],
      [ "Neukonfiguration von 2FA für einen Safe Share-Administrator", "administering-platform-administrator-users.html#re-configuring-2fa-for-a-platform-administrator", null ]
    ] ],
    [ "Verwaltung von Organisationen", "administering-organisations.html", [
      [ "Verwaltung einer Organisation", "administering-organisations.html#administering-an-organisation", null ],
      [ "Hinzufügen einer neuen Organisation", "administering-organisations.html#adding-a-new-organisation", null ],
      [ "Suche nach vorhandenen Organisationen", "administering-organisations.html#finding-existing-organisations", null ],
      [ "Bearbeitung einer bestehenden Organisation", "administering-organisations.html#editing-an-existing-organisation", null ],
      [ "Entfernen einer Organisation", "administering-organisations.html#removing-an-organisation", null ]
    ] ],
    [ "LDAP-Konfiguration", "configuring-ldap.html", [
      [ "Konfiguration einer LDAP-Verbindung", "configuring-ldap.html#configuring-an-ldap-connection", null ],
      [ "Hinzufügen einer neuen LDAP-Verbindung", "configuring-ldap.html#adding-a-new-ldap-connection", null ],
      [ "Bearbeitung einer bestehenden LDAP-Verbindung", "configuring-ldap.html#editing-an-existing-ldap-connection", null ],
      [ "Deaktivierung und Reaktivierung einer LDAP-Verbindung", "configuring-ldap.html#disabling-or-re-enabling-an-ldap-connection", null ]
    ] ],
    [ "Konfiguration der Einzelanmeldung über einen IdP", "configuring-single-sign-on-using-an-idp.html", [
      [ "Konfiguration eines IdP-Service", "configuring-single-sign-on-using-an-idp.html#configuring-an-idp-service", null ],
      [ "Hinzufügen einer IdP-Service-Konfiguration", "configuring-single-sign-on-using-an-idp.html#adding-an-idp-service-configuration", null ],
      [ "Bearbeitung der IdP-Service-Konfiguration", "configuring-single-sign-on-using-an-idp.html#editing-the-idp-service-configuration", null ],
      [ "Deaktivierung und Reaktivierung der IdP-Service-Konfiguration", "configuring-single-sign-on-using-an-idp.html#disabling-or-re-enabling-the-idp-service-configuration", null ]
    ] ],
    [ "Konfiguration von Client-Anwendungen", "configuring-client-applications.html", [
      [ "Konfiguration einer Client-Anwendung", "configuring-client-applications.html#configuring-a-client-application", null ],
      [ "Registrierung einer neuen Client-Anwendung", "configuring-client-applications.html#registering-a-new-client-application", null ],
      [ "Bearbeitung einer registrierten Client-Anwendung", "configuring-client-applications.html#editing-a-registered-client-application", null ],
      [ "Deaktivierung und Reaktivierung einer Client-Anwendung", "configuring-client-applications.html#disabling-or-re-enabling-a-client-application", null ]
    ] ],
    [ "Konfiguration von Eigenschaften der Safe Share-Verwaltung", "configuring-platform-administration-properties.html", [
      [ "Änderung von Eigenschaftswerten der Safe Share-Verwaltung", "configuring-platform-administration-properties.html#modifying-a-platform-administration-property-value", null ],
      [ "Eigenschaften der Safe Share-Verwaltung", "configuring-platform-administration-properties.html#platform-administration-properties", [
        [ "Eigenschaften der Benutzerauthentifizierung", "configuring-platform-administration-properties.html#user-authentication-properties", null ],
        [ "Analysen-Eigenschaften", "configuring-platform-administration-properties.html#analytics-properties", null ],
        [ "Eigenschaften der Links zu Anwendungen", "configuring-platform-administration-properties.html#application-links-properties", null ],
        [ "Eigenschaften der Benutzerbeschränkungen", "configuring-platform-administration-properties.html#user-limits-properties", null ],
        [ "Eigenschaften der Dateibeschränkungen", "configuring-platform-administration-properties.html#file-limits-properties", null ],
        [ "Benachrichtigungs-Eigenschaften", "configuring-platform-administration-properties.html#notifications-properties", null ],
        [ "Eigenschaften der Systemdetails", "configuring-platform-administration-properties.html#system-details-properties", null ]
      ] ]
    ] ],
    [ "Verwaltung der Internationalisierung", "managing-internationalization.html", [
      [ "Änderung der „Systemstandard“-Sprache", "managing-internationalization.html#changing-the-system-default-language", null ],
      [ "Export eines Sprachpakets", "managing-internationalization.html#exporting-a-language-bundle", null ],
      [ "Upload eines benutzerdefinierten/externen Sprachpakets", "managing-internationalization.html#uploading-a-customised-external-language-bundle", null ],
      [ "Löschen oder Zurücksetzen eines Sprachpakets", "managing-internationalization.html#deleting-or-resetting-a-language-bundle", null ]
    ] ],
    [ "Safe Share-Organisationsverwaltung", "organisation-administration.html", "organisation-administration" ],
    [ "Anhang", "appendix.html", "appendix" ],
    [ "Support", "getting-support.html", null ],
    [ "Wichtiger Hinweis", "important-notice.html", null ],
    [ "Covata-Dokumentation: Start", "^https://docs.covata.com/de", null ]
  ] ]
];

var NAVTREEINDEX =
[
"accessing-organisation-administration.html"
];

var SYNCONMSG = 'Klicken um Panelsynchronisation auszuschalten';
var SYNCOFFMSG = 'Klicken um Panelsynchronisation einzuschalten';