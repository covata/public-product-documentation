var appendix =
[
    [ "Design der webbasierten Schnittstellen von Safe Share", "theming-the-applications-web-based-interfaces.html", [
      [ "Mit Designs gestaltbare Bereiche von Safe Share for Web", "theming-the-applications-web-based-interfaces.html#themable-areas-of-the-web-application", null ],
      [ "Mit Designs gestaltbare Bereiche der webbasierten Verwaltung", "theming-the-applications-web-based-interfaces.html#themable-web-based-administration-areas", null ],
      [ "Weitere mit Designs gestaltbare Bereiche von Safe Share", "theming-the-applications-web-based-interfaces.html#other-themable-application-areas", null ]
    ] ],
    [ "Verwaltung von Safe Share for iOS über MDM-Software", "managing-safe-share-for-ios-through-mdm-software.html", [
      [ "Schritt 1 - Installation von Safe Share for iOS über MDM-Software", "managing-safe-share-for-ios-through-mdm-software.html#step-1-install-safe-share-for-ios-via-mdm-software", null ],
      [ "Schritt 2 - Konfiguration von Safe Share for iOS über MDM-Software", "managing-safe-share-for-ios-through-mdm-software.html#step-2-configure-safe-share-for-ios-via-mdm-software", null ],
      [ "Die Datei „mdm-configuration.plist“", "managing-safe-share-for-ios-through-mdm-software.html#the-mdm-configuration-plist-file", null ]
    ] ]
];