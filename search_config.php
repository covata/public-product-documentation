<script language="php">

$config = array(
  'PROJECT_NAME' => "Covata Documentation home page",
  'GENERATE_TREEVIEW' => false,
  'DISABLE_INDEX' => false,
);

$translator = array(
  'search_results_title' => "Search Results",
  'search_results' => array(
    0 => "Sorry, no documents matching your query.",
    1 => "Found <b>1</b> document matching your query.",
    2 => "Found <b>\$num</b> documents matching your query. Showing best matches first.",
  ),
  'search_matches' => "Matches:",
  'search' => "Search",
  'split_bar' => "",
  'logo' => "Generated on Wed Mar 8 2017 17:17:06 for Covata Documentation home page by&#160;\n<a href=\"http://www.doxygen.org/index.html\">\n<img class=\"footer\" src=\"doxygen.png\" alt=\"doxygen\"/></a> 1.8.10 ",
);

</script>
