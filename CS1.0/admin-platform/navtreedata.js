var NAVTREE =
[
  [ "SafeShare Administrator's Guide for SafeShare CS1.0", "index.html", [
    [ "Accessing SafeShare Administration", "accessing-platform-administration.html", [
      [ "Signing in to SafeShare Administration", "accessing-platform-administration.html#signing-in-to-platform-administration", null ],
      [ "The SafeShare Administration interface", "accessing-platform-administration.html#the-platform-administration-interface", null ],
      [ "The SafeShare Administration dashboard", "accessing-platform-administration.html#the-platform-administration-dashboard", null ],
      [ "Signing out of SafeShare Administration", "accessing-platform-administration.html#signing-out-of-platform-administration", null ],
      [ "Accessing SafeShare Administration for the first time", "accessing-platform-administration.html#accessing-platform-administration-for-the-first-time", null ]
    ] ],
    [ "Administering SafeShare administrator users", "administering-platform-administrator-users.html", [
      [ "A SafeShare administrator user's fields", "administering-platform-administrator-users.html#a-platform-administrator-users-fields", null ],
      [ "SafeShare administrators and user roles", "administering-platform-administrator-users.html#platform-administrators-and-user-roles", null ],
      [ "Adding a SafeShare administrator account", "administering-platform-administrator-users.html#adding-a-platform-administrator-account", null ],
      [ "Removing SafeShare administrators", "administering-platform-administrator-users.html#removing-platform-administrators", null ],
      [ "Editing your SafeShare administrator account", "administering-platform-administrator-users.html#editing-your-platform-administrator-account", null ],
      [ "Changing your SafeShare administrator account password", "administering-platform-administrator-users.html#changing-your-platform-administrator-account-password", null ],
      [ "Terminating your SafeShare administrator account's sessions", "administering-platform-administrator-users.html#terminating-your-platform-administrator-accounts-sessions", null ],
      [ "Enabling or disabling 2FA for a SafeShare administrator", "administering-platform-administrator-users.html#enabling-or-disabling-2fa-for-a-platform-administrator", null ],
      [ "Re-configuring 2FA for a SafeShare administrator", "administering-platform-administrator-users.html#re-configuring-2fa-for-a-platform-administrator", null ]
    ] ],
    [ "Administering organisations", "administering-organisations.html", [
      [ "Adding a new organisation", "administering-organisations.html#adding-a-new-organisation", null ],
      [ "Finding existing organisations", "administering-organisations.html#finding-existing-organisations", null ],
      [ "Editing an existing organisation", "administering-organisations.html#editing-an-existing-organisation", null ],
      [ "Removing an organisation", "administering-organisations.html#removing-an-organisation", null ]
    ] ],
    [ "Configuring single sign-on using an IdP", "configuring-single-sign-on-using-an-idp.html", [
      [ "Adding an IdP service configuration", "configuring-single-sign-on-using-an-idp.html#adding-an-idp-service-configuration", null ],
      [ "Editing the IdP service configuration", "configuring-single-sign-on-using-an-idp.html#editing-the-idp-service-configuration", null ],
      [ "Disabling or re-enabling the IdP service configuration", "configuring-single-sign-on-using-an-idp.html#disabling-or-re-enabling-the-idp-service-configuration", null ]
    ] ],
    [ "Configuring client applications", "configuring-client-applications.html", [
      [ "Registering a new client application", "configuring-client-applications.html#registering-a-new-client-application", null ],
      [ "Editing a registered client application", "configuring-client-applications.html#editing-a-registered-client-application", null ],
      [ "Disabling and re-enabling a client application", "configuring-client-applications.html#disabling-or-re-enabling-a-client-application", null ]
    ] ],
    [ "Configuring SafeShare Administration properties", "configuring-platform-administration-properties.html", [
      [ "Modifying a SafeShare Administration property value", "configuring-platform-administration-properties.html#modifying-a-platform-administration-property-value", null ],
      [ "SafeShare Administration properties", "configuring-platform-administration-properties.html#platform-administration-properties", [
        [ "User authentication properties", "configuring-platform-administration-properties.html#user-authentication-properties", null ],
        [ "Analytics properties", "configuring-platform-administration-properties.html#analytics-properties", null ],
        [ "Application links properties", "configuring-platform-administration-properties.html#application-links-properties", null ],
        [ "User limits properties", "configuring-platform-administration-properties.html#user-limits-properties", null ],
        [ "File limits properties", "configuring-platform-administration-properties.html#file-limits-properties", null ],
        [ "Notifications properties", "configuring-platform-administration-properties.html#notifications-properties", null ],
        [ "System details properties", "configuring-platform-administration-properties.html#system-details-properties", null ]
      ] ]
    ] ],
    [ "Managing internationalisation", "managing-internationalisation.html", [
      [ "Changing the 'System Default' language", "managing-internationalisation.html#changing-the-system-default-language", null ],
      [ "Exporting a language bundle", "managing-internationalisation.html#exporting-a-language-bundle", null ],
      [ "Uploading a customised/external language bundle", "managing-internationalisation.html#uploading-a-customised-external-language-bundle", null ],
      [ "Deleting or resetting a language bundle", "managing-internationalisation.html#deleting-or-resetting-a-language-bundle", null ]
    ] ],
    [ "Organisation Administration", "organisation-administration.html", "organisation-administration" ],
    [ "Appendix", "appendix.html", "appendix" ],
    [ "Getting Support", "getting-support.html", null ],
    [ "Important notice", "important-notice.html", null ],
    [ "SafeShare Manuals Home", "^https://docs.covata.com/", null ],
    [ "Support Home", "^https://support.covata.com/", null ]
  ] ]
];

var NAVTREEINDEX =
[
"accessing-organisation-administration.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';