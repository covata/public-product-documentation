var NAVTREE =
[
  [ "Organisation Administrator's Guide for SafeShare CS1.0", "index.html", [
    [ "Accessing Organisation Administration", "accessing-organisation-administration.html", [
      [ "Signing in to Organisation Administration", "accessing-organisation-administration.html#signing-in-to-organisation-administration", null ],
      [ "The Organisation Administration interface", "accessing-organisation-administration.html#the-organisation-administration-interface", null ],
      [ "The Organisation Administration dashboard", "accessing-organisation-administration.html#the-organisation-administration-dashboard", null ],
      [ "Signing out of Organisation Administration", "accessing-organisation-administration.html#signing-out-of-organisation-administration", null ]
    ] ],
    [ "Administering users within an organisation", "administering-users-within-an-organisation.html", [
      [ "An organisation user's fields", "administering-users-within-an-organisation.html#an-organisation-users-fields", null ],
      [ "An organisation user's roles", "administering-users-within-an-organisation.html#an-organisation-users-roles", null ],
      [ "An organisation user's activities", "administering-users-within-an-organisation.html#an-organisation-users-activities", null ],
      [ "Adding an organisation user account", "administering-users-within-an-organisation.html#adding-an-organisation-user-account", null ],
      [ "Adding organisation user accounts in bulk (from CSV)", "administering-users-within-an-organisation.html#adding-organisation-user-accounts-in-bulk-from-csv", null ],
      [ "Finding user accounts within your organisation", "administering-users-within-an-organisation.html#finding-user-accounts-within-your-organisation", null ],
      [ "Editing an organisation user account", "administering-users-within-an-organisation.html#editing-an-organisation-user-account", null ],
      [ "Removing a user account from your organisation", "administering-users-within-an-organisation.html#removing-a-user-account-from-your-organisation", null ],
      [ "Editing your Organisation administrator account", "administering-users-within-an-organisation.html#editing-your-organisation-administrator-account", null ],
      [ "Changing your Organisation administrator account password", "administering-users-within-an-organisation.html#changing-your-organisation-administrator-account-password", null ],
      [ "Terminating your Organisation administrator account's sessions", "administering-users-within-an-organisation.html#terminating-your-organisation-administrator-accounts-sessions", null ],
      [ "Enabling or disabling 2FA on Organisation user accounts", "administering-users-within-an-organisation.html#enabling-or-disabling-2fa-on-organisation-user-accounts", null ],
      [ "Re-configuring 2FA for an organisation user account", "administering-users-within-an-organisation.html#re-configuring-2fa-for-an-organisation-user-account", null ],
      [ "Configuring 'Users' page columns for organisation users", "administering-users-within-an-organisation.html#configuring-users-page-columns-for-organisation-users", null ]
    ] ],
    [ "Configuring LDAP", "configuring-ldap.html", [
      [ "Adding a new LDAP connection", "configuring-ldap.html#adding-a-new-ldap-connection", null ],
      [ "Editing an existing LDAP connection", "configuring-ldap.html#editing-an-existing-ldap-connection", null ],
      [ "Disabling or re-enabling an LDAP connection", "configuring-ldap.html#disabling-or-re-enabling-an-ldap-connection", null ]
    ] ],
    [ "Managing contact groups", "managing-contact-groups.html", [
      [ "Adding a new contact group", "managing-contact-groups.html#adding-a-new-contact-group", null ],
      [ "Editing an existing contact group", "managing-contact-groups.html#editing-an-existing-contact-group", null ],
      [ "Removing an existing contact group", "managing-contact-groups.html#removing-an-existing-contact-group", null ]
    ] ],
    [ "Managing user whitelists", "managing-user-whitelists.html", [
      [ "Adding new user whitelist rules", "managing-user-whitelists.html#adding-new-user-whitelist-rules", null ],
      [ "Editing an existing user whitelist rule's description", "managing-user-whitelists.html#editing-an-existing-user-whitelist-rules-description", null ],
      [ "Removing an existing user whitelist rule", "managing-user-whitelists.html#removing-an-existing-user-whitelist-rule", null ]
    ] ],
    [ "Managing classifications", "managing-classifications.html", [
      [ "Adding a new classification", "managing-classifications.html#adding-a-new-classification", null ],
      [ "Removing an existing classification", "managing-classifications.html#removing-an-existing-classification", null ]
    ] ],
    [ "Managing clearances", "managing-clearances.html", [
      [ "Adding a new clearance", "managing-clearances.html#adding-a-new-clearance", null ],
      [ "Modifying users in an existing clearance", "managing-clearances.html#modifying-users-in-an-existing-clearance", null ],
      [ "Modifying classifications on an existing clearance", "managing-clearances.html#modifying-classifications-on-an-existing-clearance", null ],
      [ "Editing an existing clearance", "managing-clearances.html#editing-an-existing-clearance", null ],
      [ "Removing an existing clearance", "managing-clearances.html#removing-an-existing-clearance", null ]
    ] ],
    [ "Managing users' storage quotas through plans", "managing-users-storage-quotas-through-plans.html", [
      [ "Adding a new plan", "managing-users-storage-quotas-through-plans.html#adding-a-new-plan", null ],
      [ "Editing an existing plan", "managing-users-storage-quotas-through-plans.html#editing-an-existing-plan", null ],
      [ "Removing an existing plan", "managing-users-storage-quotas-through-plans.html#removing-an-existing-plan", null ]
    ] ],
    [ "Administering files", "administering-files.html", [
      [ "Finding files", "administering-files.html#finding-files", null ],
      [ "A file object's fields", "administering-files.html#a-file-objects-fields", null ],
      [ "Viewing activities on files", "administering-files.html#viewing-activities-on-files", null ],
      [ "Disabling or (re-)enabling a file", "administering-files.html#disabling-or-re-enabling-a-file", null ]
    ] ],
    [ "Generating reports", "generating-reports.html", [
      [ "Activity fields", "generating-reports.html#activity-fields", null ],
      [ "Generating a report", "generating-reports.html#generating-a-report", null ]
    ] ],
    [ "Configuring Organisation Administration properties", "configuring-organisation-administration-properties.html", [
      [ "Modifying a Organisation Administration property value", "configuring-organisation-administration-properties.html#modifying-an-organisation-administration-property-value", null ],
      [ "Organisation Administration properties", "configuring-organisation-administration-properties.html#organisation-administration-properties", null ]
    ] ],
    [ "Appendix", "appendix.html", "appendix" ],
    [ "Getting Support", "getting-support.html", null ],
    [ "Important notice", "important-notice.html", null ],
    [ "SafeShare Manuals Home", "^https://docs.covata.com/", null ],
    [ "Support Home", "^https://support.covata.com/", null ]
  ] ]
];

var NAVTREEINDEX =
[
"accessing-organisation-administration.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';